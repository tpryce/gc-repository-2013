<?php
/**
 * @file
 * feature_market_barrier_reports.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function feature_market_barrier_reports_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
