<?php

function keno_alpha_preprocess_node(&$vars) {
  // using <span>, as <time> tag is not supported yet.
  $datetime = '<span class="date">' . date("l jS F Y", $vars['created']) . '</span>';
  if ($vars['teaser']) {
    $vars['submitted'] = $datetime;
  }
  else {
    // Determine whether comments link should be displayed;
    if ($vars['node']->comment == COMMENT_NODE_HIDDEN) {
      $vars['submitted'] = t('!datetime – !username', array('!username' => $vars['name'], '!datetime' => $datetime));
    }
    else {
      $comments_link = l(format_plural($vars['comment_count'], '1 Comment', '@count Comments'),
      drupal_lookup_path('alias',$vars['path']['source']),
      array(
        'fragment' => 'comments',
        'attributes' => array('class' => array('comment-count'))));

      $vars['submitted'] = t('!datetime – !username – !comments', array('!username' => $vars['name'], '!datetime' => $datetime, '!comments' => $comments_link));
    }
  }

  // Use node action links (print, bookmark, email, etc...) from the custom
  // block implementation. Only for node pages, but not for all. Put in forbidden
  // types the nodes for which the action links are not needed.
  $forbidden_types = array('video');
  if (node_is_page($vars['node']) && !in_array($vars['type'], $forbidden_types)) {
    $gcl_node_actions = gcl_node_actions_block_view('node_actions');
    $gcl_node_actions_markup = '<div class="block block-gcl-node-actions block-gcl-node-actions-node-actions">';
    $gcl_node_actions_markup .= render($gcl_node_actions['content']);
    $gcl_node_actions_markup .= '</div>';
  }

  // The designs don't show any of the block links when the free trial form is
  // displayed. In any other case, add them after the body. Using its #suffix
  // property, as it's the quickest way to put them between the body and the
  // tags.
  if (isset($gcl_node_actions_markup) && (isset($vars['content']['body']) || isset($vars['content']['flippy_pager'])) && !isset($vars['content']['freetrialform'])) {
    // If there's a pager, links will go after it. Otherwise, after the body.
    $field_to_use = isset($vars['content']['flippy_pager']) ? 'flippy_pager' : 'body';
    if (isset($vars['content'][$field_to_use]['#suffix'])) {
      $vars['content'][$field_to_use]['#suffix'] .= $gcl_node_actions_markup;
    }
    else {
      $vars['content'][$field_to_use]['#suffix'] = $gcl_node_actions_markup;
    }
    // This can be done through the UI, but makes the custom block flag link
    // not to work, so "for now" it's ok to do it here.
    unset($vars['content']['links']['flag']);
  }

  // Based on the preceeding comment, "The designs don't show any of the block
  // links when the free trial form is displayed" unset the flippy pager if the
  // free trial form is displayed.
  if (isset($vars['content']['freetrialform'])) {
    unset($vars['content']['flippy_pager']);
  }

  /*
   * Set the Sectors, Geography, Topics and Content links in a different
   * variable, to be rendered at the top of the article.
   *
   * This can be done safely here, since 'hidden' fields on the 'manage display'
   * section of the Fields UI won't come on $vars['content'], so we'll process
   * them only in case the user is supposed to see them.
   */

  // Commented for now. Not sure if this will be the way used in the end.
  // Also, this should probably be extended for more CTs
  if ($vars['type'] == 'regulatory_reports') {
//    $vars['embedded_container'] = keno_alpha_regulatory_reports_embedded_content($vars);
  }

  // Break the managed newsletter and nodequeue links out of links so that they
  // can be displayed side by side.
  $managed_newsletter = isset($vars['content']['links']['managed_newsletter']) ? $vars['content']['links']['managed_newsletter'] : array();
  $vars['content']['links_left']['managed_newsletter'] = $managed_newsletter;
  $vars['content']['links_left']['managed_newsletter']['#theme'] = 'links__node';

  $nodequeue = isset($vars['content']['links']['nodequeue']) ? $vars['content']['links']['nodequeue'] : array();
  $vars['content']['links_right']['nodequeue'] = $nodequeue;

  unset($vars['content']['links']['managed_newsletter']);
  unset($vars['content']['links']['nodequeue']);
}

/**
 * This should aggregate the main image of the report nodes and the category
 * links to be displayed on the top (same ones apparing at the bottom,
 * but only with 1 link per vocab).
 */
function keno_alpha_regulatory_reports_embedded_content($vars) {
  if (isset($vars['content']['group_trunk_categories'])) {
    $embedded_container = NULL;

    $trunk_categories = $vars['content']['group_trunk_categories'];
    $trunk_categories['field_geography']['#items'] = array($trunk_categories['field_geography']['#items'][0]);
    $trunk_categories['field_sectors']['#items'] = array($trunk_categories['field_sectors']['#items'][0]);
    $trunk_categories['field_topics']['#items'] = array($trunk_categories['field_topics']['#items'][0]);
    $trunk_categories['field_content']['#items'] = array($trunk_categories['field_content']['#items'][0]);

    $embedded_container = '<div class="group-trunk-categories-top">' . render($trunk_categories) . '</div>';

    return $embedded_container;
  }
  else {
    return NULL;
  }
}
