<?php
/**
 * @file
 * feature_market_analysis.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function feature_market_analysis_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function feature_market_analysis_node_info() {
  $items = array(
    'market_analysis' => array(
      'name' => t('Market Analysis'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
