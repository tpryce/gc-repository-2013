<?php
// $Id$

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function managed_newsletters_senders_form($form, &$form_state) {
  $senders = managed_newsletters_get_all_senders_classes();
  $form = array();
  $form['default_sender'] = array(
	'#type' => 'radios',
	'#title' => t('Default send method'),
	'#options' => $senders,
	'#default_value' => managed_newsletter_sender::get_default_sender(),
	'#required' => true
	);
  $form['submit'] = array(
	'#type' => 'submit',
	'#value' => t('Save')
	);
  return $form;
}

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function managed_newsletters_senders_form_submit($form, &$form_state) {
  managed_newsletter_sender::set_default_sender($form_state['values']['default_sender']);
}
