<?php
/**
 * @file
 * feature_regulatory_news.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function feature_regulatory_news_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_anonymous_regulatory_news';
  $strongarm->value = 0;
  $export['comment_anonymous_regulatory_news'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_mode_regulatory_news';
  $strongarm->value = 1;
  $export['comment_default_mode_regulatory_news'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_per_page_regulatory_news';
  $strongarm->value = '50';
  $export['comment_default_per_page_regulatory_news'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_form_location_regulatory_news';
  $strongarm->value = 1;
  $export['comment_form_location_regulatory_news'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_preview_regulatory_news';
  $strongarm->value = '1';
  $export['comment_preview_regulatory_news'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_regulatory_news';
  $strongarm->value = '2';
  $export['comment_regulatory_news'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_subject_field_regulatory_news';
  $strongarm->value = 1;
  $export['comment_subject_field_regulatory_news'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'field_bundle_settings_node__regulatory_news';
  $strongarm->value = array(
    'view_modes' => array(
      'teaser' => array(
        'custom_settings' => TRUE,
      ),
      'full' => array(
        'custom_settings' => FALSE,
      ),
      'rss' => array(
        'custom_settings' => FALSE,
      ),
      'search_index' => array(
        'custom_settings' => FALSE,
      ),
      'search_result' => array(
        'custom_settings' => TRUE,
      ),
      'token' => array(
        'custom_settings' => FALSE,
      ),
      'subscription' => array(
        'custom_settings' => TRUE,
      ),
      'anonymous user' => array(
        'custom_settings' => TRUE,
      ),
      'Light Subscription' => array(
        'custom_settings' => TRUE,
      ),
      'Full Subscription' => array(
        'custom_settings' => TRUE,
      ),
      'authenticated user' => array(
        'custom_settings' => FALSE,
      ),
      'Internal Author' => array(
        'custom_settings' => FALSE,
      ),
      'Content Editor' => array(
        'custom_settings' => FALSE,
      ),
      'Content Manager' => array(
        'custom_settings' => FALSE,
      ),
      'admin' => array(
        'custom_settings' => FALSE,
      ),
      'Lite Subscription' => array(
        'custom_settings' => FALSE,
      ),
      'print' => array(
        'custom_settings' => FALSE,
      ),
      'anonymous' => array(
        'custom_settings' => TRUE,
      ),
      'Americas_Premium' => array(
        'custom_settings' => FALSE,
      ),
      'International_Premium' => array(
        'custom_settings' => FALSE,
      ),
      'Americas_Professional' => array(
        'custom_settings' => FALSE,
      ),
      'International_Professional' => array(
        'custom_settings' => FALSE,
      ),
      'Gambling_Data' => array(
        'custom_settings' => FALSE,
      ),
    ),
    'extra_fields' => array(
      'form' => array(
        'flag' => array(
          'weight' => '1',
        ),
        'title' => array(
          'weight' => '0',
        ),
        'path' => array(
          'weight' => '16',
        ),
      ),
      'display' => array(
        'flippy_pager' => array(
          'default' => array(
            'weight' => '5',
            'visible' => TRUE,
          ),
          'anonymous user' => array(
            'weight' => '1',
            'visible' => FALSE,
          ),
          'Light Subscription' => array(
            'weight' => '0',
            'visible' => FALSE,
          ),
          'Full Subscription' => array(
            'weight' => '0',
            'visible' => TRUE,
          ),
          'teaser' => array(
            'weight' => '5',
            'visible' => TRUE,
          ),
          'search_result' => array(
            'weight' => '3',
            'visible' => TRUE,
          ),
          'subscription' => array(
            'weight' => '2',
            'visible' => TRUE,
          ),
          'anonymous' => array(
            'weight' => '1',
            'visible' => TRUE,
          ),
        ),
        'subscriptions_ui' => array(
          'default' => array(
            'weight' => '18',
            'visible' => TRUE,
          ),
          'anonymous user' => array(
            'weight' => '100',
            'visible' => TRUE,
          ),
          'Light Subscription' => array(
            'weight' => '100',
            'visible' => FALSE,
          ),
          'Full Subscription' => array(
            'weight' => '100',
            'visible' => FALSE,
          ),
          'teaser' => array(
            'weight' => '100',
            'visible' => TRUE,
          ),
          'search_result' => array(
            'weight' => '5',
            'visible' => TRUE,
          ),
          'subscription' => array(
            'weight' => '15',
            'visible' => FALSE,
          ),
          'anonymous' => array(
            'weight' => '4',
            'visible' => FALSE,
          ),
        ),
        'domain' => array(
          'default' => array(
            'weight' => '0',
            'visible' => TRUE,
          ),
          'subscription' => array(
            'weight' => '6',
            'visible' => FALSE,
          ),
          'anonymous' => array(
            'weight' => '14',
            'visible' => FALSE,
          ),
          'search_result' => array(
            'weight' => '1',
            'visible' => TRUE,
          ),
        ),
        'freetrialform' => array(
          'default' => array(
            'weight' => '7',
            'visible' => FALSE,
          ),
          'subscription' => array(
            'weight' => '11',
            'visible' => FALSE,
          ),
          'anonymous' => array(
            'weight' => '2',
            'visible' => TRUE,
          ),
          'search_result' => array(
            'weight' => '2',
            'visible' => TRUE,
          ),
        ),
      ),
    ),
  );
  $export['field_bundle_settings_node__regulatory_news'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_options_regulatory_news';
  $strongarm->value = array(
    0 => 'main-menu',
  );
  $export['menu_options_regulatory_news'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_parent_regulatory_news';
  $strongarm->value = 'main-menu:0';
  $export['menu_parent_regulatory_news'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_regulatory_news';
  $strongarm->value = array(
    0 => 'moderation',
    1 => 'revision',
  );
  $export['node_options_regulatory_news'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_preview_regulatory_news';
  $strongarm->value = '1';
  $export['node_preview_regulatory_news'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_submitted_regulatory_news';
  $strongarm->value = 1;
  $export['node_submitted_regulatory_news'] = $strongarm;

  return $export;
}
