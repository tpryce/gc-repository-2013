(function ($) {
  Drupal.behaviors.gcl = {
    attach: function (context, settings) {

      $height = $('#zone-menu-wrapper').height() -50;
      $('#zone-menu-wrapper').addClass('is-closed');
      /* $('#region-menu .block .block-inner .content').height($height - 100); */
      $('#zone-menu-wrapper').height("30px");
      $('#region-menu section').animate({ width: "150px" }, 10 );
      $('#region-menu .region-inner').append('<div style="float:right;" class="closeme"><img src="/sites/all/themes/keno/images/close.png"></div>');
      $('.closeme').hide();

      $('#find-content').toggle(function() {
        $('#zone-menu-wrapper').animate({ height: $height }, 100 );
        $('#region-menu section').animate({ width: "150px" }, 10 );
        $('#region-menu section.even').animate({ width: "310px" }, 200 );
        $('.closeme').show();
        $('#zone-menu-wrapper').removeClass('is-closed');
        $('#zone-menu-wrapper').addClass('is-open');
      }, function() {
        $('#zone-menu-wrapper').animate({ height: "30px" }, 100 );
        $('.closeme').hide();
        $('#region-menu section').animate({ width: "155px" }, 400 );
        $('#zone-menu-wrapper').removeClass('is-open');
        $('#zone-menu-wrapper').addClass('is-closed');
      });

      $('.closeme').click(function() {
        $('#zone-menu-wrapper').animate({height: "30px"}, 400 );
        $('.closeme').hide();
      });
    }
  };
})(jQuery);
