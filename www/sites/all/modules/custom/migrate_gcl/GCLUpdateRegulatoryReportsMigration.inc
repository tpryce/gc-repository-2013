<?php

/**
 * @file
 * Update date fields that weren't mapped in the original import.
 *
 * This migration set the systemOfRecord to DESTINATION which means previously
 * migrated content is loaded and updated with just the fields mapped here.
 *
 * @link http://drupal.org/node/1117454 @endlink
 */

class GCLUpdateRegulatoryReportsMigration extends Migration {
  public function __construct() {
    parent::__construct();

    $this->description = t('Update fields with missing content for the Regulatory Reports migration.');

    $this->systemOfRecord = Migration::DESTINATION;

    $source_key = array(
      'nid' => array(
        'type' => 'int',
        'not null' => TRUE,
        'description' => 'Nid.',
        'alias' => 'n',
      ),
    );
    $destination_key = MigrateDestinationNode::getKeySchema();
    $this->map = new MigrateSQLMap($this->machineName, $source_key, $destination_key);

    $this->destination = new MigrateDestinationNode('regulatory_reports');

    $query = Database::getConnection('default', 'legacy')->select('node', 'n');
    // No, sheduled is not a typo.
    $query->join('field_data_field_sheduled_review', 'r', "r.entity_type = 'node' AND r.bundle = 'regulatory_reports' AND r.entity_id = n.nid AND r.deleted <> 1");
    $query->fields('n', array('nid'))
      ->fields('r', array('field_sheduled_review_value'));
    $query->condition('n.type', 'regulatory_reports');
    $query->orderBy('n.nid', 'ASC');

    $this->source = new MigrateSourceSQL($query, array(), NULL, array('map_joinable' => FALSE));

    $this->addFieldMapping('nid', 'nid')->sourceMigration('RegulatoryReports');

    $this->addFieldMapping('field_sheduled_review', 'field_sheduled_review_value');
  }

  public function prepare(stdClass $node, stdClass $source_row) {
    $field_scheduled_review = array(
      'value' => (string) $source_row->field_sheduled_review_value,
      'timezone' => 'Europe/London',
      'timezone_db'=> 'Europe/London',
      'date_type' => 'date',
    );

    $node->field_sheduled_review = array();
    $node->field_sheduled_review[LANGUAGE_NONE][0] = $field_scheduled_review;
  }
}
