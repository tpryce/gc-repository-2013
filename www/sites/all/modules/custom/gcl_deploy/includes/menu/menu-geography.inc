<?php
$menu = array (
  'menu_name' => 'menu-geography',
  'title' => 'Geography',
  'description' => 'Mega-menu geography section.',
  'items' => 
  array (
    0 => 
    array (
      'menu_name' => 'menu-geography',
      'mlid' => '1544',
      'plid' => '0',
      'link_path' => 'search/site/',
      'router_path' => 'search/site/%',
      'link_title' => 'Latin America',
      'options' => 
      array (
        'attributes' => 
        array (
          'title' => '',
        ),
        'query' => 
        array (
          'f[0]' => 'im_field_geography:97',
        ),
      ),
      'module' => 'menu',
      'hidden' => '0',
      'external' => '0',
      'has_children' => '0',
      'expanded' => '0',
      'weight' => '-50',
      'depth' => '1',
      'customized' => '1',
      'p1' => '1544',
      'p2' => '0',
      'p3' => '0',
      'p4' => '0',
      'p5' => '0',
      'p6' => '0',
      'p7' => '0',
      'p8' => '0',
      'p9' => '0',
      'updated' => '0',
      'load_functions' => 
      array (
        2 => NULL,
      ),
      'to_arg_functions' => '',
      'access_callback' => 'user_access',
      'access_arguments' => 'a:1:{i:0;s:14:"search content";}',
      'page_callback' => 'apachesolr_search_custom_page',
      'page_arguments' => 'a:3:{i:0;s:11:"core_search";i:1;i:2;i:2;b:0;}',
      'delivery_callback' => '',
      'tab_parent' => 'search/site',
      'tab_root' => 'search/site/%',
      'title' => 'Latin America',
      'title_callback' => 't',
      'title_arguments' => '',
      'theme_callback' => '',
      'theme_arguments' => 'a:0:{}',
      'type' => '132',
      'description' => '',
      'in_active_trail' => false,
      'href' => 'search/site/',
      'access' => true,
      'localized_options' => 
      array (
        'attributes' => 
        array (
          'title' => '',
        ),
        'query' => 
        array (
          'f[0]' => 'im_field_geography:97',
        ),
      ),
    ),
    1 => 
    array (
      'menu_name' => 'menu-geography',
      'mlid' => '1545',
      'plid' => '0',
      'link_path' => 'search/site/',
      'router_path' => 'search/site/%',
      'link_title' => 'Europe',
      'options' => 
      array (
        'attributes' => 
        array (
          'title' => '',
        ),
        'query' => 
        array (
          'f[0]' => 'im_field_geography:94',
        ),
      ),
      'module' => 'menu',
      'hidden' => '0',
      'external' => '0',
      'has_children' => '0',
      'expanded' => '0',
      'weight' => '-49',
      'depth' => '1',
      'customized' => '1',
      'p1' => '1545',
      'p2' => '0',
      'p3' => '0',
      'p4' => '0',
      'p5' => '0',
      'p6' => '0',
      'p7' => '0',
      'p8' => '0',
      'p9' => '0',
      'updated' => '0',
      'load_functions' => 
      array (
        2 => NULL,
      ),
      'to_arg_functions' => '',
      'access_callback' => 'user_access',
      'access_arguments' => 'a:1:{i:0;s:14:"search content";}',
      'page_callback' => 'apachesolr_search_custom_page',
      'page_arguments' => 'a:3:{i:0;s:11:"core_search";i:1;i:2;i:2;b:0;}',
      'delivery_callback' => '',
      'tab_parent' => 'search/site',
      'tab_root' => 'search/site/%',
      'title' => 'Europe',
      'title_callback' => 't',
      'title_arguments' => '',
      'theme_callback' => '',
      'theme_arguments' => 'a:0:{}',
      'type' => '132',
      'description' => '',
      'in_active_trail' => false,
      'href' => 'search/site/',
      'access' => true,
      'localized_options' => 
      array (
        'attributes' => 
        array (
          'title' => '',
        ),
        'query' => 
        array (
          'f[0]' => 'im_field_geography:94',
        ),
      ),
    ),
    2 => 
    array (
      'menu_name' => 'menu-geography',
      'mlid' => '1548',
      'plid' => '0',
      'link_path' => 'search/site/',
      'router_path' => 'search/site/%',
      'link_title' => 'Middle East',
      'options' => 
      array (
        'attributes' => 
        array (
          'title' => '',
        ),
        'query' => 
        array (
          'f[0]' => 'im_field_geography:99',
        ),
      ),
      'module' => 'menu',
      'hidden' => '0',
      'external' => '0',
      'has_children' => '0',
      'expanded' => '0',
      'weight' => '-48',
      'depth' => '1',
      'customized' => '1',
      'p1' => '1548',
      'p2' => '0',
      'p3' => '0',
      'p4' => '0',
      'p5' => '0',
      'p6' => '0',
      'p7' => '0',
      'p8' => '0',
      'p9' => '0',
      'updated' => '0',
      'load_functions' => 
      array (
        2 => NULL,
      ),
      'to_arg_functions' => '',
      'access_callback' => 'user_access',
      'access_arguments' => 'a:1:{i:0;s:14:"search content";}',
      'page_callback' => 'apachesolr_search_custom_page',
      'page_arguments' => 'a:3:{i:0;s:11:"core_search";i:1;i:2;i:2;b:0;}',
      'delivery_callback' => '',
      'tab_parent' => 'search/site',
      'tab_root' => 'search/site/%',
      'title' => 'Middle East',
      'title_callback' => 't',
      'title_arguments' => '',
      'theme_callback' => '',
      'theme_arguments' => 'a:0:{}',
      'type' => '132',
      'description' => '',
      'in_active_trail' => false,
      'href' => 'search/site/',
      'access' => true,
      'localized_options' => 
      array (
        'attributes' => 
        array (
          'title' => '',
        ),
        'query' => 
        array (
          'f[0]' => 'im_field_geography:99',
        ),
      ),
    ),
    3 => 
    array (
      'menu_name' => 'menu-geography',
      'mlid' => '1546',
      'plid' => '0',
      'link_path' => 'search/site/',
      'router_path' => 'search/site/%',
      'link_title' => 'Africa',
      'options' => 
      array (
        'attributes' => 
        array (
          'title' => '',
        ),
        'query' => 
        array (
          'f[0]' => 'im_field_geography:100',
        ),
      ),
      'module' => 'menu',
      'hidden' => '0',
      'external' => '0',
      'has_children' => '0',
      'expanded' => '0',
      'weight' => '-47',
      'depth' => '1',
      'customized' => '1',
      'p1' => '1546',
      'p2' => '0',
      'p3' => '0',
      'p4' => '0',
      'p5' => '0',
      'p6' => '0',
      'p7' => '0',
      'p8' => '0',
      'p9' => '0',
      'updated' => '0',
      'load_functions' => 
      array (
        2 => NULL,
      ),
      'to_arg_functions' => '',
      'access_callback' => 'user_access',
      'access_arguments' => 'a:1:{i:0;s:14:"search content";}',
      'page_callback' => 'apachesolr_search_custom_page',
      'page_arguments' => 'a:3:{i:0;s:11:"core_search";i:1;i:2;i:2;b:0;}',
      'delivery_callback' => '',
      'tab_parent' => 'search/site',
      'tab_root' => 'search/site/%',
      'title' => 'Africa',
      'title_callback' => 't',
      'title_arguments' => '',
      'theme_callback' => '',
      'theme_arguments' => 'a:0:{}',
      'type' => '132',
      'description' => '',
      'in_active_trail' => false,
      'href' => 'search/site/',
      'access' => true,
      'localized_options' => 
      array (
        'attributes' => 
        array (
          'title' => '',
        ),
        'query' => 
        array (
          'f[0]' => 'im_field_geography:100',
        ),
      ),
    ),
    4 => 
    array (
      'menu_name' => 'menu-geography',
      'mlid' => '1806',
      'plid' => '0',
      'link_path' => 'search/site/',
      'router_path' => 'search/site/%',
      'link_title' => 'Asia/Pacific',
      'options' => 
      array (
        'query' => 
        array (
          'f[0]' => 'im_field_geography:101',
        ),
        'attributes' => 
        array (
          'title' => '',
        ),
      ),
      'module' => 'menu',
      'hidden' => '0',
      'external' => '0',
      'has_children' => '0',
      'expanded' => '0',
      'weight' => '-46',
      'depth' => '1',
      'customized' => '1',
      'p1' => '1806',
      'p2' => '0',
      'p3' => '0',
      'p4' => '0',
      'p5' => '0',
      'p6' => '0',
      'p7' => '0',
      'p8' => '0',
      'p9' => '0',
      'updated' => '0',
      'load_functions' => 
      array (
        2 => NULL,
      ),
      'to_arg_functions' => '',
      'access_callback' => 'user_access',
      'access_arguments' => 'a:1:{i:0;s:14:"search content";}',
      'page_callback' => 'apachesolr_search_custom_page',
      'page_arguments' => 'a:3:{i:0;s:11:"core_search";i:1;i:2;i:2;b:0;}',
      'delivery_callback' => '',
      'tab_parent' => 'search/site',
      'tab_root' => 'search/site/%',
      'title' => 'Asia/Pacific',
      'title_callback' => 't',
      'title_arguments' => '',
      'theme_callback' => '',
      'theme_arguments' => 'a:0:{}',
      'type' => '132',
      'description' => '',
      'in_active_trail' => false,
      'href' => 'search/site/',
      'access' => true,
      'localized_options' => 
      array (
        'query' => 
        array (
          'f[0]' => 'im_field_geography:101',
        ),
        'attributes' => 
        array (
          'title' => '',
        ),
      ),
    ),
    5 => 
    array (
      'menu_name' => 'menu-geography',
      'mlid' => '1807',
      'plid' => '0',
      'link_path' => 'search/site/',
      'router_path' => 'search/site/%',
      'link_title' => 'Americas',
      'options' => 
      array (
        'query' => 
        array (
          'f[0]' => 'im_field_geography:95',
        ),
        'attributes' => 
        array (
          'title' => '',
        ),
      ),
      'module' => 'menu',
      'hidden' => '0',
      'external' => '0',
      'has_children' => '0',
      'expanded' => '0',
      'weight' => '-45',
      'depth' => '1',
      'customized' => '1',
      'p1' => '1807',
      'p2' => '0',
      'p3' => '0',
      'p4' => '0',
      'p5' => '0',
      'p6' => '0',
      'p7' => '0',
      'p8' => '0',
      'p9' => '0',
      'updated' => '0',
      'load_functions' => 
      array (
        2 => NULL,
      ),
      'to_arg_functions' => '',
      'access_callback' => 'user_access',
      'access_arguments' => 'a:1:{i:0;s:14:"search content";}',
      'page_callback' => 'apachesolr_search_custom_page',
      'page_arguments' => 'a:3:{i:0;s:11:"core_search";i:1;i:2;i:2;b:0;}',
      'delivery_callback' => '',
      'tab_parent' => 'search/site',
      'tab_root' => 'search/site/%',
      'title' => 'Americas',
      'title_callback' => 't',
      'title_arguments' => '',
      'theme_callback' => '',
      'theme_arguments' => 'a:0:{}',
      'type' => '132',
      'description' => '',
      'in_active_trail' => false,
      'href' => 'search/site/',
      'access' => true,
      'localized_options' => 
      array (
        'query' => 
        array (
          'f[0]' => 'im_field_geography:95',
        ),
        'attributes' => 
        array (
          'title' => '',
        ),
      ),
    ),
  ),
);