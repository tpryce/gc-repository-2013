<?php
$allargs = arg(3);
if(strpos($allargs, ',') !== false){
  $allargs = explode(',', $allargs);
}
elseif(strpos($allargs, ' ') !== false){
  $allargs = explode(' ', $allargs);
}
elseif(strpos($allargs, '%2B') !== false){
  $allargs = explode('%2B', $allargs);
}
elseif(strpos($allargs, '+') !== false){
  $allargs = explode('+', $allargs);
}

$query = '/search/site/?f';
$solrkey = 0;
if (is_array($allargs)) {
  foreach($allargs as $item){
    $tax_tree = taxonomy_get_parents_all($item);
    array_flip($tax_tree);
    foreach ($tax_tree as $parent){
      if($solrkey == 0) {
        $query .= '[' . $solrkey . ']=im_field_' . $parent->vocabulary_machine_name . '%3A' . $parent->tid;
      }
      else{
        $query .= '&f[' . $solrkey . ']=im_field_' . $parent->vocabulary_machine_name . '%3A' . $parent->tid;
      }
      $solrkey++;
    }
  }
}
else{
  $tax_term = taxonomy_get_parents_all($allargs);
  $query .= '[' . $solrkey . ']=im_field_' . $tax_term[0]->vocabulary_machine_name . '%3A' . $tax_term[0]->tid;
}
?>
<div class="spotlight_search"><a href="<?php print $query;?>">Search within this spotlight</a></div>
<div<?php print $attributes; ?>>
  <div<?php print $content_attributes; ?>>
    <a id="main-content"></a>
    <?php print render($title_prefix); ?>
    <?php if ($title): ?>
    <?php if ($title_hidden): ?><div class="element-invisible"><?php endif; ?>
    <h1 class="title" id="page-title"><?php print $title; ?></h1>
    <?php if ($title_hidden): ?></div><?php endif; ?>
    <?php endif; ?>
    <?php print render($title_suffix); ?>
    <div class="spotlight_back"><?php print l('Back to spotlight', 'taxonomy/term/' . arg(3));?></div>
    <?php if ($tabs && !empty($tabs['#primary'])): ?><div class="tabs clearfix"><?php print render($tabs); ?></div><?php endif; ?>
    <?php if ($action_links): ?><ul class="action-links"><?php print render($action_links); ?></ul><?php endif; ?>
    <?php print $content; ?>
    <?php if ($feed_icons): ?><div class="feed-icon clearfix"><?php print $feed_icons; ?></div><?php endif; ?>
  </div>
</div>