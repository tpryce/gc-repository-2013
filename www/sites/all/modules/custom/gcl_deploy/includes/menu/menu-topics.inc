<?php
$menu = array (
  'menu_name' => 'menu-topics',
  'title' => 'Topics',
  'description' => '',
  'items' => 
  array (
    0 => 
    array (
      'menu_name' => 'menu-topics',
      'mlid' => '1809',
      'plid' => '0',
      'link_path' => 'search/site/',
      'router_path' => 'search/site/%',
      'link_title' => 'Advertising Regulations',
      'options' => 
      array (
        'query' => 
        array (
          'f[0]' => 'im_field_topics:304',
        ),
        'attributes' => 
        array (
          'title' => '',
        ),
      ),
      'module' => 'menu',
      'hidden' => '0',
      'external' => '0',
      'has_children' => '0',
      'expanded' => '0',
      'weight' => '0',
      'depth' => '1',
      'customized' => '1',
      'p1' => '1809',
      'p2' => '0',
      'p3' => '0',
      'p4' => '0',
      'p5' => '0',
      'p6' => '0',
      'p7' => '0',
      'p8' => '0',
      'p9' => '0',
      'updated' => '0',
      'load_functions' => 
      array (
        2 => NULL,
      ),
      'to_arg_functions' => '',
      'access_callback' => 'user_access',
      'access_arguments' => 'a:1:{i:0;s:14:"search content";}',
      'page_callback' => 'apachesolr_search_custom_page',
      'page_arguments' => 'a:3:{i:0;s:11:"core_search";i:1;i:2;i:2;b:0;}',
      'delivery_callback' => '',
      'tab_parent' => 'search/site',
      'tab_root' => 'search/site/%',
      'title' => 'Advertising Regulations',
      'title_callback' => 't',
      'title_arguments' => '',
      'theme_callback' => '',
      'theme_arguments' => 'a:0:{}',
      'type' => '132',
      'description' => '',
      'in_active_trail' => false,
      'href' => 'search/site/',
      'access' => true,
      'localized_options' => 
      array (
        'query' => 
        array (
          'f[0]' => 'im_field_topics:304',
        ),
        'attributes' => 
        array (
          'title' => '',
        ),
      ),
    ),
    1 => 
    array (
      'menu_name' => 'menu-topics',
      'mlid' => '1810',
      'plid' => '0',
      'link_path' => 'search/site/',
      'router_path' => 'search/site/%',
      'link_title' => 'Case Law',
      'options' => 
      array (
        'query' => 
        array (
          'f[0]' => 'im_field_topics:386',
        ),
        'attributes' => 
        array (
          'title' => '',
        ),
      ),
      'module' => 'menu',
      'hidden' => '0',
      'external' => '0',
      'has_children' => '0',
      'expanded' => '0',
      'weight' => '0',
      'depth' => '1',
      'customized' => '1',
      'p1' => '1810',
      'p2' => '0',
      'p3' => '0',
      'p4' => '0',
      'p5' => '0',
      'p6' => '0',
      'p7' => '0',
      'p8' => '0',
      'p9' => '0',
      'updated' => '0',
      'load_functions' => 
      array (
        2 => NULL,
      ),
      'to_arg_functions' => '',
      'access_callback' => 'user_access',
      'access_arguments' => 'a:1:{i:0;s:14:"search content";}',
      'page_callback' => 'apachesolr_search_custom_page',
      'page_arguments' => 'a:3:{i:0;s:11:"core_search";i:1;i:2;i:2;b:0;}',
      'delivery_callback' => '',
      'tab_parent' => 'search/site',
      'tab_root' => 'search/site/%',
      'title' => 'Case Law',
      'title_callback' => 't',
      'title_arguments' => '',
      'theme_callback' => '',
      'theme_arguments' => 'a:0:{}',
      'type' => '132',
      'description' => '',
      'in_active_trail' => false,
      'href' => 'search/site/',
      'access' => true,
      'localized_options' => 
      array (
        'query' => 
        array (
          'f[0]' => 'im_field_topics:386',
        ),
        'attributes' => 
        array (
          'title' => '',
        ),
      ),
    ),
    2 => 
    array (
      'menu_name' => 'menu-topics',
      'mlid' => '1811',
      'plid' => '0',
      'link_path' => 'search/site/',
      'router_path' => 'search/site/%',
      'link_title' => 'Deal Announcements',
      'options' => 
      array (
        'query' => 
        array (
          'f[0]' => 'im_field_topics:6440',
        ),
        'attributes' => 
        array (
          'title' => '',
        ),
      ),
      'module' => 'menu',
      'hidden' => '0',
      'external' => '0',
      'has_children' => '0',
      'expanded' => '0',
      'weight' => '0',
      'depth' => '1',
      'customized' => '1',
      'p1' => '1811',
      'p2' => '0',
      'p3' => '0',
      'p4' => '0',
      'p5' => '0',
      'p6' => '0',
      'p7' => '0',
      'p8' => '0',
      'p9' => '0',
      'updated' => '0',
      'load_functions' => 
      array (
        2 => NULL,
      ),
      'to_arg_functions' => '',
      'access_callback' => 'user_access',
      'access_arguments' => 'a:1:{i:0;s:14:"search content";}',
      'page_callback' => 'apachesolr_search_custom_page',
      'page_arguments' => 'a:3:{i:0;s:11:"core_search";i:1;i:2;i:2;b:0;}',
      'delivery_callback' => '',
      'tab_parent' => 'search/site',
      'tab_root' => 'search/site/%',
      'title' => 'Deal Announcements',
      'title_callback' => 't',
      'title_arguments' => '',
      'theme_callback' => '',
      'theme_arguments' => 'a:0:{}',
      'type' => '132',
      'description' => '',
      'in_active_trail' => false,
      'href' => 'search/site/',
      'access' => true,
      'localized_options' => 
      array (
        'query' => 
        array (
          'f[0]' => 'im_field_topics:6440',
        ),
        'attributes' => 
        array (
          'title' => '',
        ),
      ),
    ),
    3 => 
    array (
      'menu_name' => 'menu-topics',
      'mlid' => '1812',
      'plid' => '0',
      'link_path' => 'search/site/',
      'router_path' => 'search/site/%',
      'link_title' => 'EU Developments',
      'options' => 
      array (
        'query' => 
        array (
          'f[0]' => 'im_field_topics:305',
        ),
        'attributes' => 
        array (
          'title' => '',
        ),
      ),
      'module' => 'menu',
      'hidden' => '0',
      'external' => '0',
      'has_children' => '0',
      'expanded' => '0',
      'weight' => '0',
      'depth' => '1',
      'customized' => '1',
      'p1' => '1812',
      'p2' => '0',
      'p3' => '0',
      'p4' => '0',
      'p5' => '0',
      'p6' => '0',
      'p7' => '0',
      'p8' => '0',
      'p9' => '0',
      'updated' => '0',
      'load_functions' => 
      array (
        2 => NULL,
      ),
      'to_arg_functions' => '',
      'access_callback' => 'user_access',
      'access_arguments' => 'a:1:{i:0;s:14:"search content";}',
      'page_callback' => 'apachesolr_search_custom_page',
      'page_arguments' => 'a:3:{i:0;s:11:"core_search";i:1;i:2;i:2;b:0;}',
      'delivery_callback' => '',
      'tab_parent' => 'search/site',
      'tab_root' => 'search/site/%',
      'title' => 'EU Developments',
      'title_callback' => 't',
      'title_arguments' => '',
      'theme_callback' => '',
      'theme_arguments' => 'a:0:{}',
      'type' => '132',
      'description' => '',
      'in_active_trail' => false,
      'href' => 'search/site/',
      'access' => true,
      'localized_options' => 
      array (
        'query' => 
        array (
          'f[0]' => 'im_field_topics:305',
        ),
        'attributes' => 
        array (
          'title' => '',
        ),
      ),
    ),
    4 => 
    array (
      'menu_name' => 'menu-topics',
      'mlid' => '1813',
      'plid' => '0',
      'link_path' => 'search/site/',
      'router_path' => 'search/site/%',
      'link_title' => 'Fraud/Money Laundering',
      'options' => 
      array (
        'query' => 
        array (
          'f[0]' => 'im_field_topics:306',
        ),
        'attributes' => 
        array (
          'title' => '',
        ),
      ),
      'module' => 'menu',
      'hidden' => '0',
      'external' => '0',
      'has_children' => '0',
      'expanded' => '0',
      'weight' => '0',
      'depth' => '1',
      'customized' => '1',
      'p1' => '1813',
      'p2' => '0',
      'p3' => '0',
      'p4' => '0',
      'p5' => '0',
      'p6' => '0',
      'p7' => '0',
      'p8' => '0',
      'p9' => '0',
      'updated' => '0',
      'load_functions' => 
      array (
        2 => NULL,
      ),
      'to_arg_functions' => '',
      'access_callback' => 'user_access',
      'access_arguments' => 'a:1:{i:0;s:14:"search content";}',
      'page_callback' => 'apachesolr_search_custom_page',
      'page_arguments' => 'a:3:{i:0;s:11:"core_search";i:1;i:2;i:2;b:0;}',
      'delivery_callback' => '',
      'tab_parent' => 'search/site',
      'tab_root' => 'search/site/%',
      'title' => 'Fraud/Money Laundering',
      'title_callback' => 't',
      'title_arguments' => '',
      'theme_callback' => '',
      'theme_arguments' => 'a:0:{}',
      'type' => '132',
      'description' => '',
      'in_active_trail' => false,
      'href' => 'search/site/',
      'access' => true,
      'localized_options' => 
      array (
        'query' => 
        array (
          'f[0]' => 'im_field_topics:306',
        ),
        'attributes' => 
        array (
          'title' => '',
        ),
      ),
    ),
    5 => 
    array (
      'menu_name' => 'menu-topics',
      'mlid' => '1816',
      'plid' => '0',
      'link_path' => 'search/site/',
      'router_path' => 'search/site/%',
      'link_title' => 'IPO',
      'options' => 
      array (
        'query' => 
        array (
          'f[0]' => 'im_field_topics:6437',
        ),
        'attributes' => 
        array (
          'title' => '',
        ),
      ),
      'module' => 'menu',
      'hidden' => '0',
      'external' => '0',
      'has_children' => '0',
      'expanded' => '0',
      'weight' => '0',
      'depth' => '1',
      'customized' => '1',
      'p1' => '1816',
      'p2' => '0',
      'p3' => '0',
      'p4' => '0',
      'p5' => '0',
      'p6' => '0',
      'p7' => '0',
      'p8' => '0',
      'p9' => '0',
      'updated' => '0',
      'load_functions' => 
      array (
        2 => NULL,
      ),
      'to_arg_functions' => '',
      'access_callback' => 'user_access',
      'access_arguments' => 'a:1:{i:0;s:14:"search content";}',
      'page_callback' => 'apachesolr_search_custom_page',
      'page_arguments' => 'a:3:{i:0;s:11:"core_search";i:1;i:2;i:2;b:0;}',
      'delivery_callback' => '',
      'tab_parent' => 'search/site',
      'tab_root' => 'search/site/%',
      'title' => 'IPO',
      'title_callback' => 't',
      'title_arguments' => '',
      'theme_callback' => '',
      'theme_arguments' => 'a:0:{}',
      'type' => '132',
      'description' => '',
      'in_active_trail' => false,
      'href' => 'search/site/',
      'access' => true,
      'localized_options' => 
      array (
        'query' => 
        array (
          'f[0]' => 'im_field_topics:6437',
        ),
        'attributes' => 
        array (
          'title' => '',
        ),
      ),
    ),
    6 => 
    array (
      'menu_name' => 'menu-topics',
      'mlid' => '1814',
      'plid' => '0',
      'link_path' => 'search/site/',
      'router_path' => 'search/site/%',
      'link_title' => 'Identity Verification',
      'options' => 
      array (
        'query' => 
        array (
          'f[0]' => 'im_field_topics:392',
        ),
        'attributes' => 
        array (
          'title' => '',
        ),
      ),
      'module' => 'menu',
      'hidden' => '0',
      'external' => '0',
      'has_children' => '0',
      'expanded' => '0',
      'weight' => '0',
      'depth' => '1',
      'customized' => '1',
      'p1' => '1814',
      'p2' => '0',
      'p3' => '0',
      'p4' => '0',
      'p5' => '0',
      'p6' => '0',
      'p7' => '0',
      'p8' => '0',
      'p9' => '0',
      'updated' => '0',
      'load_functions' => 
      array (
        2 => NULL,
      ),
      'to_arg_functions' => '',
      'access_callback' => 'user_access',
      'access_arguments' => 'a:1:{i:0;s:14:"search content";}',
      'page_callback' => 'apachesolr_search_custom_page',
      'page_arguments' => 'a:3:{i:0;s:11:"core_search";i:1;i:2;i:2;b:0;}',
      'delivery_callback' => '',
      'tab_parent' => 'search/site',
      'tab_root' => 'search/site/%',
      'title' => 'Identity Verification',
      'title_callback' => 't',
      'title_arguments' => '',
      'theme_callback' => '',
      'theme_arguments' => 'a:0:{}',
      'type' => '132',
      'description' => '',
      'in_active_trail' => false,
      'href' => 'search/site/',
      'access' => true,
      'localized_options' => 
      array (
        'query' => 
        array (
          'f[0]' => 'im_field_topics:392',
        ),
        'attributes' => 
        array (
          'title' => '',
        ),
      ),
    ),
    7 => 
    array (
      'menu_name' => 'menu-topics',
      'mlid' => '1815',
      'plid' => '0',
      'link_path' => 'search/site/',
      'router_path' => 'search/site/%',
      'link_title' => 'Intellectual Property',
      'options' => 
      array (
        'query' => 
        array (
          'f[0]' => 'im_field_topics:6441',
        ),
        'attributes' => 
        array (
          'title' => '',
        ),
      ),
      'module' => 'menu',
      'hidden' => '0',
      'external' => '0',
      'has_children' => '0',
      'expanded' => '0',
      'weight' => '0',
      'depth' => '1',
      'customized' => '1',
      'p1' => '1815',
      'p2' => '0',
      'p3' => '0',
      'p4' => '0',
      'p5' => '0',
      'p6' => '0',
      'p7' => '0',
      'p8' => '0',
      'p9' => '0',
      'updated' => '0',
      'load_functions' => 
      array (
        2 => NULL,
      ),
      'to_arg_functions' => '',
      'access_callback' => 'user_access',
      'access_arguments' => 'a:1:{i:0;s:14:"search content";}',
      'page_callback' => 'apachesolr_search_custom_page',
      'page_arguments' => 'a:3:{i:0;s:11:"core_search";i:1;i:2;i:2;b:0;}',
      'delivery_callback' => '',
      'tab_parent' => 'search/site',
      'tab_root' => 'search/site/%',
      'title' => 'Intellectual Property',
      'title_callback' => 't',
      'title_arguments' => '',
      'theme_callback' => '',
      'theme_arguments' => 'a:0:{}',
      'type' => '132',
      'description' => '',
      'in_active_trail' => false,
      'href' => 'search/site/',
      'access' => true,
      'localized_options' => 
      array (
        'query' => 
        array (
          'f[0]' => 'im_field_topics:6441',
        ),
        'attributes' => 
        array (
          'title' => '',
        ),
      ),
    ),
    8 => 
    array (
      'menu_name' => 'menu-topics',
      'mlid' => '1817',
      'plid' => '0',
      'link_path' => 'search/site/',
      'router_path' => 'search/site/%',
      'link_title' => 'M&A',
      'options' => 
      array (
        'query' => 
        array (
          'f[0]' => 'im_field_topics:6438',
        ),
        'attributes' => 
        array (
          'title' => '',
        ),
      ),
      'module' => 'menu',
      'hidden' => '0',
      'external' => '0',
      'has_children' => '0',
      'expanded' => '0',
      'weight' => '0',
      'depth' => '1',
      'customized' => '1',
      'p1' => '1817',
      'p2' => '0',
      'p3' => '0',
      'p4' => '0',
      'p5' => '0',
      'p6' => '0',
      'p7' => '0',
      'p8' => '0',
      'p9' => '0',
      'updated' => '0',
      'load_functions' => 
      array (
        2 => NULL,
      ),
      'to_arg_functions' => '',
      'access_callback' => 'user_access',
      'access_arguments' => 'a:1:{i:0;s:14:"search content";}',
      'page_callback' => 'apachesolr_search_custom_page',
      'page_arguments' => 'a:3:{i:0;s:11:"core_search";i:1;i:2;i:2;b:0;}',
      'delivery_callback' => '',
      'tab_parent' => 'search/site',
      'tab_root' => 'search/site/%',
      'title' => 'M&A',
      'title_callback' => 't',
      'title_arguments' => '',
      'theme_callback' => '',
      'theme_arguments' => 'a:0:{}',
      'type' => '132',
      'description' => '',
      'in_active_trail' => false,
      'href' => 'search/site/',
      'access' => true,
      'localized_options' => 
      array (
        'query' => 
        array (
          'f[0]' => 'im_field_topics:6438',
        ),
        'attributes' => 
        array (
          'title' => '',
        ),
      ),
    ),
    9 => 
    array (
      'menu_name' => 'menu-topics',
      'mlid' => '1818',
      'plid' => '0',
      'link_path' => 'search/site/',
      'router_path' => 'search/site/%',
      'link_title' => 'Machine/Technical Standards',
      'options' => 
      array (
        'query' => 
        array (
          'f[0]' => 'im_field_topics:398',
        ),
        'attributes' => 
        array (
          'title' => '',
        ),
      ),
      'module' => 'menu',
      'hidden' => '0',
      'external' => '0',
      'has_children' => '0',
      'expanded' => '0',
      'weight' => '0',
      'depth' => '1',
      'customized' => '1',
      'p1' => '1818',
      'p2' => '0',
      'p3' => '0',
      'p4' => '0',
      'p5' => '0',
      'p6' => '0',
      'p7' => '0',
      'p8' => '0',
      'p9' => '0',
      'updated' => '0',
      'load_functions' => 
      array (
        2 => NULL,
      ),
      'to_arg_functions' => '',
      'access_callback' => 'user_access',
      'access_arguments' => 'a:1:{i:0;s:14:"search content";}',
      'page_callback' => 'apachesolr_search_custom_page',
      'page_arguments' => 'a:3:{i:0;s:11:"core_search";i:1;i:2;i:2;b:0;}',
      'delivery_callback' => '',
      'tab_parent' => 'search/site',
      'tab_root' => 'search/site/%',
      'title' => 'Machine/Technical Standards',
      'title_callback' => 't',
      'title_arguments' => '',
      'theme_callback' => '',
      'theme_arguments' => 'a:0:{}',
      'type' => '132',
      'description' => '',
      'in_active_trail' => false,
      'href' => 'search/site/',
      'access' => true,
      'localized_options' => 
      array (
        'query' => 
        array (
          'f[0]' => 'im_field_topics:398',
        ),
        'attributes' => 
        array (
          'title' => '',
        ),
      ),
    ),
    10 => 
    array (
      'menu_name' => 'menu-topics',
      'mlid' => '1819',
      'plid' => '0',
      'link_path' => 'search/site/',
      'router_path' => 'search/site/%',
      'link_title' => 'Payment Processing',
      'options' => 
      array (
        'query' => 
        array (
          'f[0]' => 'im_field_topics:401',
        ),
        'attributes' => 
        array (
          'title' => '',
        ),
      ),
      'module' => 'menu',
      'hidden' => '0',
      'external' => '0',
      'has_children' => '0',
      'expanded' => '0',
      'weight' => '0',
      'depth' => '1',
      'customized' => '1',
      'p1' => '1819',
      'p2' => '0',
      'p3' => '0',
      'p4' => '0',
      'p5' => '0',
      'p6' => '0',
      'p7' => '0',
      'p8' => '0',
      'p9' => '0',
      'updated' => '0',
      'load_functions' => 
      array (
        2 => NULL,
      ),
      'to_arg_functions' => '',
      'access_callback' => 'user_access',
      'access_arguments' => 'a:1:{i:0;s:14:"search content";}',
      'page_callback' => 'apachesolr_search_custom_page',
      'page_arguments' => 'a:3:{i:0;s:11:"core_search";i:1;i:2;i:2;b:0;}',
      'delivery_callback' => '',
      'tab_parent' => 'search/site',
      'tab_root' => 'search/site/%',
      'title' => 'Payment Processing',
      'title_callback' => 't',
      'title_arguments' => '',
      'theme_callback' => '',
      'theme_arguments' => 'a:0:{}',
      'type' => '132',
      'description' => '',
      'in_active_trail' => false,
      'href' => 'search/site/',
      'access' => true,
      'localized_options' => 
      array (
        'query' => 
        array (
          'f[0]' => 'im_field_topics:401',
        ),
        'attributes' => 
        array (
          'title' => '',
        ),
      ),
    ),
    11 => 
    array (
      'menu_name' => 'menu-topics',
      'mlid' => '1820',
      'plid' => '0',
      'link_path' => 'search/site/',
      'router_path' => 'search/site/%',
      'link_title' => 'Player Protection',
      'options' => 
      array (
        'query' => 
        array (
          'f[0]' => 'im_field_topics:307',
        ),
        'attributes' => 
        array (
          'title' => '',
        ),
      ),
      'module' => 'menu',
      'hidden' => '0',
      'external' => '0',
      'has_children' => '0',
      'expanded' => '0',
      'weight' => '0',
      'depth' => '1',
      'customized' => '1',
      'p1' => '1820',
      'p2' => '0',
      'p3' => '0',
      'p4' => '0',
      'p5' => '0',
      'p6' => '0',
      'p7' => '0',
      'p8' => '0',
      'p9' => '0',
      'updated' => '0',
      'load_functions' => 
      array (
        2 => NULL,
      ),
      'to_arg_functions' => '',
      'access_callback' => 'user_access',
      'access_arguments' => 'a:1:{i:0;s:14:"search content";}',
      'page_callback' => 'apachesolr_search_custom_page',
      'page_arguments' => 'a:3:{i:0;s:11:"core_search";i:1;i:2;i:2;b:0;}',
      'delivery_callback' => '',
      'tab_parent' => 'search/site',
      'tab_root' => 'search/site/%',
      'title' => 'Player Protection',
      'title_callback' => 't',
      'title_arguments' => '',
      'theme_callback' => '',
      'theme_arguments' => 'a:0:{}',
      'type' => '132',
      'description' => '',
      'in_active_trail' => false,
      'href' => 'search/site/',
      'access' => true,
      'localized_options' => 
      array (
        'query' => 
        array (
          'f[0]' => 'im_field_topics:307',
        ),
        'attributes' => 
        array (
          'title' => '',
        ),
      ),
    ),
    12 => 
    array (
      'menu_name' => 'menu-topics',
      'mlid' => '1821',
      'plid' => '0',
      'link_path' => 'search/site/',
      'router_path' => 'search/site/%',
      'link_title' => 'Private Equity/VC',
      'options' => 
      array (
        'query' => 
        array (
          'f[0]' => 'im_field_topics:6439',
        ),
        'attributes' => 
        array (
          'title' => '',
        ),
      ),
      'module' => 'menu',
      'hidden' => '0',
      'external' => '0',
      'has_children' => '0',
      'expanded' => '0',
      'weight' => '0',
      'depth' => '1',
      'customized' => '1',
      'p1' => '1821',
      'p2' => '0',
      'p3' => '0',
      'p4' => '0',
      'p5' => '0',
      'p6' => '0',
      'p7' => '0',
      'p8' => '0',
      'p9' => '0',
      'updated' => '0',
      'load_functions' => 
      array (
        2 => NULL,
      ),
      'to_arg_functions' => '',
      'access_callback' => 'user_access',
      'access_arguments' => 'a:1:{i:0;s:14:"search content";}',
      'page_callback' => 'apachesolr_search_custom_page',
      'page_arguments' => 'a:3:{i:0;s:11:"core_search";i:1;i:2;i:2;b:0;}',
      'delivery_callback' => '',
      'tab_parent' => 'search/site',
      'tab_root' => 'search/site/%',
      'title' => 'Private Equity/VC',
      'title_callback' => 't',
      'title_arguments' => '',
      'theme_callback' => '',
      'theme_arguments' => 'a:0:{}',
      'type' => '132',
      'description' => '',
      'in_active_trail' => false,
      'href' => 'search/site/',
      'access' => true,
      'localized_options' => 
      array (
        'query' => 
        array (
          'f[0]' => 'im_field_topics:6439',
        ),
        'attributes' => 
        array (
          'title' => '',
        ),
      ),
    ),
    13 => 
    array (
      'menu_name' => 'menu-topics',
      'mlid' => '1822',
      'plid' => '0',
      'link_path' => 'search/site/',
      'router_path' => 'search/site/%',
      'link_title' => 'Sports Law',
      'options' => 
      array (
        'query' => 
        array (
          'f[0]' => 'im_field_topics:4085',
        ),
        'attributes' => 
        array (
          'title' => '',
        ),
      ),
      'module' => 'menu',
      'hidden' => '0',
      'external' => '0',
      'has_children' => '0',
      'expanded' => '0',
      'weight' => '0',
      'depth' => '1',
      'customized' => '1',
      'p1' => '1822',
      'p2' => '0',
      'p3' => '0',
      'p4' => '0',
      'p5' => '0',
      'p6' => '0',
      'p7' => '0',
      'p8' => '0',
      'p9' => '0',
      'updated' => '0',
      'load_functions' => 
      array (
        2 => NULL,
      ),
      'to_arg_functions' => '',
      'access_callback' => 'user_access',
      'access_arguments' => 'a:1:{i:0;s:14:"search content";}',
      'page_callback' => 'apachesolr_search_custom_page',
      'page_arguments' => 'a:3:{i:0;s:11:"core_search";i:1;i:2;i:2;b:0;}',
      'delivery_callback' => '',
      'tab_parent' => 'search/site',
      'tab_root' => 'search/site/%',
      'title' => 'Sports Law',
      'title_callback' => 't',
      'title_arguments' => '',
      'theme_callback' => '',
      'theme_arguments' => 'a:0:{}',
      'type' => '132',
      'description' => '',
      'in_active_trail' => false,
      'href' => 'search/site/',
      'access' => true,
      'localized_options' => 
      array (
        'query' => 
        array (
          'f[0]' => 'im_field_topics:4085',
        ),
        'attributes' => 
        array (
          'title' => '',
        ),
      ),
    ),
    14 => 
    array (
      'menu_name' => 'menu-topics',
      'mlid' => '1823',
      'plid' => '0',
      'link_path' => 'search/site/',
      'router_path' => 'search/site/%',
      'link_title' => 'Tax',
      'options' => 
      array (
        'query' => 
        array (
          'f[0]' => 'im_field_topics:371',
        ),
        'attributes' => 
        array (
          'title' => '',
        ),
      ),
      'module' => 'menu',
      'hidden' => '0',
      'external' => '0',
      'has_children' => '0',
      'expanded' => '0',
      'weight' => '0',
      'depth' => '1',
      'customized' => '1',
      'p1' => '1823',
      'p2' => '0',
      'p3' => '0',
      'p4' => '0',
      'p5' => '0',
      'p6' => '0',
      'p7' => '0',
      'p8' => '0',
      'p9' => '0',
      'updated' => '0',
      'load_functions' => 
      array (
        2 => NULL,
      ),
      'to_arg_functions' => '',
      'access_callback' => 'user_access',
      'access_arguments' => 'a:1:{i:0;s:14:"search content";}',
      'page_callback' => 'apachesolr_search_custom_page',
      'page_arguments' => 'a:3:{i:0;s:11:"core_search";i:1;i:2;i:2;b:0;}',
      'delivery_callback' => '',
      'tab_parent' => 'search/site',
      'tab_root' => 'search/site/%',
      'title' => 'Tax',
      'title_callback' => 't',
      'title_arguments' => '',
      'theme_callback' => '',
      'theme_arguments' => 'a:0:{}',
      'type' => '132',
      'description' => '',
      'in_active_trail' => false,
      'href' => 'search/site/',
      'access' => true,
      'localized_options' => 
      array (
        'query' => 
        array (
          'f[0]' => 'im_field_topics:371',
        ),
        'attributes' => 
        array (
          'title' => '',
        ),
      ),
    ),
    15 => 
    array (
      'menu_name' => 'menu-topics',
      'mlid' => '1824',
      'plid' => '0',
      'link_path' => 'search/site/',
      'router_path' => 'search/site/%',
      'link_title' => 'Trading Statement',
      'options' => 
      array (
        'query' => 
        array (
          'f[0]' => 'im_field_topics:6436',
        ),
        'attributes' => 
        array (
          'title' => '',
        ),
      ),
      'module' => 'menu',
      'hidden' => '0',
      'external' => '0',
      'has_children' => '0',
      'expanded' => '0',
      'weight' => '0',
      'depth' => '1',
      'customized' => '1',
      'p1' => '1824',
      'p2' => '0',
      'p3' => '0',
      'p4' => '0',
      'p5' => '0',
      'p6' => '0',
      'p7' => '0',
      'p8' => '0',
      'p9' => '0',
      'updated' => '0',
      'load_functions' => 
      array (
        2 => NULL,
      ),
      'to_arg_functions' => '',
      'access_callback' => 'user_access',
      'access_arguments' => 'a:1:{i:0;s:14:"search content";}',
      'page_callback' => 'apachesolr_search_custom_page',
      'page_arguments' => 'a:3:{i:0;s:11:"core_search";i:1;i:2;i:2;b:0;}',
      'delivery_callback' => '',
      'tab_parent' => 'search/site',
      'tab_root' => 'search/site/%',
      'title' => 'Trading Statement',
      'title_callback' => 't',
      'title_arguments' => '',
      'theme_callback' => '',
      'theme_arguments' => 'a:0:{}',
      'type' => '132',
      'description' => '',
      'in_active_trail' => false,
      'href' => 'search/site/',
      'access' => true,
      'localized_options' => 
      array (
        'query' => 
        array (
          'f[0]' => 'im_field_topics:6436',
        ),
        'attributes' => 
        array (
          'title' => '',
        ),
      ),
    ),
  ),
);