<?php
/**
 * SolrSearchSaved class
 */

class SolrSearchSaved
{
    protected $sid;
    protected $uid;
    protected $title;
    protected $text;
    protected $panel = 0;
    protected $panel_column = 1;
    protected $panel_order = 0;
    protected $params = array();

    /**
     * Checks if current page is a search results page
     *
     * @static
     * @return bool true if current page is search results page
     */
    static function isSearchPage()
    {
      $env_id = apachesolr_default_environment();
			$query = apachesolr_current_query($env_id);
  		$has_filter = ($query===NULL) ? false : (bool)count($query->getFilters());
      return (bool)(arg(0) == 'search' && arg(1) == 'site' && (arg(2) || $has_filter));
    }

    /**
     * Initiates saved search object from page page parameters
     * @static
     * @return bool|solr_search_saved
     */
    static public function init()
    {
        if (SolrSearchSaved::isSearchPage()) {
            $search = new SolrSearchSaved();
      			$search->title = '';
            $params = array();
            $keys = trim(arg(2));
            // Also try to pull search keywords out of the $_REQUEST variable to
            // support old GET format of searches for existing links.
            if (!$keys && !empty($_REQUEST['keys'])) {
                $keys = trim($_REQUEST['keys']);
            }
            if ($keys) {
                $params['keys'] = $keys;
        				$search->title = $keys;
				//$keys_in_title = true;
            }
			else{
				//$keys_in_title = false;
			}
      $env_id = apachesolr_default_environment();
			$query = apachesolr_current_query($env_id);
			
			$all_crit = ($query===NULL) ? array() : $query->getFilters();
        if (count($all_crit)>0) {
				foreach ($all_crit as $crit){
					$params['f'][] = $crit['#name'] . ':' . $crit['#value'];
					//if (!$keys_in_title){
						if ($crit['#name']=='bundle'){
							$types = node_type_get_names();
							$search->title .= ' ' . $types[$crit['#value']];
						}
						else{
							$term = taxonomy_term_load($crit['#value']);
							$search->title .= ' ' . $term->name;
						}
						//$keys_in_title = true;
					//}
				}
            }

            if (isset($_GET['solrsort']) && $_GET['solrsort']) {
                $params['solrsort'] = $_GET['solrsort'];
            }
            $search->setParams($params);
            return $search;
        }
        return false;
    }

    /**
     * Saves current object in the database
     * @return void
     */
    public function save()
    {

        if ($this->sid) {
            $query = db_update(SOLR_SEARCH_SAVED_TABLE_NAME);
            $query->condition('sid', $this->sid);
        }
        else {
            $query = db_insert(SOLR_SEARCH_SAVED_TABLE_NAME);
        }

        $query->fields(array(
                            'uid' => $this->getUid(),
                            'title' => $this->getTitle(),
                            'text' => $this->getText(),
                            'panel' => $this->getPanel(),
                            'panel_column' => $this->getPanelColumn(),
                            'panel_order' => $this->getPanelOrder(),
                       ));
        $res = $query->execute();
        return $res;
    }

    public function setPanel($panel)
    {
        $this->panel = $panel;
    }

    public function getPanel()
    {
        return $this->panel;
    }

    public function setPanelColumn($panel_column)
    {
        $this->panel_column = $panel_column;
    }

    public function getPanelColumn()
    {
        return $this->panel_column;
    }

    public function setPanelOrder($panel_order)
    {
        $this->panel_order = $panel_order;
    }

    public function getPanelOrder()
    {
        return $this->panel_order;
    }

    public function setParams($params)
    {
        $this->params = $params;
        $this->text = serialize($this->params);
    }

    public function getParams()
    {
        return $this->params;
    }

    public function getSid()
    {
        return $this->sid;
    }


    public function getText()
    {
        return $this->text;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    public function getUid()
    {
        return $this->uid;
    }

    /**
     * Returns one saved search according to conditions
     * False if an object has not been found
     * @static
     * @param null $uid
     * @param null $sid
     * @param null $title
     * @param null $text
     * @param null $condition
     * @param string $order_by
     * @return bool
     */
    public static function findOne($uid = NULL, $sid = NULL, $title = NULL, $text = NULL, $condition = NULL, $order_by = 'title')
    {
        $searches = SolrSearchSaved::find($uid, $sid, $title, $text, $condition, $order_by, 1);
        if (isset($searches[0])) {
            return $searches[0];
        }
        return false;
    }

    /**
     * Returns array with saved searches by conditions
     * @static
     * @param null $uid
     * @param null $sid
     * @param null $title
     * @param null $text
     * @param null $condition
     * @param string $order_by
     * @param null $limit
     * @return array
     */
    public static function find($uid = NULL, $sid = NULL, $title = NULL, $text = NULL, array $conditions = NULL, $order_by = 'title', $limit = NULL)
    {
        $query = db_select(SOLR_SEARCH_SAVED_TABLE_NAME, 's');
        $query->fields('s');
        if ($uid) {
            $query->condition('uid', $uid);
        }
        if ($sid) {
            $query->condition('sid', $sid);
        }
        if ($title) {
            $query->condition('title', $title, 'LIKE');
        }
        if ($text) {
            $query->condition('text', $text, 'LIKE');
        }

        if ($conditions) {
            foreach ($conditions as $condition)
            {
                list($field, $value, $operator) = $condition;
                $query->condition($field, $value, $operator);
            }
        }

        if ($order_by) {
            $query->orderBy($order_by);
        }
        if ($limit) {
            $query->range(0, $limit);
        }

        $result = $query->execute();
        $searches = array();
        while ($row = $result->fetchAssoc()) {
            $search = new SolrSearchSaved();
            $search->_fromDb($row);
            $searches[] = $search;
        }
        return $searches;
    }

    /**
     * Fills is objects values from database results
     * @param array $data
     * @return void
     */
    protected function _fromDb(array $data)
    {
        foreach ($data as $key => $value) {
            if (property_exists($this, $key)) {
                $this->$key = $value;
            }
        }
        $this->params = unserialize($this->text);
    }

    /**
     * Returns path to repeat the search
     * @return array
     */
    public function getPath()
    {
        $params = $this->getParams();
        $path = 'search/site/';
        if (isset($params['keys'])) {
            $path .= $params['keys'];
        }
        $query = array();
        if (isset($params['f'])) {
			foreach ($params['f'] as $f){
            	//$query['f'][] = array('filters' => $params['filters']);
            	$query['f'][] = $f;
			}
        }
        if (isset($params['solrsort'])) {
            $query[] = array('solrsort' => $params['solrsort']);
        }
        return array('path' => $path, 'query' => $query);
    }

    /**
     * Deletes  saved search from the database
     * @return void
     */
    public function delete()
    {
        $query = db_delete(SOLR_SEARCH_SAVED_TABLE_NAME);
        $query->condition('sid', $this->getSid());
        $query->execute();
    }

    public function execute($rows = 5)
    {
		if (function_exists('_solr_is_available') && _solr_is_available()===FALSE) return;
        $conditions = $this->getParams();

        //$filters = isset($conditions['f']) ? $conditions['f'] : array();
        $solrsort = isset($conditions['solrsort']) ? $conditions['solrsort'] : '';
        //TODO remove hardcode
        $base_path = 'search/site';

		if (isset($params['keys'])) {
            $path .= $params['keys'];
        }

        $query = apachesolr_drupal_query('apachesolr', array(), $solrsort, $base_path);

		apachesolr_search_basic_params($query);

		if (isset($conditions['f'])) {
			foreach($conditions['f'] as $condition){
				$values = explode(':', $condition);
				$query->addFilter($values[0], $values[1]);
			}
        }
		if (isset($conditions['keys'])) {
			$query->replaceParam('q', $conditions['keys']);
		}
		$query->replaceParam('rows',$rows);
        $query->addParam('fl','teaser');

		
        //apachesolr_search_add_facet_params($query);
        apachesolr_search_add_boost_params($query);

        list($final_query, $response) = apachesolr_do_query($query);
        //TODO ??
        apachesolr_has_searched(FALSE);
        return $response;
    }
}