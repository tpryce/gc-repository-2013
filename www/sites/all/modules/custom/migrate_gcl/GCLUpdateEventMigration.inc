<?php

/**
 * @file
 * Update link_field and date fields that weren't mapped in the original import.
 *
 * This migration set the systemOfRecord to DESTINATION which means previously
 * migrated content is loaded and updated with just the fields mapped here.
 *
 * @link http://drupal.org/node/1117454 @endlink
 */

class GCLUpdateEventMigration extends Migration {
  public function __construct() {
    parent::__construct();

    $this->description = t('Update fields with missing content for the Event migration.');

    $this->systemOfRecord = Migration::DESTINATION;

    $source_key = array(
      'nid' => array(
        'type' => 'int',
        'not null' => TRUE,
        'description' => 'Nid.',
        'alias' => 'n',
      ),
    );
    $destination_key = MigrateDestinationNode::getKeySchema();
    $this->map = new MigrateSQLMap($this->machineName, $source_key, $destination_key);

    $this->destination = new MigrateDestinationNode('event');

    $query = Database::getConnection('default', 'legacy')->select('node', 'n');
    $query->join('field_data_field_date_range', 'd', "d.entity_type = 'node' AND d.bundle = 'event' AND d.entity_id = n.nid AND d.deleted <> 1");
    $query->join('field_data_field_event_link', 'l', "l.entity_type = 'node' AND d.bundle = 'event' AND l.entity_id = n.nid AND l.deleted <> 1");
    $query->fields('n', array('nid'))
      ->fields('d', array('field_date_range_value', 'field_date_range_value2'))
      ->fields('l', array('field_event_link_url', 'field_event_link_title', 'field_event_link_attributes'));
    $query->condition('n.type', 'event');
    $query->orderBy('n.nid', 'ASC');

    $this->source = new MigrateSourceSQL($query, array(), NULL, array('map_joinable' => FALSE));

    $this->addFieldMapping('nid', 'nid')->sourceMigration('Event');

    $this->addFieldMapping('field_date_range', 'field_date_range_value');
    $this->addFieldMapping('field_event_link', 'field_event_link_url');
  }

  public function prepare(stdClass $node, stdClass $source_row) {
    $field_date_range = array(
      'value' => (string) $source_row->field_date_range_value,
      'value2' => (string) $source_row->field_date_range_value2,
      'timezone' => 'Europe/London',
      'timezone_db'=> 'Europe/London',
      'date_type' => 'date',
    );

    $node->field_date_range = array();
    $node->field_date_range[LANGUAGE_NONE][0] = $field_date_range;

    $field_event_link = array(
      'url' => $source_row->field_event_link_url,
      'title' => $source_row->field_event_link_title,
      'attributes' => unserialize($source_row->field_event_link_attributes),
    );

    $node->field_event_link = array();
    $node->field_event_link[LANGUAGE_NONE][0] = $field_event_link;
  }
}
