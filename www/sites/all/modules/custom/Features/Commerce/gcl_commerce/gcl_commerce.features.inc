<?php
/**
 * @file
 * gcl_commerce.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function gcl_commerce_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_uc_product_default_classes().
 */
function gcl_commerce_uc_product_default_classes() {
  $items = array(
    'file_download' => array(
      'name' => t('File Download'),
      'base' => 'uc_product',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Name'),
      'help' => '',
    ),
    'product' => array(
      'name' => t('Market Barrier Reports'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
