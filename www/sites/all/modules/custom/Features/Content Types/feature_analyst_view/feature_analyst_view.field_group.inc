<?php
/**
 * @file
 * feature_analyst_view.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function feature_analyst_view_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_trunk_categories|node|analyst_view|default';
  $field_group->group_name = 'group_trunk_categories';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'analyst_view';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Trunk Categories',
    'weight' => '2',
    'children' => array(
      0 => 'field_content',
      1 => 'field_geography',
      2 => 'field_sectors',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'formatter' => 'open',
      'instance_settings' => array(
        'description' => '',
        'show_label' => 1,
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
        'classes' => '',
      ),
    ),
  );
  $export['group_trunk_categories|node|analyst_view|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_trunk_categories|node|analyst_view|subscription';
  $field_group->group_name = 'group_trunk_categories';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'analyst_view';
  $field_group->mode = 'subscription';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Trunk Categories',
    'weight' => '2',
    'children' => array(
      0 => 'field_content',
      1 => 'field_geography',
      2 => 'field_sectors',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'formatter' => 'open',
      'instance_settings' => array(
        'description' => '',
        'show_label' => 1,
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
        'classes' => '',
      ),
    ),
  );
  $export['group_trunk_categories|node|analyst_view|subscription'] = $field_group;

  return $export;
}
