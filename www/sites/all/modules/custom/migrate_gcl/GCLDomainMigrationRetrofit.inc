<?php

/**
 * @file
 * Retospectively migrate node domain data.
 */

/**
 * GCLDomainMigrationRetrofit class.
 *
 * Adds source domain data to migrated nodes.
 */
class GCLDomainMigrationRetrofit extends Migration {
  private $arguments;

  public function __construct(array $arguments) {
    $this->arguments = $arguments;

    parent::__construct();

    $this->description = t('This migration load nodes from the destination and updates them with domain data from the source.');

    // Load and update destination data.
    $this->systemOfRecord = Migration::DESTINATION;

    $this->setMap();
    $this->setSource();
    $this->setDestination();

    $this->addFieldMapping('nid', 'nid');
  }

  private function setMap() {
    $machine_name = $this->machineName;

    $source_key = array(
      'nid' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => 'Source nid.',
      ),
    );

    $destination_key = GCLDomainMigrationRetrofitDestination::getKeySchema();

    $this->map = new MigrateSQLMap($machine_name, $source_key, $destination_key);
  }

  private function setSource() {
    $query = Database::getConnection('default', 'legacy')
      ->select('node', 'n')
      ->fields('n', array('nid'))
      ->orderBy('n.nid');
    $this->source = new MigrateSourceSQL($query);
  }

  private function setDestination() {
    $this->destination = new GCLDomainMigrationRetrofitDestination();
  }

  public function prepareRow(stdClass $source_row) {
    if ($nid = $this->findDestinationKey($source_row->nid)) {
      $source_row->destid1 = $nid;
      return TRUE;
    }

    return FALSE;
  }

  public function prepare(stdClass $node, stdClass $source_row) {
    $source_node = node_load($source_row->destid1);
    $node->type = $source_node->type;
    $this->setDomainDefaults($node);
    $this->setDomainSource($node, $source_row);
  }

  private function setDomainDefaults(stdClass $node) {
    $node->domains = $node->domain_id = array(1 => 1, 2 => 2);
    $node->subdomains[] = t('All affiliates');
    $node->domain_site = TRUE;
  }

  private function setDomainSource(stdClass $node, stdClass $source_row) {
    $domain_source = Database::getConnection('default', 'legacy')
      ->select('domain_source', 'ds')
      ->fields('ds', array('domain_id'))
      ->condition('ds.nid', $source_row->nid)
      ->execute()
      ->fetchAssoc();

    if ($domain_source) {
      $node->domain_source = $this->transposeDomainGid($domain_source['domain_id']);
    }
    else {
      switch ($node->type) {
        case 'analyst_view':
        case 'chart':
        case 'data':
        case 'data_report':
          $node->domain_source = 2; // Data.
          break;

        default:
          $node->domain_source = 1; // Compliance
          break;
      }
    }
  }

  private function transposeDomainGid($gid) {
    $map = array(0 => 1, 1 => 2);
    return isset($map[$gid]) ? $map[$gid] : $gid;
  }

  private function findDestinationKey($source_row_nid, $map_tables = NULL) {
    if (is_null($map_tables)) {
      $map_tables = $this->getMapTables();
    }

    foreach ($map_tables as $table) {
      $query = db_select($table, 'map');
      $query->join('node', 'n', 'n.nid = map.destid1');

      $row = $query->fields('map', array('destid1'))
        ->condition('map.sourceid1', $source_row_nid)
        ->execute()
        ->fetchCol();

      if ($row) {
        return $row[0];
      }
    }

    return FALSE;
  }

  private function getMapTables() {
    static $map_tables = array();

    if (empty($map_tables)) {
      $migrations = migrate_migrations();

      foreach ($migrations as $migration) {
        $vars = get_object_vars($migration);
        if (isset($vars['arguments']['class_name']) && $vars['arguments']['class_name'] === 'GCLNodeMigration') {
          $map_tables[] = 'migrate_map_' . drupal_strtolower($vars['arguments']['machine_name']);
        }
      }
    }

    return $map_tables;
  }
}

/**
 * Migrate destination class.
 */
class GCLDomainMigrationRetrofitDestination extends MigrateDestination {

  static public function getKeySchema() {
    return array(
      'nid' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => 'Destination nid.',
      ),
    );
  }

  public function __toString() {
    return t('Migration destination for domain node data.');
  }

  public function fields() {
    return array(
      'domains' => t('Domains'),
      'domain_site' => t('Domain site'),
      'subdomains' => t('Subdomains'),
      'domain_id' => t('Domain Id'),
    );
  }

  public function import(stdClass $node, stdClass $source_row) {
    // Invoke migration prepare handlers.
    $this->prepare($node, $source_row);

    $update_node = node_load($source_row->destid1);

    $fields = array('domains', 'domain_site', 'domain_source', 'subdomains');

    foreach ($fields as $field) {
      if (isset($node->{$field})) {
        $update_node->{$field} = $node->{$field};
      }
    }

    migrate_instrument_start('node_save');
    node_save($update_node);
    migrate_instrument_stop('node_save');

    $this->numUpdated++;

    $this->complete($update_node, $source_row);
    return array($update_node->nid);
  }

  public function prepare(stdClass $node, stdClass $source_row) {
    $migration = Migration::currentMigration();
    if (method_exists($migration, 'prepare')) {
      $migration->prepare($node, $source_row);
    }
  }

  public function complete(stdClass $node, stdClass $source_row) {
    $migration = Migration::currentMigration();
    if (method_exists($migration, 'complete')) {
      try {
        $migration->complete($node, $source_row);
      }
      catch (Exception $e) {
        $migration->saveMessage($e->getMessage());
      }
    }
  }
}