<?php
$menu = array (
  'menu_name' => 'menu-sources',
  'title' => 'Sources',
  'description' => '',
  'items' => 
  array (
    0 => 
    array (
      'menu_name' => 'menu-sources',
      'mlid' => '4365',
      'plid' => '0',
      'link_path' => 'search/site/',
      'router_path' => 'search/site/%',
      'link_title' => 'Academic',
      'options' => 
      array (
        'query' => 
        array (
          'f[0]' => 'im_field_data_sources:17186',
        ),
        'attributes' => 
        array (
          'title' => '',
        ),
      ),
      'module' => 'menu',
      'hidden' => '0',
      'external' => '0',
      'has_children' => '0',
      'expanded' => '0',
      'weight' => '0',
      'depth' => '1',
      'customized' => '1',
      'p1' => '4365',
      'p2' => '0',
      'p3' => '0',
      'p4' => '0',
      'p5' => '0',
      'p6' => '0',
      'p7' => '0',
      'p8' => '0',
      'p9' => '0',
      'updated' => '0',
      'load_functions' => 
      array (
        2 => NULL,
      ),
      'to_arg_functions' => '',
      'access_callback' => 'user_access',
      'access_arguments' => 'a:1:{i:0;s:14:"search content";}',
      'page_callback' => 'apachesolr_search_custom_page',
      'page_arguments' => 'a:3:{i:0;s:11:"core_search";i:1;i:2;i:2;b:0;}',
      'delivery_callback' => '',
      'tab_parent' => 'search/site',
      'tab_root' => 'search/site/%',
      'title' => 'Academic',
      'title_callback' => 't',
      'title_arguments' => '',
      'theme_callback' => '',
      'theme_arguments' => 'a:0:{}',
      'type' => '132',
      'description' => '',
      'in_active_trail' => false,
      'href' => 'search/site/',
      'access' => true,
      'localized_options' => 
      array (
        'query' => 
        array (
          'f[0]' => 'im_field_data_sources:17186',
        ),
        'attributes' => 
        array (
          'title' => '',
        ),
      ),
    ),
    1 => 
    array (
      'menu_name' => 'menu-sources',
      'mlid' => '4370',
      'plid' => '0',
      'link_path' => 'search/site/',
      'router_path' => 'search/site/%',
      'link_title' => 'Company',
      'options' => 
      array (
        'query' => 
        array (
          'f[0]' => 'im_field_data_sources:17187',
        ),
        'attributes' => 
        array (
          'title' => '',
        ),
      ),
      'module' => 'menu',
      'hidden' => '0',
      'external' => '0',
      'has_children' => '0',
      'expanded' => '0',
      'weight' => '0',
      'depth' => '1',
      'customized' => '1',
      'p1' => '4370',
      'p2' => '0',
      'p3' => '0',
      'p4' => '0',
      'p5' => '0',
      'p6' => '0',
      'p7' => '0',
      'p8' => '0',
      'p9' => '0',
      'updated' => '0',
      'load_functions' => 
      array (
        2 => NULL,
      ),
      'to_arg_functions' => '',
      'access_callback' => 'user_access',
      'access_arguments' => 'a:1:{i:0;s:14:"search content";}',
      'page_callback' => 'apachesolr_search_custom_page',
      'page_arguments' => 'a:3:{i:0;s:11:"core_search";i:1;i:2;i:2;b:0;}',
      'delivery_callback' => '',
      'tab_parent' => 'search/site',
      'tab_root' => 'search/site/%',
      'title' => 'Company',
      'title_callback' => 't',
      'title_arguments' => '',
      'theme_callback' => '',
      'theme_arguments' => 'a:0:{}',
      'type' => '132',
      'description' => '',
      'in_active_trail' => false,
      'href' => 'search/site/',
      'access' => true,
      'localized_options' => 
      array (
        'query' => 
        array (
          'f[0]' => 'im_field_data_sources:17187',
        ),
        'attributes' => 
        array (
          'title' => '',
        ),
      ),
    ),
    2 => 
    array (
      'menu_name' => 'menu-sources',
      'mlid' => '4367',
      'plid' => '0',
      'link_path' => 'search/site/',
      'router_path' => 'search/site/%',
      'link_title' => 'Government',
      'options' => 
      array (
        'query' => 
        array (
          'f[0]' => 'im_field_data_sources:17184',
        ),
        'attributes' => 
        array (
          'title' => '',
        ),
      ),
      'module' => 'menu',
      'hidden' => '0',
      'external' => '0',
      'has_children' => '0',
      'expanded' => '0',
      'weight' => '0',
      'depth' => '1',
      'customized' => '1',
      'p1' => '4367',
      'p2' => '0',
      'p3' => '0',
      'p4' => '0',
      'p5' => '0',
      'p6' => '0',
      'p7' => '0',
      'p8' => '0',
      'p9' => '0',
      'updated' => '0',
      'load_functions' => 
      array (
        2 => NULL,
      ),
      'to_arg_functions' => '',
      'access_callback' => 'user_access',
      'access_arguments' => 'a:1:{i:0;s:14:"search content";}',
      'page_callback' => 'apachesolr_search_custom_page',
      'page_arguments' => 'a:3:{i:0;s:11:"core_search";i:1;i:2;i:2;b:0;}',
      'delivery_callback' => '',
      'tab_parent' => 'search/site',
      'tab_root' => 'search/site/%',
      'title' => 'Government',
      'title_callback' => 't',
      'title_arguments' => '',
      'theme_callback' => '',
      'theme_arguments' => 'a:0:{}',
      'type' => '132',
      'description' => '',
      'in_active_trail' => false,
      'href' => 'search/site/',
      'access' => true,
      'localized_options' => 
      array (
        'query' => 
        array (
          'f[0]' => 'im_field_data_sources:17184',
        ),
        'attributes' => 
        array (
          'title' => '',
        ),
      ),
    ),
    3 => 
    array (
      'menu_name' => 'menu-sources',
      'mlid' => '4366',
      'plid' => '0',
      'link_path' => 'search/site/',
      'router_path' => 'search/site/%',
      'link_title' => 'Industry Body',
      'options' => 
      array (
        'query' => 
        array (
          'f[0]' => 'im_field_data_sources:17185',
        ),
        'attributes' => 
        array (
          'title' => '',
        ),
      ),
      'module' => 'menu',
      'hidden' => '0',
      'external' => '0',
      'has_children' => '0',
      'expanded' => '0',
      'weight' => '0',
      'depth' => '1',
      'customized' => '1',
      'p1' => '4366',
      'p2' => '0',
      'p3' => '0',
      'p4' => '0',
      'p5' => '0',
      'p6' => '0',
      'p7' => '0',
      'p8' => '0',
      'p9' => '0',
      'updated' => '0',
      'load_functions' => 
      array (
        2 => NULL,
      ),
      'to_arg_functions' => '',
      'access_callback' => 'user_access',
      'access_arguments' => 'a:1:{i:0;s:14:"search content";}',
      'page_callback' => 'apachesolr_search_custom_page',
      'page_arguments' => 'a:3:{i:0;s:11:"core_search";i:1;i:2;i:2;b:0;}',
      'delivery_callback' => '',
      'tab_parent' => 'search/site',
      'tab_root' => 'search/site/%',
      'title' => 'Industry Body',
      'title_callback' => 't',
      'title_arguments' => '',
      'theme_callback' => '',
      'theme_arguments' => 'a:0:{}',
      'type' => '132',
      'description' => '',
      'in_active_trail' => false,
      'href' => 'search/site/',
      'access' => true,
      'localized_options' => 
      array (
        'query' => 
        array (
          'f[0]' => 'im_field_data_sources:17185',
        ),
        'attributes' => 
        array (
          'title' => '',
        ),
      ),
    ),
    4 => 
    array (
      'menu_name' => 'menu-sources',
      'mlid' => '4369',
      'plid' => '0',
      'link_path' => 'search/site/',
      'router_path' => 'search/site/%',
      'link_title' => 'Regulator/Commission',
      'options' => 
      array (
        'query' => 
        array (
          'f[0]' => 'im_field_data_sources:17182',
        ),
        'attributes' => 
        array (
          'title' => '',
        ),
      ),
      'module' => 'menu',
      'hidden' => '0',
      'external' => '0',
      'has_children' => '0',
      'expanded' => '0',
      'weight' => '0',
      'depth' => '1',
      'customized' => '1',
      'p1' => '4369',
      'p2' => '0',
      'p3' => '0',
      'p4' => '0',
      'p5' => '0',
      'p6' => '0',
      'p7' => '0',
      'p8' => '0',
      'p9' => '0',
      'updated' => '0',
      'load_functions' => 
      array (
        2 => NULL,
      ),
      'to_arg_functions' => '',
      'access_callback' => 'user_access',
      'access_arguments' => 'a:1:{i:0;s:14:"search content";}',
      'page_callback' => 'apachesolr_search_custom_page',
      'page_arguments' => 'a:3:{i:0;s:11:"core_search";i:1;i:2;i:2;b:0;}',
      'delivery_callback' => '',
      'tab_parent' => 'search/site',
      'tab_root' => 'search/site/%',
      'title' => 'Regulator/Commission',
      'title_callback' => 't',
      'title_arguments' => '',
      'theme_callback' => '',
      'theme_arguments' => 'a:0:{}',
      'type' => '132',
      'description' => '',
      'in_active_trail' => false,
      'href' => 'search/site/',
      'access' => true,
      'localized_options' => 
      array (
        'query' => 
        array (
          'f[0]' => 'im_field_data_sources:17182',
        ),
        'attributes' => 
        array (
          'title' => '',
        ),
      ),
    ),
    5 => 
    array (
      'menu_name' => 'menu-sources',
      'mlid' => '4368',
      'plid' => '0',
      'link_path' => 'search/site/',
      'router_path' => 'search/site/%',
      'link_title' => 'State-run Gambling Entities',
      'options' => 
      array (
        'query' => 
        array (
          'f[0]' => 'im_field_data_sources:17183',
        ),
        'attributes' => 
        array (
          'title' => '',
        ),
      ),
      'module' => 'menu',
      'hidden' => '0',
      'external' => '0',
      'has_children' => '0',
      'expanded' => '0',
      'weight' => '0',
      'depth' => '1',
      'customized' => '1',
      'p1' => '4368',
      'p2' => '0',
      'p3' => '0',
      'p4' => '0',
      'p5' => '0',
      'p6' => '0',
      'p7' => '0',
      'p8' => '0',
      'p9' => '0',
      'updated' => '0',
      'load_functions' => 
      array (
        2 => NULL,
      ),
      'to_arg_functions' => '',
      'access_callback' => 'user_access',
      'access_arguments' => 'a:1:{i:0;s:14:"search content";}',
      'page_callback' => 'apachesolr_search_custom_page',
      'page_arguments' => 'a:3:{i:0;s:11:"core_search";i:1;i:2;i:2;b:0;}',
      'delivery_callback' => '',
      'tab_parent' => 'search/site',
      'tab_root' => 'search/site/%',
      'title' => 'State-run Gambling Entities',
      'title_callback' => 't',
      'title_arguments' => '',
      'theme_callback' => '',
      'theme_arguments' => 'a:0:{}',
      'type' => '132',
      'description' => '',
      'in_active_trail' => false,
      'href' => 'search/site/',
      'access' => true,
      'localized_options' => 
      array (
        'query' => 
        array (
          'f[0]' => 'im_field_data_sources:17183',
        ),
        'attributes' => 
        array (
          'title' => '',
        ),
      ),
    ),
  ),
);