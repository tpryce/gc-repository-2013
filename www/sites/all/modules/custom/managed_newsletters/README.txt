/* $Id$ */

Managed Newsletters

Allows to send highly managable newsletter from the site.

To install, place the entire module folder into your modules directory.
Go to Administer -> Site Building -> Modules and enable the Managed Newsletters module.
