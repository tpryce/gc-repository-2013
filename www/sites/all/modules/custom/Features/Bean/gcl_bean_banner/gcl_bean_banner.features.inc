<?php
/**
 * @file
 * gcl_bean_banner.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function gcl_bean_banner_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "bean_admin_ui" && $api == "bean") {
    return array("version" => "5");
  }
}
