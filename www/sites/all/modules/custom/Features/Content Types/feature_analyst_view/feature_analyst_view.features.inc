<?php
/**
 * @file
 * feature_analyst_view.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function feature_analyst_view_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function feature_analyst_view_node_info() {
  $items = array(
    'analyst_view' => array(
      'name' => t('Analyst View'),
      'base' => 'node_content',
      'description' => t('Data Analyst View'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
