<?php

class GCLNodeMigration extends DrupalNode7Migration {

  protected $file_fields = array();

  public function __construct(array $arguments) {

    parent::__construct($arguments);

    $field_instances = field_info_instances('node', $arguments['destination_type']);

    $this->options['file_function'] = 'MigrateFileFid';

    foreach($this->sourceFields as $field_name => $field_description) {

      // If there's a field that's in the source, but not the local db, skip it.
      if (!isset($field_instances[$field_name])) {
        continue;
      }

      if ($field_name == 'body') {
        // For videos, there's no body value, just the summary, so we take it.
        if ($arguments['destination_type'] == 'video') {
          $this->addFieldMapping($field_name, $field_name . '_summary', FALSE);
          $this->removeFieldMapping($field_name . ':summary');
        }
        // General case: done already by DrupalNode7Migration::__construct().
        continue;
      }

      // This makes the assumption that the type of the field hasn't changed
      // from source db to destination db.
      $field_info = field_info_field($field_name);

      switch ($field_info['type']) {

        case 'image':
        case 'file':

          $this->file_fields[] = $field_name;

          // @todo - Any other field data to come across here?
          // EG alt/title on images, etc?

          $this->addFieldMapping($field_name, $field_name)->sourceMigration('File');
          $this->addFieldMapping($field_name . ':file_class')->defaultValue('MigrateFileFid');

          // dsm("Skipping: node type {$arguments['destination_type']},  field name {$field_name}, field type {$field_info['type']}");
          /*
          File example:
          field_attachment	Attachments
          field_attachment_display	Attachments subfield
          field_attachment_description	Attachments subfield
          */
          break;

        /*
        case 'image':
          // dsm("Skipping: node type {$arguments['destination_type']},  field name {$field_name}, field type {$field_info['type']}");
          /*
          Image example:
          field_chart	Chart
          field_chart_alt	Chart subfield
          field_chart_title	Chart subfield
          field_chart_width	Chart subfield
          field_chart_height	Chart subfield
           * /
          break;
        */

        case 'taxonomy_term_reference':
          $this->addFieldMapping($field_name, $field_name);
          $this->addFieldMapping($field_name . ':source_type')->defaultValue('tid');
          $this->addFieldMapping($field_name . ':create_term')->defaultValue(TRUE);
          $this->addFieldMapping($field_name . ':ignore_case')->defaultValue(TRUE);
          break;

        case 'text':
        case 'text_long':
          $this->addFieldMapping($field_name, $field_name);
          $this->addFieldMapping($field_name . ':format', $field_name . '_format');
          $this->addFieldMapping($field_name . ':language');
          break;

        case 'text_with_summary':
          $this->addFieldMapping($field_name, $field_name);
          $this->addFieldMapping($field_name . ':summary', $field_name . '_summary');
          $this->addFieldMapping($field_name . ':format', $field_name . '_format');
          $this->addFieldMapping($field_name . ':language');
          break;

        case 'user_reference':
          $this->addFieldMapping($field_name, $field_name)->sourceMigration('User')->defaultValue(1);
          break;

        case 'date':
          // @todo: Think this is ok, but need to check it.
          $this->addFieldMapping($field_name, $field_name);
          break;

        case 'computed':
          // @todo: Think this is ok, but need to check it.
          $this->addFieldMapping($field_name, $field_name);
          break;
        // For video content_type we have a youtube field (on the new site).
        case 'youtube':
          $this->addFieldMapping($field_name, $field_name);
          break;

        default:
          //dsm("Using default: node type {$arguments['destination_type']},  field name {$field_name}, field type {$field_info['type']}");
          $this->addFieldMapping($field_name, $field_name);
      }
    }
  }

//  public function prepare($node, $row) {
//    parent::prepareRow($node, $row);
//
//    // drupal_set_message('<pre>' . print_r($node, TRUE) . '</pre>');
//    // drupal_set_message('<pre>' . print_r($row, TRUE) . '</pre>');
//
////    dsm($this->file_fields);
//
//    // This is a bit hacky, but file fields wouldn't seem to work any other way.
//    // Loop over all the file field names we stashed earlier.
//    foreach ($this->file_fields as $field_name) {
//
//      // Check that field's set on this object
//      if (isset($row->$field_name)) {
//
//        $file_id = reset($row->$field_name);
//
//        $file = file_load($file_id);
//
//        if ($file) {
//
//          // Build a field array to save on the object:
//          $field = array(
//            LANGUAGE_NONE => array(
//              0 => (array) $file,
//            ),
//          );
//
//          $node->$field_name = $field;
//        }
//      }
//    }
////    dsm($node);
////    dsm($row);
//  }

}

class MigrateYoutubeFieldHandler extends MigrateFieldHandler {

  public $db_connection;

  public function __construct() {
    $this->registerTypes(array('youtube'));
    $this->db_connection = $connection = Database::getConnection('default', 'legacy');;
  }

  public function prepare($entity, array $field_info, array $instance, array $values) {
    $migration = Migration::currentMigration();
    $arguments = (isset($values['arguments']))? $values['arguments']: array();

    // Legacy field_video_file fid.
    $legacy_fid = $entity->field_video_file[0];

    $query = "SELECT filename FROM {file_managed} WHERE fid = :fid";
    $args = array(
      ':fid' => $legacy_fid,
    );
    $results = $this->db_connection->query($query, $args);

    // A foreach straight after the ->query() function seems a bit bizarre, but
    // it's the only way this is working.
    foreach ($results as $res) {
      $file_id = $res->filename;
    }

    $language = $this->getFieldLanguage($entity, $field_info, $arguments);

    $delta = 0;
    foreach ($values as $value) {
      // This might happen
      if (empty($file_id)) {
        continue;
      }
      // Some file_id might come with other params we don't need. Scrub them.
      $return[$language][$delta]['input'] = 'http://www.youtube.com/watch?v=' . substr($file_id, 0, 15);
      $return[$language][$delta]['video_id'] = substr($file_id, 0, 15);
    }

    if (!isset($return)) {
      $return = NULL;
    }
    return $return;
  }
}