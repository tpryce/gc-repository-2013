<?php
/**
 * @file
 * feature_chart.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function feature_chart_views_default_views() {
  $export = array();

  $view = new view;
  $view->name = 'gd_charts_page';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'GD Charts page';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Charts';
  $handler->display->display_options['access']['type'] = 'role';
  $handler->display->display_options['access']['role'] = array(
    2 => '2',
  );
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '50';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['expose']['items_per_page_options_all'] = 0;
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Content: Chart */
  $handler->display->display_options['fields']['field_chart']['id'] = 'field_chart';
  $handler->display->display_options['fields']['field_chart']['table'] = 'field_data_field_chart';
  $handler->display->display_options['fields']['field_chart']['field'] = 'field_chart';
  $handler->display->display_options['fields']['field_chart']['label'] = '';
  $handler->display->display_options['fields']['field_chart']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field_chart']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_chart']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_chart']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_chart']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_chart']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_chart']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_chart']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_chart']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_chart']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_chart']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_chart']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_chart']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_chart']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['field_chart']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_chart']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_chart']['hide_alter_empty'] = 0;
  $handler->display->display_options['fields']['field_chart']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_chart']['settings'] = array(
    'image_style' => 'chart_preview',
    'image_link' => 'content',
  );
  $handler->display->display_options['fields']['field_chart']['field_api_classes'] = 0;
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 0;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'chart' => 'chart',
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page_1');
  $handler->display->display_options['path'] = 'charts';
  $export['gd_charts_page'] = $view;

  return $export;
}
