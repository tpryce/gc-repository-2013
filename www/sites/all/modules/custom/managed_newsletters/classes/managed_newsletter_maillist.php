<?php
// $Id$

abstract class managed_newsletter_maillist {
  protected $lid;
  protected $title;

  public function get_lid() {
    return $this->lid;
  }

  public function get_title() {
    return $this->title;
  }

  public function set_title($value) {
    $this->title = $value;
  }

  public function load() {

  }

  public function update($values) {
    $this->set_title($values['title']);
  }

  public function save() {
    if ($this->get_lid()) {
      db_update('managed_newsletters_maillists')
	  ->fields(array(
			'title' => $this->get_title(),
		  ))
	  ->condition('lid', $this->get_lid())
	  ->execute();
    }
    else {
      $id = db_insert('managed_newsletters_maillists')
	  ->fields(array(
			'lid' => $this->get_lid(),
			'class' => get_class($this),
			'title' => $this->get_title(),
		  ))
	  ->execute();
      $this->lid = $id;
    }

    menu_rebuild();
  }

  public function delete() {
    db_delete('managed_newsletters_maillists')
	  ->condition('lid', $this->get_lid())
	  ->execute();
    menu_rebuild();
  }

  public static function get_all($lid = null, $class = null) {
    $sql = db_select('managed_newsletters_maillists','m')->fields('m');
    //$conditions = array();

    //$where = array();
    if ($class) {
      /*$where[] =  ' class = \'%s\'';
      $conditions[] = $class;*/
		$sql->condition('class', $class);
    }

    if ($lid) {
      /*$where[] =  ' lid = \'%d\'';
      $conditions[] = $lid;*/
		$sql->condition('lid', $lid);
    }

    /*if (!empty($where)) {
      $sql .= ' WHERE ' . implode(' AND ', $where);
    }*/
    // TODO Please convert this statement to the D7 database API syntax.
    //$result = db_query($sql, $conditions);
    $result = $sql->execute();
    $return = array();
    //while ($row = db_fetch_array($result)) {
    while ($row = $result->fetchAssoc()) {
      if (class_exists($row['class'])) {
        $return[$row['lid']] = new $row['class']();
        unset($row['class']);
        foreach ($row as $key => $value) {
          $return[$row['lid']]->$key = $value;
        }
      }
      else {
        // throw exception
      }
    }
    return $lid ? $return[$lid] : $return;
  }

  public function get_edit_form() {
    $this->load();
    $form = array();

    $form['maillist'] = array(
		'#type' => 'value',
		'#value' => $this
		);

    $form['title'] = array(
		'#type' => 'textfield',
		'#title' => t('Title'),
		'#default_value' => $this->title,
		'#required' => true,
		'#description' => t('Used only for presentation')
		);

    $form['submit'] = array(
		'#type' => 'submit',
		'#value' => t('Save'),
		'#weight' => 10
		);
    return $form;
  }

  abstract function get_recipients();
}
