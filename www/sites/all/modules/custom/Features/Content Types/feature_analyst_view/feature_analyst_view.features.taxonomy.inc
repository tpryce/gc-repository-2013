<?php
/**
 * @file
 * feature_analyst_view.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function feature_analyst_view_taxonomy_default_vocabularies() {
  return array(
    'spotlight_subject' => array(
      'name' => 'Spotlight',
      'machine_name' => 'spotlight_subject',
      'description' => '',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
    ),
  );
}
