<?php

/**
 * PHPUnit tests.
 */

require_once '../gcl_spotlight.module';

class GCLSpotlightTest extends PHPUnit_Framework_TestCase {

  public function testExtractTidsFromTaxonomy() {
    $parameter = '18639,94,160';
    $expected = array(18639,94,160);
    $this->assertEquals($expected, gcl_spotlight_extract_tids_from_taxonomy_parameter($parameter));

    $parameter = '18639 94 160';
    $expected = array(18639,94,160);
    $this->assertEquals($expected, gcl_spotlight_extract_tids_from_taxonomy_parameter($parameter));

    $parameter = '';
    $expected = array();
    $this->assertEquals($expected, gcl_spotlight_extract_tids_from_taxonomy_parameter($parameter));

    $parameter = 'jdfljosjsdkk skjd jsd';
    $expected = array();
    $this->assertEquals($expected, gcl_spotlight_extract_tids_from_taxonomy_parameter($parameter));
  }

  public function testExtractTidsFromSolr() {
    $parameters = array(
      'im_field_spotlight_subject:18639',
      'im_field_geography:95',
      'im_field_sectors:52',
      'im_field_geography:181',
      'im_field_geography:95',
    );
    $expected = array(18639, 95, 52, 181, 95);
    $this->assertEquals($expected, gcl_spotlight_extract_tids_from_solr_parameter($parameters));

    $parameters = array(
      'im_field_spotlight_subject:18639',
      'im_field_geography:95',
      'im_field_sectors:52',
      'im_field_geography:181',
      'im_field_geography:95',
      'ds_created:[2010-01-01T00:00:00Z TO 2011-01-01T00:00:00Z]',
      'ds_created:[2010-01-01T00:00:00Z TO 2010-02-01T00:00:00Z]',
    );
    $expected = array(18639, 95, 52, 181, 95);
    $this->assertEquals($expected, gcl_spotlight_extract_tids_from_solr_parameter($parameters));
  }
}
