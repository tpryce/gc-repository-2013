<?php

/**
 * @file
 * This file is empty by default because the base theme chain (Alpha & Omega) provides
 * all the basic functionality. However, in case you wish to customize the output that Drupal
 * generates through Alpha & Omega this file is a good place to do so.
 *
 * Alpha comes with a neat solution for keeping this file as clean as possible while the code
 * for your subtheme grows. Please read the README.txt in the /preprocess and /process subfolders
 * for more information on this topic.
 */

/**
  * Implementation of hook form alter
  */

function keno_data_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == 'search_block_form') {
    $form['actions']['submit']['#value'] = t('Go');
    $form['search_block_form']['#title'] = '';
    $form['search_block_form']['#size'] = 25;
    $form['search_block_form']['#default_value'] = 'What are you looking for?';
    $form['search_block_form']['#attributes']['onblur'] = "if (this.value == '') {this.value = 'What are you looking for?';}";
    $form['search_block_form']['#attributes']['onfocus'] = "if (this.value == 'What are you looking for?') {this.value = '';}";
  }
  // Customization to user_homepage module, to print the symbolset icons.
  switch ($form_id) {
    case 'user_homepage_button':
      $form['user_homepage_button']['#prefix'] = '<span class="ss-symbol ss-icon">&#x2302;</span>';
      $form['user_homepage_button']['#value'] = t('Save as my homepage');
      break;
    case 'user_homepage_reset_button':
      $form['user_homepage_reset_button']['#prefix'] = '<span class="ss-symbol ss-icon">&#x2421;</span>';
      $form['user_homepage_reset_button']['#value'] = t('Reset my homepage');
      break;
  }
}

/**
 * Implements hook_process_region().
 */
function keno_data_alpha_process_region(&$vars) {
  if (in_array($vars['elements']['#region'], array('content', 'menu', 'branding'))) {
    $theme = alpha_get_theme();

    switch ($vars['elements']['#region']) {

      case 'branding':
        $datalogo_array = array(
          'path' => 'sites/all/themes/keno_data/images/data_logo_active.png',
          'alt' => 'Gambling Data',
          'title' => 'Gambling Data',
          'width' => '199px',
          'height' => '27px',
          'attributes' => array('class' => 'data-img', 'id' => 'data-img'),
        );
        $gcl_domain_data = variable_get('gcl_domain_data', 'http://www.gamblingdata.com');
        $vars['datalogo'] = l(theme('image',$datalogo_array), $gcl_domain_data, array('attributes' => array('title' => 'Gambling Data'), 'html' => TRUE));

        $datalogo_array = array(
          'path' => 'sites/all/themes/keno_data/images/gcl_logo_inactive.png',
          'alt' => 'Gambling Compliance',
          'title' => 'Gambling Compliance',
          'width' => '199px',
          'height' => '27px',
          'attributes' => array('class' => 'data-img', 'id' => 'gcl-img'),
        );
        $gcl_domain_compliance = variable_get('gcl_domain_compliance', 'http://www.gamblingcompliance.com');
        $vars['gcllogo'] = l(theme('image',$datalogo_array), $gcl_domain_compliance, array('attributes' => array('title' => 'Gambling Compliance'), 'html' => TRUE));

        break;
    }
  }
}

function keno_data_views_pre_render(&$view) {
  if(($view->name == 'data') && ($view->current_display == 'page_1')) {
    $view->display_handler->set_option('link_display', 'custom_url');
    $view->display_handler->set_option('link_url', 'data/in-depth');
  }
}

/**
 * Implements hook_page_alter.
 */
/*
function keno_data_page_alter(&$vars) {
  if ($footer_first = $vars['footer']['footer']['footer_first']) {
    $footer_first['locations'] = array(
      '#markup' => '<h2 class="locations-title">' . t('Locations') . '</h2>',
      '#weight' => 1.5,
    );
    $footer_first['#sorted'] = FALSE;
    $vars['footer']['footer']['footer_first'] = $footer_first;
  }

  // Search page tweaks
  if (strstr(current_path(), 'search/site')) {
    $searched_value = $vars['content']['content']['content']['system_main']['search_form']['basic']['keys']['#default_value'];
    var_dump($vars['content']['content']['content']['system_main']['search_form']['basic']['keys']['#default_value']);
    // If no search string, but facet selected, put it in the "Results for "
    // message.
    if ($searched_value == ' ' && isset($_GET['f'][0])) {
      $selected_facet = $_GET['f'][0];
      $split_facet = preg_split('/:/', $selected_facet, 2);
      $facet_value = $split_facet[1];

      // Facet is a bundle (node type).
      if (strstr($selected_facet, 'bundle')) {
        $searched_value = ucwords(str_replace('_', ' ', $facet_value));
      }
      // Facet is a taxonomy_term.
      else {
        $facet_term = taxonomy_term_load($facet_value);
        $searched_value = $facet_term->name;
      }
    }

    // Set up proper search results message.
    $vars['content']['content']['search_page_title'] = array(
      '#markup'=> '<h1 id="page-title" class="grid-12">Results for <b>"' . $searched_value . '"</b></h1>',
      '#weight' => 0,
    );
    // Design's don't feature any search_form or suggestions.
    unset($vars['content']['content']['content']['system_main']['search_form']);
    unset($vars['content']['content']['content']['system_main']['suggestions']);
  }
}
*/

/**
 * Preprocess pages.
 */
function keno_data_preprocess_page(&$vars) {
  // Search page tweaks
  if (strstr(current_path(), 'search/site')) {
    // Hide default title. Using custom one above the main sidebar / content
    // regions.
    $vars['title_hidden'] = TRUE;
  }
}

/**
 * Preprocess search results.
 */
function keno_data_preprocess_search_results(&$vars) {
  $vars['pager'] = theme('pager', array('quantity' => 3));
}

/**
 * Preprocess search result.
 */
function keno_data_preprocess_search_result(&$vars) {
  $marker = theme('mark', array('type' => node_mark($vars['result']['node']->nid, $vars['result']['node']->changed)));
  // using <span>, as <time> tag is not supported yet.
  $datetime = date("l jS F Y", $vars['result']['node']->created);
  $vars['info'] = '<span class="date">' . $datetime . '</span>' . $marker;
}



function keno_data_block_view_alter(&$data, $block) {
  global $_domain;
  $current_domain = $_domain['domain_id'];
  if(($current_domain == 2) && ($block->region == 'menu')) {
    switch ($block->delta) {
    case 'menu-sector-menu':
        $block->title = t('Sector');
        break;
    }
  }
}
