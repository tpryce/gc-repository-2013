<?php
/**
 * @file
 * gcl_content_browser.features.inc
 */

/**
 * Implements hook_views_api().
 */
function gcl_content_browser_views_api() {
  return array("version" => "3.0");
}
