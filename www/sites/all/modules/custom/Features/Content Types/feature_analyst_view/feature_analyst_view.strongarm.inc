<?php
/**
 * @file
 * feature_analyst_view.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function feature_analyst_view_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_analyst_view';
  $strongarm->value = '1';
  $export['comment_analyst_view'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_anonymous_analyst_view';
  $strongarm->value = 0;
  $export['comment_anonymous_analyst_view'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_mode_analyst_view';
  $strongarm->value = 1;
  $export['comment_default_mode_analyst_view'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_per_page_analyst_view';
  $strongarm->value = '50';
  $export['comment_default_per_page_analyst_view'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_form_location_analyst_view';
  $strongarm->value = 1;
  $export['comment_form_location_analyst_view'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_preview_analyst_view';
  $strongarm->value = '1';
  $export['comment_preview_analyst_view'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_subject_field_analyst_view';
  $strongarm->value = 1;
  $export['comment_subject_field_analyst_view'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'field_bundle_settings_node__analyst_view';
  $strongarm->value = array(
    'view_modes' => array(
      'teaser' => array(
        'custom_settings' => TRUE,
      ),
      'search_result' => array(
        'custom_settings' => TRUE,
      ),
      'subscription' => array(
        'custom_settings' => TRUE,
      ),
      'full' => array(
        'custom_settings' => FALSE,
      ),
      'rss' => array(
        'custom_settings' => FALSE,
      ),
      'search_index' => array(
        'custom_settings' => FALSE,
      ),
      'token' => array(
        'custom_settings' => FALSE,
      ),
      'anonymous' => array(
        'custom_settings' => TRUE,
      ),
      'Americas_Premium' => array(
        'custom_settings' => FALSE,
      ),
      'International_Premium' => array(
        'custom_settings' => FALSE,
      ),
      'Americas_Professional' => array(
        'custom_settings' => FALSE,
      ),
      'International_Professional' => array(
        'custom_settings' => FALSE,
      ),
      'Gambling_Data' => array(
        'custom_settings' => FALSE,
      ),
      'print' => array(
        'custom_settings' => FALSE,
      ),
    ),
    'extra_fields' => array(
      'form' => array(),
      'display' => array(
        'subscriptions_ui' => array(
          'default' => array(
            'weight' => '100',
            'visible' => TRUE,
          ),
          'search_result' => array(
            'weight' => '100',
            'visible' => TRUE,
          ),
        ),
        'domain' => array(
          'subscription' => array(
            'weight' => '3',
            'visible' => FALSE,
          ),
          'anonymous' => array(
            'weight' => '2',
            'visible' => FALSE,
          ),
          'default' => array(
            'weight' => '3',
            'visible' => FALSE,
          ),
        ),
        'freetrialform' => array(
          'subscription' => array(
            'weight' => '4',
            'visible' => FALSE,
          ),
          'anonymous' => array(
            'weight' => '1',
            'visible' => TRUE,
          ),
          'default' => array(
            'weight' => '4',
            'visible' => FALSE,
          ),
        ),
      ),
    ),
  );
  $export['field_bundle_settings_node__analyst_view'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_options_analyst_view';
  $strongarm->value = array();
  $export['menu_options_analyst_view'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_parent_analyst_view';
  $strongarm->value = 'menu-category:0';
  $export['menu_parent_analyst_view'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_analyst_view';
  $strongarm->value = array(
    0 => 'moderation',
    1 => 'revision',
  );
  $export['node_options_analyst_view'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_preview_analyst_view';
  $strongarm->value = '1';
  $export['node_preview_analyst_view'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_submitted_analyst_view';
  $strongarm->value = 1;
  $export['node_submitted_analyst_view'] = $strongarm;

  return $export;
}
