<?php
/**
 * @file
 * gcl_views.features.inc
 */

/**
 * Implements hook_views_api().
 */
function gcl_views_views_api() {
  return array("version" => "3.0");
}
