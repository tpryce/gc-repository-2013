<?php
/**
 * @file
 * gcl_rules.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function gcl_rules_default_rules_configuration() {
  $items = array();
  $items['rules_notify_admin_after_a_user_forwards_a_node'] = entity_import('rules_config', '{ "rules_notify_admin_after_a_user_forwards_a_node" : {
      "LABEL" : "Notify admin after a user forwards a node",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "Forward" ],
      "REQUIRES" : [ "rules", "forward" ],
      "ON" : [ "node_forward" ],
      "DO" : [
        { "mail" : {
            "to" : "forwardto@gamblingcompliance.com\\r\\nmark.davies@codeenigma.com",
            "subject" : "[Forward by: [user:name]] - [forwarded-node:title]",
            "message" : "The user [user:name] who has the following subscriptions, [user:role-names] has forwarded the following article:\\r\\n\\r\\n\\u003Ch2\\u003E[forwarded-node:title]\\u003C\\/h2\\u003E\\r\\n[forwarded-node:url]\\r\\n\\r\\n[forwarded-node:body]\\r\\n\\r\\n\\r\\n\\r\\n[site:name]\\r\\n[site:slogan]\\r\\n[site:mail]",
            "from" : "[site:mail]",
            "language" : [ "" ]
          }
        }
      ]
    }
  }');
  $items['rules_notify_all_users_who_are_watching_this_node'] = entity_import('rules_config', '{ "rules_notify_all_users_who_are_watching_this_node" : {
      "LABEL" : "Notify all users who are watching this node",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "watchlist" ],
      "REQUIRES" : [ "rules", "workbench_moderation", "flag" ],
      "ON" : [ "node_update" ],
      "IF" : [
        { "node_is_of_type" : {
            "node" : [ "node" ],
            "type" : { "value" : {
                "analyst_view" : "analyst_view",
                "chart" : "chart",
                "data" : "data",
                "data_report" : "data_report",
                "event" : "event",
                "market_analysis" : "market_analysis",
                "product" : "product",
                "press_release" : "press_release",
                "regulatory_news" : "regulatory_news",
                "regulatory_reports" : "regulatory_reports",
                "white_paper" : "white_paper",
                "video" : "video"
              }
            }
          }
        },
        { "NOT data_is" : { "data" : [ "node-unchanged:created" ], "value" : [ "node:created" ] } },
        { "content_is_live_revision" : { "node" : [ "node" ] } }
      ],
      "DO" : [
        { "flag_fetch_users_node" : {
            "USING" : { "flag" : "add_to_watchlist", "node" : [ "node" ] },
            "PROVIDE" : { "users" : { "users_who_flagged" : "Users who flagged" } }
          }
        },
        { "LOOP" : {
            "USING" : { "list" : [ "users-who-flagged" ] },
            "ITEM" : { "user_to_email" : "User to email" },
            "DO" : [
              { "mail" : {
                  "to" : "[user-to-email:mail]",
                  "subject" : "[node:title] updated!",
                  "message" : "Hi [user-to-email:name],\\r\\n\\r\\nYou are recieving this email as you have added [node:url] to your Watchlist!\\r\\n\\r\\n[node:summary]\\r\\n\\r\\nYou last viewed on the article on [node:last-view]\\r\\n\\r\\nIf you would like to add\\/remove the article from your watchlist please use the following link: [node:flag-add-to-watchlist-link]\\r\\n\\r\\nMany Thanks,\\r\\n[site:name]\\r\\n[site:slogan]\\r\\n[site:url]",
                  "from" : "[site:mail]",
                  "language" : [ "" ]
                }
              }
            ]
          }
        }
      ]
    }
  }');
  return $items;
}
