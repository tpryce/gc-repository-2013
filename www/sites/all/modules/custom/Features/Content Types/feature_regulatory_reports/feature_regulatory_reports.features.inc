<?php
/**
 * @file
 * feature_regulatory_reports.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function feature_regulatory_reports_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function feature_regulatory_reports_node_info() {
  $items = array(
    'regulatory_reports' => array(
      'name' => t('Regulatory Reports'),
      'base' => 'node_content',
      'description' => t('Regulatory Reports'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
