<?php
$menu = array (
  'menu_name' => 'menu-category',
  'title' => 'Category',
  'description' => 'Mega-menu category section.',
  'items' =>
  array (
    0 =>
    array (
      'menu_name' => 'menu-category',
      'mlid' => '1547',
      'plid' => '0',
      'link_path' => 'search/site/',
      'router_path' => 'search/site/%',
      'link_title' => 'Regulatory News',
      'options' =>
      array (
        'attributes' =>
        array (
          'title' => '',
        ),
        'query' =>
        array (
          'f[0]' => 'bundle:regulatory_news',
        ),
      ),
      'module' => 'menu',
      'hidden' => '0',
      'external' => '0',
      'has_children' => '0',
      'expanded' => '0',
      'weight' => '-40',
      'depth' => '1',
      'customized' => '1',
      'p1' => '1547',
      'p2' => '0',
      'p3' => '0',
      'p4' => '0',
      'p5' => '0',
      'p6' => '0',
      'p7' => '0',
      'p8' => '0',
      'p9' => '0',
      'updated' => '0',
      'load_functions' =>
      array (
        2 => NULL,
      ),
      'to_arg_functions' => '',
      'access_callback' => 'user_access',
      'access_arguments' => 'a:1:{i:0;s:14:"search content";}',
      'page_callback' => 'apachesolr_search_custom_page',
      'page_arguments' => 'a:3:{i:0;s:11:"core_search";i:1;i:2;i:2;b:0;}',
      'delivery_callback' => '',
      'tab_parent' => 'search/site',
      'tab_root' => 'search/site/%',
      'title' => 'Regulatory News',
      'title_callback' => 't',
      'title_arguments' => '',
      'theme_callback' => '',
      'theme_arguments' => 'a:0:{}',
      'type' => '132',
      'description' => '',
      'in_active_trail' => false,
      'href' => 'search/site/',
      'access' => true,
      'localized_options' =>
      array (
        'attributes' =>
        array (
          'title' => '',
        ),
        'query' =>
        array (
          'f[0]' => 'bundle:regulatory_news',
        ),
      ),
    ),
    1 =>
    array (
      'menu_name' => 'menu-category',
      'mlid' => '3983',
      'plid' => '0',
      'link_path' => 'search/site/',
      'router_path' => 'search/site/%',
      'link_title' => 'Market Analysis',
      'options' =>
      array (
        'query' =>
        array (
          'f[0]' => 'bundle:market_analysis',
        ),
        'attributes' =>
        array (
          'title' => '',
        ),
      ),
      'module' => 'menu',
      'hidden' => '0',
      'external' => '0',
      'has_children' => '0',
      'expanded' => '0',
      'weight' => '0',
      'depth' => '1',
      'customized' => '1',
      'p1' => '3983',
      'p2' => '0',
      'p3' => '0',
      'p4' => '0',
      'p5' => '0',
      'p6' => '0',
      'p7' => '0',
      'p8' => '0',
      'p9' => '0',
      'updated' => '0',
      'load_functions' =>
      array (
        2 => NULL,
      ),
      'to_arg_functions' => '',
      'access_callback' => 'user_access',
      'access_arguments' => 'a:1:{i:0;s:14:"search content";}',
      'page_callback' => 'apachesolr_search_custom_page',
      'page_arguments' => 'a:3:{i:0;s:11:"core_search";i:1;i:2;i:2;b:0;}',
      'delivery_callback' => '',
      'tab_parent' => 'search/site',
      'tab_root' => 'search/site/%',
      'title' => 'Market Analysis',
      'title_callback' => 't',
      'title_arguments' => '',
      'theme_callback' => '',
      'theme_arguments' => 'a:0:{}',
      'type' => '132',
      'description' => '',
      'in_active_trail' => false,
      'href' => 'search/site/',
      'access' => true,
      'localized_options' =>
      array (
        'query' =>
        array (
          'f[0]' => 'bundle:market_analysis',
        ),
        'attributes' =>
        array (
          'title' => '',
        ),
      ),
    ),
    2 =>
    array (
      'menu_name' => 'menu-category',
      'mlid' => '1804',
      'plid' => '0',
      'link_path' => 'search/site/',
      'router_path' => 'search/site/%',
      'link_title' => 'Regulatory Reports',
      'options' =>
      array (
        'query' =>
        array (
          'f[0]' => 'bundle:regulatory_reports',
        ),
        'attributes' =>
        array (
          'title' => '',
        ),
      ),
      'module' => 'menu',
      'hidden' => '0',
      'external' => '0',
      'has_children' => '0',
      'expanded' => '0',
      'weight' => '0',
      'depth' => '1',
      'customized' => '1',
      'p1' => '1804',
      'p2' => '0',
      'p3' => '0',
      'p4' => '0',
      'p5' => '0',
      'p6' => '0',
      'p7' => '0',
      'p8' => '0',
      'p9' => '0',
      'updated' => '0',
      'load_functions' =>
      array (
        2 => NULL,
      ),
      'to_arg_functions' => '',
      'access_callback' => 'user_access',
      'access_arguments' => 'a:1:{i:0;s:14:"search content";}',
      'page_callback' => 'apachesolr_search_custom_page',
      'page_arguments' => 'a:3:{i:0;s:11:"core_search";i:1;i:2;i:2;b:0;}',
      'delivery_callback' => '',
      'tab_parent' => 'search/site',
      'tab_root' => 'search/site/%',
      'title' => 'Regulatory Reports',
      'title_callback' => 't',
      'title_arguments' => '',
      'theme_callback' => '',
      'theme_arguments' => 'a:0:{}',
      'type' => '132',
      'description' => '',
      'in_active_trail' => false,
      'href' => 'search/site/',
      'access' => true,
      'localized_options' =>
      array (
        'query' =>
        array (
          'f[0]' => 'bundle:regulatory_reports',
        ),
        'attributes' =>
        array (
          'title' => '',
        ),
      ),
    ),
    3 =>
    array (
      'menu_name' => 'menu-category',
      'mlid' => '1805',
      'plid' => '0',
      'link_path' => 'search/site/',
      'router_path' => 'search/site/%',
      'link_title' => 'Special Reports',
      'options' =>
      array (
        'query' =>
        array (
          'f[0]' => 'bundle:white_paper',
        ),
        'attributes' =>
        array (
          'title' => '',
        ),
      ),
      'module' => 'menu',
      'hidden' => '0',
      'external' => '0',
      'has_children' => '0',
      'expanded' => '0',
      'weight' => '0',
      'depth' => '1',
      'customized' => '1',
      'p1' => '1805',
      'p2' => '0',
      'p3' => '0',
      'p4' => '0',
      'p5' => '0',
      'p6' => '0',
      'p7' => '0',
      'p8' => '0',
      'p9' => '0',
      'updated' => '0',
      'load_functions' =>
      array (
        2 => NULL,
      ),
      'to_arg_functions' => '',
      'access_callback' => 'user_access',
      'access_arguments' => 'a:1:{i:0;s:14:"search content";}',
      'page_callback' => 'apachesolr_search_custom_page',
      'page_arguments' => 'a:3:{i:0;s:11:"core_search";i:1;i:2;i:2;b:0;}',
      'delivery_callback' => '',
      'tab_parent' => 'search/site',
      'tab_root' => 'search/site/%',
      'title' => 'Special Reports',
      'title_callback' => 't',
      'title_arguments' => '',
      'theme_callback' => '',
      'theme_arguments' => 'a:0:{}',
      'type' => '132',
      'description' => '',
      'in_active_trail' => false,
      'href' => 'search/site/',
      'access' => true,
      'localized_options' =>
      array (
        'query' =>
        array (
          'f[0]' => 'bundle:white_paper',
        ),
        'attributes' =>
        array (
          'title' => '',
        ),
      ),
    ),
  ),
);