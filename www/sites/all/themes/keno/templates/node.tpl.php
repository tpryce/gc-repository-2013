<article<?php print $attributes; ?>>
  <?php if ($teaser && $content['field_chart']) : print render($content['field_chart']); endif; ?>
  <?php print $user_picture; ?>
  <?php print render($title_prefix); ?>
  <?php if (!$page && $title): ?>
  <header>
    <h2<?php print $title_attributes; ?>><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h2>
  </header>
  <?php endif; ?>
  <?php print render($title_suffix); ?>
  <?php if ($display_submitted): ?>
  <footer class="submitted"><?php print $submitted; ?><?php print theme('mark', array('type' => node_mark($node->nid, $node->changed))); ?></footer>
  <?php endif; ?>

  <div<?php print $content_attributes; ?>>
    <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
      hide($content['links_left']);
      hide($content['links_right']);
// $embedded_container should/chould contain the category links placed at the top
// of the report nodes, as well as the main image (if it exists).
      if (isset($embedded_container)) {
        print $embedded_container;
      }
      print render($content);
    ?>
    <div>
      <?php if (!empty($content['links'])): ?>
        <nav class="links node-links clearfix"><?php print render($content['links']); ?></nav>
      <?php endif; ?>

      <div class="clearfix">
        <?php if (!empty($content['links_left'])): ?>
          <nav class="links node-links left"><?php print render($content['links_left']); ?></nav>
        <?php endif; ?>

        <?php if (!empty($content['links_right'])): ?>
          <nav class="links node-links right"><?php print render($content['links_right']); ?></nav>
        <?php endif; ?>
      </div>

      <?php print render($content['comments']); ?>
    </div>
  </div>
</article>
