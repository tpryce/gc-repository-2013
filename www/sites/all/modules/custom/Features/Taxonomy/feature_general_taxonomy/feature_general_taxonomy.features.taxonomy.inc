<?php
/**
 * @file
 * feature_general_taxonomy.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function feature_general_taxonomy_taxonomy_default_vocabularies() {
  return array(
    'content' => array(
      'name' => 'Content',
      'machine_name' => 'content',
      'description' => 'Describes whether the content is news, analysis, rulebooks, etc',
      'hierarchy' => '1',
      'module' => 'taxonomy',
      'weight' => '8',
    ),
    'geography' => array(
      'name' => 'Geography',
      'machine_name' => 'geography',
      'description' => 'Geographical Taxonomy',
      'hierarchy' => '1',
      'module' => 'taxonomy',
      'weight' => '-9',
    ),
    'sectors' => array(
      'name' => 'Sectors',
      'machine_name' => 'sectors',
      'description' => 'Gambling Industry Sectors',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '-7',
    ),
    'subscription_role' => array(
      'name' => 'Subscription Role',
      'machine_name' => 'subscription_role',
      'description' => '',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '10',
    ),
    'tags' => array(
      'name' => 'Tags',
      'machine_name' => 'tags',
      'description' => 'To be used for the "Similar by Term" block',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '10',
    ),
    'topics' => array(
      'name' => 'Topics',
      'machine_name' => 'topics',
      'description' => 'Topics of interest. Topics are usually linked to a campaign',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '10',
    ),
  );
}
