<?php
// $Id$


/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function managed_newsletters_send_form($form, $form_state, $mode='') {
  $form = array();
  $back = false;
  if (!isset($form_state['managed_newsletters'])) {
    $form_state['managed_newsletters'] = array();
    if ($mode == 'full') {
      $step = 1;
    }
    else {
      $n = managed_newsletter_template::get_all('managed_newsletter_newsletter', $mode);
      if ($n) {
        $form_state['values']['newsletter'] = $mode;
        $form_state['managed_newsletters']['newsletter'] = $mode;
        $step = 2;
      }
      else {
        $step = 1;
      }
    }
    $form_state['managed_newsletters']['step'] = $step;
  }
  else {
    $step = $form_state['managed_newsletters']['step'];

    if ($mode == 'full' && $step <= 1) {
      $back = false;
    }
    elseif ($mode != 'full' && $step <= 2) {
      $back = false;
    }
    else {
      $back = true;
    }
  }

  switch ($step) {
    case 1:
      managed_newsletters_send_form_step_newsletter($form, $form_state);
      break;
    case 2:
      managed_newsletters_send_form_step_queue($form, $form_state);
      break;
    case 3:
      managed_newsletters_send_form_step_order($form, $form_state);
      break;
    case 4:
      managed_newsletters_send_form_step_edit($form, $form_state);
      break;
    case 5:
      managed_newsletters_send_form_step_test($form, $form_state);
      break;
    case 6:
      managed_newsletters_send_form_step_add_to_queue($form, $form_state);
      break;
	case 7:
      managed_newsletters_send_form_step_send($form, $form_state);
      break;
    case 8:
      managed_newsletters_send_form_step_reset_queue($form, $form_state);
      $back = false;
      break;
  }
  if ($back) {
    $form['back'] = array(
			'#type' => 'submit',
			'#submit' => array('managed_newsletters_send_form_submit_back'),
			'#value' => t('Back'),
			'#weight' => 9
		);
  }
  $form['cancel'] = array(
		'#type' => 'submit',
		'#submit' => array('managed_newsletters_send_form_submit_cancel'),
		'#value' => t('Cancel'),
		'#weight' => 15,
		'#attributes' => array(
			'style' => 'margin-left:5em;',
		),
	);
  return $form;
}

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function managed_newsletters_send_form_submit_cancel($form, &$form_state) {
  unset($form_state['managed_newsletters']);
  $form_state['rebuild'] = false;
  $form_state['redirect'] = $_GET['q'];
}

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function managed_newsletters_send_form_submit_back($form, &$form_state) {
  $form_state['rebuild'] = true;
  $form_state['managed_newsletters'] = $form_state['values'] + $form_state['managed_newsletters'];
  $form_state['managed_newsletters']['step']--;
}

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function managed_newsletters_send_form_submit($form, &$form_state) {
  $form_state['rebuild'] = true;
  $form_state['managed_newsletters'] = $form_state['values'] + $form_state['managed_newsletters'];

  switch ($form_state['managed_newsletters']['step']) {
    case 5:
      managed_newsletters_send_test($form_state['managed_newsletters']);
      break;
	case 6:
      managed_newsletters_add_to_queue($form_state['managed_newsletters']);
      break;
    case 7:
      managed_newsletters_batch_send_action($form_state['managed_newsletters']);
      break;
    case 8:
      managed_newsletters_reset_queue($form_state['managed_newsletters']);
      managed_newsletters_send_form_submit_cancel($form, $form_state);
      break;
  }
  if (isset($form_state['managed_newsletters'])) {
	  $form_state['managed_newsletters']['step']++;
  }
}

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function managed_newsletters_send_form_step_newsletter(&$form, &$form_state) {
  $newsletters = array();
  foreach (managed_newsletter_template::get_all(null, null, null, 0) as $item) {
    $newsletters[$item->get_tid()] = $item->get_title();
  }

  $maillist = array();
  foreach (managed_newsletter_maillist::get_all() as $item) {
    $maillist[$item->get_lid()] = $item->get_title() . ' ' . l('Preview', 'admin/managed_newsletters/maillists/preview/' . $item->get_lid(), array(
			'attributes' => array(
			'target' => '_blank'
		)
		));
  }

  $def_value_newslleter = (isset($form_state['managed_newsletters']['newsletter'])) ? $form_state['managed_newsletters']['newsletter'] : array();
  $def_value_maillist = (isset($form_state['managed_newsletters']['maillists'])) ? (array)$form_state['managed_newsletters']['maillists'] : array();
  $form['newsletter'] = array(
		'#type' => 'radios', '#title' => t('Newsletter'), '#options' => $newsletters, '#default_value' => $def_value_newslleter, '#required' => true, '#description' => t('Select newsletter you want to send')
	);
  $form['maillists'] = array(
		'#type' => 'checkboxes', '#title' => t('Mail lists'), '#options' => $maillist, '#default_value' => $def_value_maillist, '#required' => true, '#description' => t('Select mail lists you want to send the newsletter to')
	);
  $form['next'] = array(
		'#type' => 'submit', '#value' => t('Next'), '#weight' => 10
	);
}

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function managed_newsletters_send_form_step_queue(&$form, &$form_state) {
  if ($newsletter = managed_newsletter_template::get_all(null, $form_state['managed_newsletters']['newsletter'])) {
    $newsletter->load();
    $form['queue'] = array(
			'#tree' => true
		);
    foreach ($newsletter->get_blocks_templates() as $tid => $block) {
      if (is_a($block, 'managed_newsletter_block_node')) {
        $form['queue'][$tid] = array(
					'#type' => 'fieldset', '#title' => $block->get_title(), '#tree' => true
				);
        foreach ($block->get_nodes() as $group) {
          foreach ($group as $node) {
            $form['queue'][$tid][$node->nid] = array(
							'#type' => 'radios',
							'#options' => array(
								'use' => t('Use'), 'hold' => t('Hold'), 'remove' => t('Discard')
								),
							'#title' => $node->title,
							'#default_value' => isset($form_state['managed_newsletters']['queue'][$tid][$node->nid]) ? $form_state['managed_newsletters']['queue'][$tid][$node->nid] : 'remove'
						);
          }
        }
      }
    }
    $form['next'] = array(
			'#type' => 'submit', '#value' => t('Next'), '#weight' => 10
		);
    $form['#theme']  = 'managed_newsletters_send_form_queue';
  }
  else {
    drupal_set_message(t('Selected newsletter has not been found'), 'error');
  }
}

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function managed_newsletters_send_form_step_order(&$form, &$form_state) {
  if ($newsletter = managed_newsletter_template::get_all(null, $form_state['managed_newsletters']['newsletter'])) {
    $newsletter->load();
    /*@var $newsletter managed_newsletter_newsletter*/

    $form['order'] = array(
			'#tree' => true
		);
    $image_path = base_path() . drupal_get_path('module', 'managed_newsletters') . '/images';
    foreach ($newsletter->get_blocks_templates() as $tid => $block) {
      if (is_a($block, 'managed_newsletter_block_node')) {
        $form['order'][$tid] = array(
					'#type' => 'fieldset',
					'#title' => $block->get_title(),
					'#tree' => true,
				);

        $block_nodes = $block->get_nodes();
        $groups = array();
        foreach ($block_nodes as $group => $nodes) {
          $groups[drupal_clean_css_identifier($group)] = $group;
        }

        $gr_ord = 1;
        foreach ($groups as $group_id => $group) {
          $nodes = $block_nodes[$group];

          $form['order'][$tid][$group_id]['#prefix'] = '<div class="managed-newsletters-group"><div class="managed-newsletters-group-buttons"><img class="managed-newsletters-group-button-up" src="' . $image_path . '/up.png"><img class="managed-newsletters-group-button-down" src="' . $image_path . '/down.png"></div>';
          $form['order'][$tid][$group_id]['#type'] = 'fieldset';
          $form['order'][$tid][$group_id]['#title'] = $group;
          $form['order'][$tid][$group_id]['#attributes'] = array(
						'id' => $group_id,
					);
          $form['order'][$tid][$group_id]['#tree'] = true;
          $form['order'][$tid][$group_id]['#suffix'] = '</div>';
          $form['order'][$tid][$group_id]['#weight'] = isset($form_state['managed_newsletters']['order'][$tid][$group_id]) ? $form_state['managed_newsletters']['order'][$tid][$group_id]['order'] : $gr_ord;
          $form['order'][$tid][$group_id]['order'] = array(
						'#type' => 'hidden',
						'#default_value' => isset($form_state['managed_newsletters']['order'][$tid][$group_id]) ? $form_state['managed_newsletters']['order'][$tid][$group_id]['order'] : $gr_ord,
						'#attributes' => array(
							'class' => 'managed-newsletters-group-order'
						),
					);
          $gr_ord++;

          $ord = 1;
          foreach ($nodes as $node) {
            if ($form_state['managed_newsletters']['queue'][$tid][$node->nid] == 'use') {
              if (isset($form_state['managed_newsletters']['order'][$tid][$group_id][$node->nid]['group'])) {
                $group_val = $form_state['managed_newsletters']['order'][$tid][$group_id][$node->nid]['group'];
              }
              else {
                $group_val = $group_id;
              }
              $form['order'][$tid][$group_id][$node->nid]['group'] = array(
								'#type' => 'select',
								'#options' => $groups,
								'#title' => $node->title,
								'#default_value' => $group_val,
								//#weight' => $ord,
								'#prefix' => '<div class="managed-newsletters-node-wrapper"><div class="managed-newsletters-node-buttons"><img class="managed-newsletters-node-button-up" src="' . $image_path . '/up.png"><img class="managed-newsletters-node-button-down" src="' . $image_path . '/down.png"></div>'
							);
              $form['order'][$tid][$group_id][$node->nid]['order'] = array(
								'#type' => 'hidden',
								'#default_value' => isset($form_state['managed_newsletters']['order'][$tid][$group_id][$node->nid]['order']) ? $form_state['managed_newsletters']['order'][$tid][$group_id][$node->nid]['order'] : $ord,
								'#attributes' => array(
									'class' => 'managed-newsletters-order'
								),
								//'#weight' => $ord,
								'#suffix' => '</div>'
							);
              $ord++;
            }
          }
        }
      }
    }
    $form['next'] = array(
			'#type' => 'submit',
			'#value' => t('Next'),
			'#weight' => 10,
		);
    drupal_add_js(drupal_get_path('module', 'managed_newsletters') . '/managed_newsletters_order_nodes_1.js');
    drupal_add_css(drupal_get_path('module', 'managed_newsletters') . '/managed_newsletters_order_nodes.css');
  }
  else {
    drupal_set_message(t('Selected newsletter has not been found'), 'error');
  }
}

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function managed_newsletters_send_form_step_edit(&$form, &$form_state) {
  if ($newsletter = managed_newsletter_template::get_all(null, $form_state['managed_newsletters']['newsletter'])) {
    $newsletter->load();
    /*@var $newsletter managed_newsletter_newsletter*/
//dpr($form_state['managed_newsletters']);
    $form['edit'] = array(
			'#tree' => true
		);

    foreach ($newsletter->get_blocks_templates() as $tid => $block) {
      if (is_a($block, 'managed_newsletter_block_node')) {
        $form['edit'][$tid] = array(
					'#type' => 'fieldset',
					'#title' => $block->get_title(),
					'#tree' => true,
				);
        foreach ($block->get_nodes() as $group => $nodes) {
          $group_id = drupal_clean_css_identifier($group);

          $form['edit'][$tid][$group_id]['#type'] = 'fieldset';
          $form['edit'][$tid][$group_id]['#title'] = $group;
          $form['edit'][$tid][$group_id]['#tree'] = true;
          $form['edit'][$tid][$group_id]['#weight'] = $form_state['managed_newsletters']['order'][$tid][$group_id]['order'];

          $form['edit'][$tid][$group_id]['order'] = array(
						'#type' => 'hidden',
						'#default_value' => $form_state['managed_newsletters']['order'][$tid][$group_id]['order'],
					);
          $form['edit'][$tid][$group_id]['name'] = array(
						'#type' => 'hidden',
						'#default_value' => $group,
					);

          $form['edit'][$tid][$group_id]['nodes']['#tree'] = true;
          foreach ($nodes as $node) {
            if ($form_state['managed_newsletters']['queue'][$tid][$node->nid] == 'use') {
              $new_group = $form_state['managed_newsletters']['order'][$tid][$group_id][$node->nid]['group'];
              $weight = $form_state['managed_newsletters']['order'][$tid][$group_id][$node->nid]['order'];
              $form['edit'][$tid][$new_group]['nodes'][$node->nid]['#tree'] = true;
              $form['edit'][$tid][$new_group]['nodes'][$node->nid]['#weight'] = ($weight);
              $form['edit'][$tid][$new_group]['nodes'][$node->nid]['title'] = array(
								'#type' => 'textfield',
								//'#title' => t('Title') . ($weight * 10),
								'#default_value' => isset($form_state['managed_newsletters']['edit'][$tid][$new_group]['nodes'][$node->nid]['title']) ? $form_state['managed_newsletters']['edit'][$tid][$new_group]['nodes'][$node->nid]['title'] : $node->title,
								'#attributes' => array('style' => 'width:100%;'),
								'#weight' => ($weight * 10)
							);
				$lang = isset($node->language) ? $node->language : LANGUAGE_NONE;
				$node_teaser = $node->body[$lang][0]['summary'];
              $form['edit'][$tid][$new_group]['nodes'][$node->nid]['teaser'] = array(
								'#type' => 'textarea',
								//'#title' => t('Teaser') . (($weight * 10) + 1),
								'#default_value' => isset($form_state['managed_newsletters']['edit'][$tid][$new_group]['nodes'][$node->nid]['teaser']) ? $form_state['managed_newsletters']['edit'][$tid][$new_group]['nodes'][$node->nid]['teaser'] : $node_teaser,
								'#attributes' => array('style' => 'width:100%;'),
								'#weight' => ($weight * 10) + 1
							);
              $form['edit'][$tid][$new_group]['nodes'][$node->nid]['order'] = array(
								'#type' => 'hidden',
								'#default_value' => $weight,
							);
            }
          }
        }

        foreach ($form['edit'][$tid] as $block => $set) {
          if (is_array($set) && $set['#type'] == 'fieldset') {
            if (count($set['nodes']) == 1) {
              unset($form['edit'][$tid][$block]);
            }
            else {
              uasort($form['edit'][$tid][$block]['nodes'], 'element_sort');
            }
          }
        }
      }
    }
    $form['next'] = array(
			'#type' => 'submit',
			'#value' => t('Next'),
			'#weight' => 10
		);
    drupal_add_css(drupal_get_path('module', 'managed_newsletters') . '/managed_newsletters_order_nodes.css');
  }
  else {
    drupal_set_message(t('Selected newsletter has not been found'), 'error');
  }
}

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function managed_newsletters_send_form_step_test(&$form, &$form_state) {
  global $user;
  $user = user_load($user->uid);
  if ($newsletter = managed_newsletter_template::get_all('managed_newsletter_newsletter', $form_state['managed_newsletters']['newsletter'])) {
    $newsletter->load();
    foreach ($newsletter->get_blocks_templates() as $tid => $block) {
      if (is_a($block, 'managed_newsletter_block_node')) {
        $newsletter->get_blocks_templates($tid)->load_changed_nodes($form_state['managed_newsletters']['edit'][$tid]);
      }
    }
    $form['html'] = array(
			'#type' => 'fieldset',
			'#title' => 'Html',
		);
    $form['html']['html'] = array(
			'#type' => 'markup',
			'#markup' => $newsletter->build_html_content($user),
		);

    $form['text'] = array(
			'#type' => 'fieldset',
			'#title' => 'Text',
		);
    $form['text']['text'] = array(
			'#type' => 'markup',
			'#markup' => nl2br($newsletter->build_text_content($user)),
		);

  }
  $form['desc'] = array(
		'#markup' => '<p style="font-weight:bold;">Test newsletter will be sent on your email address</p>',
	);
  $form['send_test'] = array(
		'#type' => 'submit',
		'#value' => t('Send test email'),
		'#weight' => 10,
	);
}

function managed_newsletters_send_form_step_add_to_queue(&$form, &$form_state){
	
	$form['add_queue'] = array(
			'#type' => 'submit',
			'#value' => t('Add to queue'),
			'#weight' => 10,
		);
}

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function managed_newsletters_send_form_step_send(&$form, &$form_state) {
  if ($newsletter = managed_newsletter_template::get_all('managed_newsletter_newsletter', $form_state['managed_newsletters']['newsletter'])) {
    $newsletter->load();
    foreach ($newsletter->get_blocks_templates() as $tid => $block) {
      if (is_a($block, 'managed_newsletter_block_node')) {
        $newsletter->get_blocks_templates($tid)->load_changed_nodes($form_state['managed_newsletters']['edit'][$tid]);
      }
    }
    if (!isset($form_state['managed_newsletters']['maillists'])) {
      $form_state['managed_newsletters']['maillists'] = $newsletter->get_default_maillists();
    }
    $maillists = array();
    foreach (array_filter($form_state['managed_newsletters']['maillists']) as $lid) {
      $maillist = managed_newsletter_maillist::get_all($lid);
      $maillists[] = $maillist->get_title()
				. ' '
				. ' ( ' . t('@num users', array('@num' => count($maillist->get_recipients()))) . ') '
				. l('Preview',
					'admin/managed_newsletters/maillists/preview/' . $maillist->get_lid(),
					array(
						'attributes' => array(
							'target' => '_blank'
						)
					))
				;
    }

    $form['desc'] = array(
			'#markup' => '<p style="font-weight:bold;">'
				. t('@newsletter will be sent to all users from maillists:',
					array(
						'@newsletter' => $newsletter->get_title()
					))
				. '</p>'
				. theme('item_list', array('items' => $maillists))
		);

    $form['send'] = array(
			'#type' => 'submit',
			'#value' => t('Send Newsletter'),
			'#weight' => 10,
		);
  }
}
/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function managed_newsletters_send_form_step_reset_queue(&$form, &$form_state) {
  $form['reset'] = array(
		'#type' => 'submit',
		'#value' => t('Reset Nodes Queue'),
		'#weight' => 10
	);
}

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function managed_newsletters_send_test($storage) {
  global $user;
  $user = user_load($user->uid);
  if ($newsletter = managed_newsletter_template::get_all('managed_newsletter_newsletter', $storage['newsletter'])) {
    $newsletter->load();
    foreach ($newsletter->get_blocks_templates() as $tid => $block) {
      if (is_a($block, 'managed_newsletter_block_node')) {
        $newsletter->get_blocks_templates($tid)->load_changed_nodes($storage['edit'][$tid]);
      }
    }
    $sender = managed_newsletter_sender::load_default_sender();
    $sender->send_test($newsletter, $user);
    drupal_set_message(t('Test message has been sent to @mail.', array(
			'@mail' => $user->mail
		)));
  }
  else {
    drupal_set_message(t('Test message has not been sent. Newsletter template doesn\'t exist.'), 'error');
  }
}

function managed_newsletters_add_to_queue(&$storage){
	if ($newsletter = managed_newsletter_template::get_all('managed_newsletter_newsletter', $storage['newsletter'])){
		$newsletter->load();

		foreach ($newsletter->get_blocks_templates() as $tid => $block) {
		  if (is_a($block, 'managed_newsletter_block_node')) {
			$nodes_params[] = $storage['edit'][$tid];
		  }
		}

		$qid = db_insert('managed_newsletters_send_queue')
		  ->fields(array(
				'tid' => $newsletter->get_tid(),
				'created' => REQUEST_TIME,
				'nodes_params' =>serialize($nodes_params),
			  ))
		  ->execute();

		if (!isset($storage['maillists'])) {
		  $storage['maillists'] = $newsletter->get_default_maillists();
		}

		foreach (array_filter($storage['maillists']) as $lid) {
			$maillist = managed_newsletter_maillist::get_all($lid);
			$recipients = $maillist->get_recipients();
			foreach ($recipients as $uid){
				$user_in_queue = db_select('managed_newsletters_send_queue_users', 'mnsqu')
				  ->fields('mnsqu', array('uid'))
				  ->condition('qid',$qid)
				  ->condition('uid', $uid)
				  ->execute()
				  ->rowCount();
				if ($user_in_queue == 0){
					db_insert('managed_newsletters_send_queue_users')
					  ->fields(array(
							'qid' => $qid,
							'uid' => $uid,
						  ))
					  ->execute();
				}
			}
		}
	}
	$storage['qid'] = $qid;
	drupal_set_message('Mail queue has been created.');
}

function managed_newsletters_batch_send($qid, &$context){
  // We can safely process the apachesolr_cron_limit nodes at a time without a
  // timeout or out of memory error.
  $limit = variable_get('managed_newsletters_send_limit', 50);

  $queue_params = db_select('managed_newsletters_send_queue','mnsq')
		  ->fields('mnsq', array('qid', 'tid', 'created', 'nodes_params'))
  		  ->condition('qid', $qid)
		  ->execute()->fetchAssoc();
  $query_users = db_select('managed_newsletters_send_queue_users','mnsqu')
				  ->fields('mnsqu', array('uid'))
				  ->condition('qid', $qid);
  if (empty($context['sandbox'])) {
    $count_all_users = $query_users->execute()->rowCount();
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['failed'] = 0;
    $context['sandbox']['max'] = $count_all_users;
  }
  // With each pass through the callback, retrieve the next group of nids.
  $users = $query_users->orderBy('error', 'asc')->range(0, $limit)->execute();
  $count_sent = managed_newsletters_send_emails($users, $queue_params);

  $context['sandbox']['progress'] += $count_sent['success'] + $count_sent['failed'];
  $context['sandbox']['failed'] += $count_sent['failed'];
  $context['message'] = t('Sent @current of @total emails (@failed failed)', array('@current' => $context['sandbox']['progress'], '@total' => $context['sandbox']['max'], '@failed' => $context['sandbox']['failed']));

  // Inform the batch engine that we are not finished, and provide an
  // estimation of the completion level we reached.
  $context['finished'] = empty($context['sandbox']['max']) ? 1 : $context['sandbox']['progress'] / $context['sandbox']['max'];

  // Put the total into the results section when we're finished so we can
  // show it to the admin.
  if ($context['finished']) {
    $context['results']['count'] = $context['sandbox']['progress'];
    $context['results']['failed'] = $context['sandbox']['failed'];
  }

}

function managed_newsletters_batch_send_finished($success, $results, $operations) {
  $message = '';
  // $results['count'] will not be set if Solr is unavailable.
  if (isset($results['count'])) {
    $message .= format_plural($results['count'], '1 email sent.', '@count emails sent.');
    if ($results['failed']>0) $message .= '<br>' . format_plural($results['count'], '1 email failed.', '@count emails failed.');
  }
  if ($success && $results['failed']==0) {
    $type = 'status';
  }
  else {
    // An error occurred. $operations contains the unprocessed operations.
    $error_operation = reset($operations);
    $message .= ' ' . t('An error occurred while processing @num with arguments :', array('@num' => $error_operation[0])) . print_r($error_operation[0], TRUE) .
	'<br>' .l('View queued emails and resend', 'admin/managed_newsletters/queues');
    $type = 'error';
  }
  drupal_set_message($message, $type);
}

function managed_newsletters_send_emails($users, $queue_params){
  $count_success = 0;
  $count_failed = 0;
  $node_params = unserialize($queue_params['nodes_params']);
  if ($newsletter = managed_newsletter_template::get_all('managed_newsletter_newsletter', $queue_params['tid'])) {
    $newsletter->load();
    foreach ($newsletter->get_blocks_templates() as $tid => $block) {
      if (is_a($block, 'managed_newsletter_block_node')) {
        $newsletter->get_blocks_templates($tid)->load_changed_nodes($node_params[0]);
      }
    }

    foreach ($users as $user) {
      $sender = managed_newsletter_sender::load_default_sender();
	  try{
      	$result = $sender->send_newsletter($newsletter, $user, $queue_params['qid']);
	  }
	  catch (Exception $e) {
		$count_failed++;
		continue;
	  }
	  if ($result) $count_success++;
    }
  }
  else {
    drupal_set_message(t('Messages have not been sent. Newsletter template doesn\'t exist.'), 'error');
  }
  return array('success' => $count_success, 'failed' => $count_failed);
}

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function managed_newsletters_reset_queue($storage) {
  if ($newsletter = managed_newsletter_template::get_all('managed_newsletter_newsletter', $storage['newsletter'])) {
    $newsletter->load();
    foreach ($newsletter->get_blocks_templates() as $tid => $block) {
      if (is_a($block, 'managed_newsletter_block_node')) {
        $newsletter->get_blocks_templates($tid)->reset_queue($storage['queue'][$tid]);
      }
    }
  }
}

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function theme_managed_newsletters_send_form_queue($variables) {
  $form = $variables['form'];
  foreach ((array) $form['queue'] as $tid => $array1) {
    // Skip form properties.
    if (substr($tid, 0, 1) == '#') {
      continue;
    }
    if ($array1['#type'] == 'fieldset') {
      $header = array(
					t('Title'),
				);

      $rows = array();
      foreach ($array1 as $nid => $array2) {
        if (substr($tid, 0, 1) == '#') {
          continue;
        }
        if (is_array($array2) && is_numeric($nid)) {
          $header += $form['queue'][$tid][$nid]['#options'];
          $cells = array();
          $cells[] = $array2['#title'];

          foreach ($form['queue'][$tid][$nid] as $key => $el) {
            if (substr($key, 0, 1) == '#') {
              continue;
            }
            if (isset($header[$key])) {
              unset($el['#title']);
              //$cells[] = drupal_render_children($el);
              $cells[] = drupal_render($el);
            }
          }
          $rows[] = $cells;
          unset($form['queue'][$tid][$nid]);
        }
      }
      $table = theme('table', array('header' => $header, 'rows' => $rows));
      $form['queue'][$tid]['table'] = array(
					'#type' => 'markup', '#markup' => $table
				);
    }
  }
  return drupal_render_children($form);
}

function managed_newsletters_queue_form($form, $form_state){
	$queues = db_select('managed_newsletters_send_queue','mnsq')
		  ->fields('mnsq', array('qid', 'tid', 'created', 'nodes_params'))
		  ->execute();

	$header = array(t('Title'), t('Name'), t('Date Created'), t('Count users'), t('Operations'));
	$rows = array();
    foreach ($queues as $queue) {
		$count_users = db_select('managed_newsletters_send_queue_users','mnsqu')
			  ->fields('mnsqu', array('uid'))
			  ->condition('qid', $queue->qid)
			  ->execute()->rowCount();

		$template = managed_newsletter_template::get_all('managed_newsletter_newsletter', $queue->tid);
		$operations = array();
		$operations[] = l(t('Send emails'), 'admin/managed_newsletters/send_queue/' . $queue->qid);
		$operations[] = l(t('Delete queue'), 'admin/managed_newsletters/delete_queue/' . $queue->qid);

		$cells = array();
		$cells[] = $template->get_title();
		$cells[] = $template->get_name();
		$cells[] = format_date( $queue->created, 'medium');
		$cells[] = $count_users . t(' not sent emails (') . l('view', 'admin/managed_newsletters/queues/' . $queue->qid) . ')';
		$cells[] = implode(' ', $operations);
		$rows[] = $cells;
    }

	if (count($rows)==0) {
		$form['list_queues'] = array(
			'#markup' => 'No active queues',
		);
	}
	else{
		$form['list_queues'] = array(
			'#markup' => theme('table', array('header' => $header, 'rows' => $rows)),
		);
	}
	return $form;
}

function managed_newsletters_queue_users_form($form, $form_state, $qid){
	if (!isset($qid)) {
		$form['list_queues'] = array(
			'#markup' => 'Argument queue is required',
		);
		return $form;
	}

	$header = array(t('Name'), t('Email'), t('Error'), t('Operations'));
	$rows = array();
	$users = db_select('managed_newsletters_send_queue_users','mnsqu')
		  ->fields('mnsqu', array('uid', 'error'))
		  ->condition('qid', $qid)
		  ->execute();

	foreach ($users as $user){
		$account = user_load($user->uid);
		$cells = array();
		$cells[] = $account->name;
		$cells[] = $account->mail;
		$cells[] = $user->error;
		$cells[] = l('Edit', 'user/' . $account->uid . '/edit') . ' ' . l('View', 'user/' . $account->uid);
		$rows[] = $cells;
	}

	if (count($rows)==0) {
		$form['list_queues'] = array(
			'#markup' => 'No users in queue',
		);
	}
	else{
		$form['list_queues'] = array(
			'#markup' => theme('table', array('header' => $header, 'rows' => $rows)),
		);
	}
	return $form;
}

function managed_newsletters_send_queue_action($form, $form_state, $qid){
  $form['qid'] = array(
	  '#type' => 'hidden',
	  '#value' => $qid,
  );
 $count_not_sent = db_select('managed_newsletters_send_queue_users','mnsqu')
			  ->fields('mnsqu', array('uid'))
			  ->condition('qid', $qid)
			  ->execute()->rowCount();
  $form['info'] = array(
	  '#markup' => '<p>' . t('This queue has !count not sent emails', array('!count'=>$count_not_sent)) . '</p>',
  );

  $form['submit'] = array(
	  '#type' => 'submit',
	  '#value' => t('Send queue'),
  );

  return $form;
}


function managed_newsletters_send_queue_action_submit($form, $form_state){
  managed_newsletters_batch_send_action($form_state['values']);
}

function managed_newsletters_batch_send_action($newsletter){
  $batch = array(
    'operations' => array(
      array('managed_newsletters_batch_send', array($newsletter['qid'])),
    ),
    'finished' => 'managed_newsletters_batch_send_finished',
    'title' => t('Sending'),
    'init_message' => t('Preparing to send newsletters'),
    'progress_message' => t('Sending newsletters...'),
    'error_message' => t('Newsletter send process has encountered an error.'),
    'file' => drupal_get_path('module', 'managed_newsletters') . '/managed_newsletters.admin.send.inc',
  );
  batch_set($batch);
}

function managed_newsletters_delete_queue_action($qid){
	 $tid = db_select('managed_newsletters_send_queue', 'mnsq')
			 ->condition('qid', $qid)
			 ->fields('mnsq', array('tid'))
			 ->execute()->fetchCol();
	 db_delete('managed_newsletters_send_queue_users')
				->condition('qid', $qid)
				->execute();
	 db_delete('managed_newsletters_send_queue')->condition('qid', $qid)->execute();
	 drupal_set_message('Queue deleted.');
	 drupal_goto('admin/managed_newsletters/queues');
}