<?php
/**
 * @file
 * gcl_google_analytics.domains.inc
 */

/**
 * Implements hook_domain_conf_default_variables().
 */
function gcl_google_analytics_domain_conf_default_variables() {
$domain_variables = array();
  $domain_variables['gambling_compliance_d7_stage_stage1_codeenigma_com'] = array();
  $domain_variables['gambling_data_new_stage_stage1_codeenigma_com'] = array(
  'googleanalytics_account' => 'UA-25448550-2',
  'googleanalytics_domain_mode' => '0',
  'googleanalytics_cross_domains' => '',
  'googleanalytics_visibility_pages' => '0',
  'googleanalytics_pages' => 'admin
admin/*
batch
node/add*
node/*/*
user/*/*',
  'googleanalytics_visibility_roles' => '1',
  'googleanalytics_roles' => array(
    3 => '3',
    4 => '4',
    6 => '6',
    7 => '7',
    1 => 0,
    2 => 0,
    10 => 0,
    13 => 0,
    11 => 0,
    14 => 0,
    12 => 0,
  ),
  'googleanalytics_custom' => '0',
  'googleanalytics_trackoutbound' => 1,
  'googleanalytics_trackmailto' => 1,
  'googleanalytics_trackfiles' => 1,
  'googleanalytics_trackfiles_extensions' => '7z|aac|arc|arj|asf|asx|avi|bin|csv|doc|exe|flv|gif|gz|gzip|hqx|jar|jpe?g|js|mp(2|3|4|e?g)|mov(ie)?|msi|msp|pdf|phps|png|ppt|qtm?|ra(m|r)?|sea|sit|tar|tgz|torrent|txt|wav|wma|wmv|wpd|xls|xml|z|zip',
  'googleanalytics_trackmessages' => array(
    'status' => 0,
    'warning' => 0,
    'error' => 0,
  ),
  'googleanalytics_site_search' => 0,
  'googleanalytics_trackadsense' => 0,
  'googleanalytics_trackdoubleclick' => 0,
  'googleanalytics_tracker_anonymizeip' => 0,
  'googleanalytics_privacy_donottrack' => 1,
  'tracking__active_tab' => 'edit-role-vis-settings',
  'googleanalytics_custom_var' => array(
    'slots' => array(
      1 => array(
        'slot' => 1,
        'name' => '',
        'value' => '',
        'scope' => '3',
      ),
      2 => array(
        'slot' => 2,
        'name' => '',
        'value' => '',
        'scope' => '3',
      ),
      3 => array(
        'slot' => 3,
        'name' => '',
        'value' => '',
        'scope' => '3',
      ),
      4 => array(
        'slot' => 4,
        'name' => '',
        'value' => '',
        'scope' => '3',
      ),
      5 => array(
        'slot' => 5,
        'name' => '',
        'value' => '',
        'scope' => '3',
      ),
    ),
  ),
  'googleanalytics_cache' => 0,
  'googleanalytics_codesnippet_before' => '',
  'googleanalytics_codesnippet_after' => '',
  'googleanalytics_js_scope' => 'header',
);

return $domain_variables;
}
