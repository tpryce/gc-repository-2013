<?php

/**
 * @file
 * freetrialform.tpl.php
 *
 * Theme implementation to display a simple pager.
 *
 * Default variables:
 * - $first_link: A formatted <A> link to the first item.
 * - $previous_link: A formatted <A> link to the previous item.
 * - $next_link: A formatted <A> link to the next item.
 * - $last_link: A formatted <A> link to the last item.
 *
 */
?>
<div class="free-trial-form">
  <p>To Read this article</p>
  <p><?php print $register_link; ?> or <?php print $signin ?> to read the full article.</p>
  <div class="instant-form clearfix">
    <div class="instant-left">
      <p>Instant access to gaming industry</p>
      <p>insight and intelligence</p></div>
    <div class="instant-right"><p><?php print $instant_button ?></p></div>
  </div>
</div>
