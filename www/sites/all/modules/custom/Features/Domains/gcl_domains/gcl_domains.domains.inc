<?php
/**
 * @file
 * gcl_domains.domains.inc
 */

/**
 * Implements hook_domain_default_domains().
 */
function gcl_domains_domain_default_domains() {
$domains = array();
  $domains['gamblingcompliance_com'] = array(
  'subdomain' => 'gamblingcompliance.com',
  'sitename' => 'Gambling Compliance',
  'scheme' => 'http',
  'valid' => '1',
  'weight' => '-1',
  'is_default' => '1',
  'machine_name' => 'gamblingcompliance_com',
);
  $domains['gamblingdata_com'] = array(
  'subdomain' => 'gamblingdata.com',
  'sitename' => 'Gambling Data',
  'scheme' => 'http',
  'valid' => '1',
  'weight' => '2',
  'is_default' => '0',
  'machine_name' => 'gamblingdata_com',
);

return $domains;
}

/**
 * Implements hook_domain_alias_default_aliases().
 */
function gcl_domains_domain_alias_default_aliases() {
$domain_aliases = array();
  $domain_aliases['gamblingcompliance_com'] = array(
  'gamblingcompliance.local' => array(
    'pattern' => 'gamblingcompliance.local',
    'redirect' => '0',
    'machine_name' => 'gamblingcompliance_com',
  ),
);
  $domain_aliases['gamblingdata_com'] = array(
  'gamblingdata.local' => array(
    'pattern' => 'gamblingdata.local',
    'redirect' => '0',
    'machine_name' => 'gamblingdata_com',
  ),
);

return $domain_aliases;
}
