<?php
// $Id$

class managed_newsletter_block_node extends managed_newsletter_block {
  private $node_type;
  private $node_action;
  private $group_by;
  private $group_html;
  private $group_text;
  private $node_html;
  private $node_text;

  private $node_groups;

  public function set_node_types($value) {
    $this->node_type = array_filter($value);
  }

  public function set_node_actions($value) {
    $this->node_action = array_filter($value);
  }

  public function set_group_by($value) {
    $this->group_by = $value;
  }

  public function get_node_types() {
    return (array) $this->node_type;
  }

  public function get_node_actions() {
    return (array) $this->node_action;
  }

  public function get_group_by() {
    return $this->group_by;
  }

  public function load() {
    parent::load();
    $result = db_query('SELECT * FROM {managed_newsletters_block_node_settings} WHERE tid = :tid', array(':tid' => $this->get_tid()));
    //while ($row = db_fetch_array($result)) {
    while ($row = $result->fetchAssoc()) {
      if ($row['name'] == 'node_action' || $row['name'] == 'node_type') {
        $this->{$row['name']}[] = $row['value'];
      }
      else {
        $this->{$row['name']} = $row['value'];
      }
    }
  }

  public function get_edit_form() {
    $form = parent::get_edit_form();
    $form['node_types'] = array(
		'#type' => 'checkboxes',
		'#title' => t('Node types'),
		'#options' => node_type_get_names(),
		'#default_value' => (count($this->get_node_types()) > 0 ) ? array_combine($this->get_node_types(), $this->get_node_types()) : array(),
		'#description' => t('Select types of nodes which can be added in the block')
		);

    $form['node_actions'] = array(
		'#type' => 'checkboxes',
		'#title' => t('Node actions'),
		'#options' => array('insert' => 'insert', 'update' => 'update'),
		'#default_value' => (count($this->get_node_actions()) > 0 ) ? array_combine($this->get_node_actions(), $this->get_node_actions()) : array(),
		'#description' => t('Select types of nodes which can be added in the block')
		);


    $group_by = array('' => t('None'));
    foreach (taxonomy_get_vocabularies() as $voc) {
      $group_by['vocabulary_' . $voc->vid] = $voc->name;
    }

    $form['group_by'] = array(
		'#type' => 'select',
		'#title' => t('Group By'),
		'#options' => $group_by,
		'#default_value' => $this->get_group_by(),
		'#description' => t('')
		);

    $form['templates'] = array(
		'#type' => 'fieldset',
		'#title' => t('Templates')
		);

    $form['templates']['template_group'] = array(
		'#type' => 'fieldset',
		'#title' => t('Group'),
		'#tree' => true
		);
    $form['templates']['template_group']['html'] = array(
		'#type' => 'textarea',
		'#title' => t('Html template'),
		'#default_value' => $this->group_html,
		'#required' => true,
		'#description' => t('Used for generation of html part of newsletters')
		);
    $form['templates']['template_group']['text'] = array(
		'#type' => 'textarea',
		'#title' => t('Text template'),
		'#required' => true,
		'#default_value' => $this->group_text,
		'#description' => t('Used for generation of plain text part of newsletters')
		);

    $form['templates']['template_group']['tokens'] = array(
		'#type' => 'fieldset',
		'#title' => t('Available tokens'),
		'#weight' => 9,
		'#collapsible' => true,
		'#collapsed' => true
		);
    $form['templates']['template_group']['tokens']['tokens'] = array(
		'#type' => 'markup',
		'#markup' => $this->get_tokens_table($this->get_tokens('group'))
		);

    $form['templates']['template_node'] = array(
		'#type' => 'fieldset',
		'#title' => t('Node'),
		'#tree' => true
		);
    $form['templates']['template_node']['html'] = array(
		'#type' => 'textarea',
		'#title' => t('Html template'),
		'#default_value' => $this->node_html,
		'#required' => true,
		'#description' => t('Used for generation of html part of newsletters')
		);
    $form['templates']['template_node']['text'] = array(
		'#type' => 'textarea',
		'#title' => t('Text template'),
		'#required' => true,
		'#default_value' => $this->node_text,
		'#description' => t('Used for generation of plain text part of newsletters')
		);
    $form['templates']['template_node']['tokens'] = array(
		'#type' => 'fieldset',
		'#title' => t('Available tokens'),
		'#weight' => 9,
		'#collapsible' => true,
		'#collapsed' => true
		);
    $form['templates']['template_node']['tokens']['tokens'] = array(
		'#type' => 'markup',
		'#markup' => $this->get_tokens_table($this->get_tokens('node'))
		);

    return $form;
  }

  public function validate_values($values) {
    parent::validate_values($values);
  }

  public function update($values) {
    parent::update($values);
    $this->set_node_types($values['node_types']);
    $this->set_node_actions($values['node_actions']);
    $this->set_group_by($values['group_by']);
    $this->group_html = $values['template_group']['html'];
    $this->group_text = $values['template_group']['text'];
    $this->node_html = $values['template_node']['html'];
    $this->node_text = $values['template_node']['text'];
  }

  public function save() {
    parent::save();
    // TODO Please review the conversion of this statement to the D7 database API syntax.
	// TODO need test
    /* db_query('DELETE FROM {managed_newsletters_block_node_settings} WHERE tid = %d', $this->get_tid()) */
    db_delete('managed_newsletters_block_node_settings')
	  ->condition('tid', $this->get_tid())
	  ->execute();

    foreach ($this->get_node_types() as $type) {
		db_insert('managed_newsletters_block_node_settings')
			->fields(array(
					'tid' => $this->get_tid(),
					'name' => 'node_type',
					'value' => $type
					))
			->execute();
    }

    foreach ($this->get_node_actions() as $action) {
		db_insert('managed_newsletters_block_node_settings')
			->fields(array(
					'tid' => $this->get_tid(),
					'name' => 'node_action',
					'value' => $action
					))
			->execute();
    }

	db_insert('managed_newsletters_block_node_settings')
			->fields(array(
					'tid' => $this->get_tid(),
					'name' => 'group_by',
					'value' => $this->get_group_by()
					))
			->execute();
    db_insert('managed_newsletters_block_node_settings')
			->fields(array(
					'tid' => $this->get_tid(),
					'name' => 'group_html',
					'value' => $this->group_html
					))
			->execute();
    db_insert('managed_newsletters_block_node_settings')
			->fields(array(
					'tid' => $this->get_tid(),
					'name' => 'group_text',
					'value' => $this->group_text
					))
			->execute();
    db_insert('managed_newsletters_block_node_settings')
			->fields(array(
					'tid' => $this->get_tid(),
					'name' => 'node_html',
					'value' => $this->node_html
					))
			->execute();
    db_insert('managed_newsletters_block_node_settings')
			->fields(array(
					'tid' => $this->get_tid(),
					'name' => 'node_text',
					'value' => $this->node_text
					))
			->execute();
  }

  public function delete() {

    db_delete('managed_newsletters_block_node_settings')
	  ->condition('tid', $this->get_tid())
	  ->execute();
    db_delete('managed_newsletters_block_node_queue')
	  ->condition('tid', $this->get_tid())
	  ->execute();
    parent::delete();
  }

  public static function add_in_queue($tid, $nid) {
    if (!managed_newsletter_block_node::is_in_queue($tid, $nid)) {
      db_insert('managed_newsletters_block_node_queue')
			  ->fields(array(
					//'tqid' => '',
					'tid' => $tid,
					'nid' => $nid,
					'added' => REQUEST_TIME,
					'sorting' => 0,
				  ))
			  ->execute();
    }
  }

  public static function delete_from_queue($tid, $nid) {
    db_delete('managed_newsletters_block_node_queue')
	  ->condition('tid', $tid)
	  ->condition('nid', $nid)
	  ->execute();
  }

  public static function get_queues($type, $op = null) {
    if ($op) {
      $query = db_select('managed_newsletters_block_node_settings', 't1');
	  $block_alias = $query->join('managed_newsletters_block_node_settings', 't2', 't1.tid=t2.tid');
	  $result = $query->condition('t1.name', 'node_type')
			  ->condition('t2.name', 'node_action')
			  ->condition('t1.value', $type)
			  ->condition('t2.value', $op)
	  		  ->fields('t1', array('tid'))
			  ->execute();
    }
    else {
	  $result = db_select('managed_newsletters_block_node_settings', 't1')
			  ->condition('t1.name', 'node_type')
			  ->condition('t1.value', $type)
	  		  ->fields('t1', array('tid'))
			  ->execute();
    }
    $tids = array();
    while ($row = $result->fetchAssoc()) {
      $tids[] = $row['tid'];
    }
    return $tids;
  }

  public static function is_in_queue($tid, $nid) {
    return db_query('SELECT count(*) as c FROM {managed_newsletters_block_node_queue} WHERE tid = :tid AND nid = :nid', array(':tid' => $tid, ':nid' => $nid))->fetchField();
  }

  public function get_tokens($field = null) {
    $tokens = parent::get_tokens();
    $tokens += $this->get_class_tokens();
    return $tokens;
  }

  public function get_class_tokens($field = null) {
    $class = get_class($this);
    $tokens[$class]['managed-newsletter']['managed-newsletter:node-block-content'] = 'Content of node block (set of nodes or groups of nodes)';
    switch ($field) {
      case 'group':
        $tokens[$class]['managed-newsletter']['managed-newsletter:group-content'] = 'Set of nodes in the group';
        $tokens[$class]['managed-newsletter']['managed-newsletter:group-title'] = 'Title of the group (term name, etc.)';
        break;
      case 'node':
        $tokens['node'] = token_get_info('node');
        break;
    }
    return $tokens;
  }

  public function build_html_content($account) {
    $objects = array(
		'global' => new stdClass(),
		'user' => $account,
		);
    $result = token_replace($this->build_inner_content(1,$objects), $objects);
    return $result;
  }

  public function build_text_content($account) {
    $objects = array(
		'global' => new stdClass(),
		'user' => $account,
		);
    $result = token_replace($this->build_inner_content(0,$objects), $objects);
    return $result;
  }

  private function build_inner_content($html = 1, $objects) {
    $output = '';
    /*$groups = array();
	global $user;
    $objects['user'] = $user;*/
    foreach ($this->get_nodes() as $group_title => $nodes) {
      $values['managed-newsletter:group-title'] = $group_title;

      $values['managed-newsletter:group-content'] = '';
	  //echo "nodes"; dpr($nodes);
      foreach ($nodes as $node) {
        $objects['node'] = $node;
        $template = $html ? $this->node_html : $this->node_text;
        $values['managed-newsletter:group-content'] .=  token_replace($template, $objects);
      }
      $group_template = $html ? $this->group_html : $this->group_text;
      $objects['managed-newsletter'] = $values;
		$output .= token_replace($group_template, $objects);
    }
    $block_template = $html ? $this->get_html() : $this->get_text();
    $values = array();
    $values['managed-newsletter:node-block-content'] = $output;
	$objects ['managed-newsletter'] = $values;
    $output = token_replace($block_template, $objects);;

    return $output;
  }

  public function get_nodes() {
    if (!isset($this->node_groups)) {
      $this->node_groups = array();
      $result = db_select('managed_newsletters_block_node_queue', 'm')
	  			->fields('m')
	  			->condition('tid', $this->get_tid())
			    ->orderBy('sorting')
			    ->orderBy('added', 'DESC')
			    ->execute();

      $group_by = explode('_', $this->get_group_by());
      while ($row = $result->fetchAssoc()) {
        $node = node_load($row['nid']);
        if ($node !== FALSE && $node->nid) {
          if ($group_by[0] == 'vocabulary' && $group_by[1]) {
			$tax = managed_newsletters_node_get_terms($node);
            foreach ($tax as $tid => $term) {
              if ($term->vid == $group_by[1]) {
                $this->node_groups[$term->name][$node->nid] = $node;
                break;
              }
            }
          }
          else {
            $this->node_groups[0][$node->nid] = $node;
          }
        }
      }
    }
    return $this->node_groups;
  }

  public function load_changed_nodes($values) {
    $this->node_groups = array();
    if (is_array($values)) {
      uasort($values, array("managed_newsletter_block_node", "sort_by_order"));
      foreach ($values as $name => $group) {
        if (count($group['nodes']) > 0) {
          $this->node_groups[$group['name']] = array();
          uasort($group['nodes'], array("managed_newsletter_block_node", "sort_by_order"));
          foreach ($group['nodes'] as $nid => $n) {
            $node = node_load($nid);
            if ($node->nid) {
              $node->title = preg_replace("#(<\/?)(\w+)([^>]*>)#e", "", $n['title']);
			  $lang = isset($node->language) ? $node->language : LANGUAGE_NONE;
              $node->body[$lang][0]['safe_summary'] = preg_replace("#(<\/?)(\w+)([^>]*>)#e", "", $n['teaser']);
              $this->node_groups[$group['name']][$node->nid] = $node;
            }
          }
        }

      }
    }
  }
  public static function sort_by_order($a, $b) {
    $al = intval($a['order']);
    $bl = intval($b['order']);

    if ($al == $bl) {
      return 0;
    }
    return ($al > $bl) ? + 1 : -1;
  }

  public function reset_queue($values) {
    $nids = array(0);
    foreach ($values as $node => $action) {
      if ($action == 'hold') {
        $nids[] = $node;
      }
    }
    db_delete('managed_newsletters_block_node_queue')
			->condition('tid', $this->get_tid())
			->condition('nid', $nids, 'NOT IN')
			->execute();
  }

}

