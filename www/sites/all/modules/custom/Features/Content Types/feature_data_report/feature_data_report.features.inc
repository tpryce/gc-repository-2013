<?php
/**
 * @file
 * feature_data_report.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function feature_data_report_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function feature_data_report_node_info() {
  $items = array(
    'data_report' => array(
      'name' => t('Data Report'),
      'base' => 'node_content',
      'description' => t('GamblingData reports'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
