<?php
/**
 * @file
 * feature_white_paper.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function feature_white_paper_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function feature_white_paper_node_info() {
  $items = array(
    'white_paper' => array(
      'name' => t('Special Reports'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
