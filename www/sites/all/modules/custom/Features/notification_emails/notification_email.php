<?php
class notification_email {
  private $delta;
  private $source;
  private $email;
  private $from_email;
  private $subject;
  private $admin_message;
  private $restrict_dynamic;
  private $title;
  private $description;
  private $additional_data;
  private $use_node = true;
  private $use_user = true;

  /**
   * Class cunstructor
   * Needs unique id of notification email. Uses for identify email
   *
   * @param string $delta
   */
  public function __construct($delta) {
    $this->delta = $delta;
  }

  /**
   * Inits object with default values
   * Uses in hook implementations for supply default values
   *
   * @param string $source
   * @param string $email
   * @param string $subject
   * @param string $admin_message
   * @param string $title
   * @param string $description
   * @param array $additional_data
   */
  public function init($source, $email, $from_email, $subject, $admin_message, $title, $description, $use_user = true, $use_node = true, $additional_data = array()) {
    $this->source           = $source;
    $this->email            = $email;
    $this->from_email       = $from_email;
    $this->subject          = $subject;
    $this->admin_message    = $admin_message;
    $this->title            = $title;
    $this->description      = $description;
    $this->use_user         = $use_user;
    $this->use_node         = $use_node;
    $this->additional_data  = $additional_data;
  }

  /**
   * Loads object values from DB or default values
   * Return true of values loaded
   *
   * @return true
   */
  public function load() {
    if ($t = $this->get($this->delta)) {
      foreach (get_object_vars($t) as $key => $value) {
        $this->$key = $value;
      }
      return true;
    }
    return false;
  }

  /**
   * Sets dinamic email
   * Can be used by modules for send notification to different emails
   * If dinamic email address defined, email will be sent to this address instead of admin address from settings
   *
   * @param string $email
   */
  public function set_dinamic_email($email) {
    $this->dinamic_email = $email;
  }

  /**
   * Returns email delta
   *
   * @return string
   */
  public function get_delta() {
    return $this->delta;
  }

  /**
   * Returns email source (module name)
   *
   * @return string
   */
  public function get_source() {
    return $this->source;
  }

  /**
   * Returns email address
   *
   * @return string
   */
  public function get_email() {
    return $this->email;
  }

	/**
   * Returns from email address
   *
   * @return string
   */
  public function get_from_email() {
    return $this->from_email;
  }

  /**
   * Returns email subject
   *
   * @return string
   */
  public function get_subject() {
    return $this->subject;
  }

  /**
   * Returns email body
   *
   * @return string
   */
  public function get_body() {
    return $this->admin_message;
  }

  /**
   * Returns restrict dynamic flag
   *
   * @return string
   */
  public function get_restrict_dynamic() {
    return $this->restrict_dynamic;
  }

  /**
   * Returns notification email title for administer page
   *
   * @return string
   */
  public function get_title() {
    return $this->title;
  }

  /**
   * Returns notification email description for administer page
   *
   * @return string
   */
  public function get_description() {
    return $this->description;
  }

  /**
   * Returns array with kays of additional data which can be used in email templates
   *
   * @return array
   */
  public function get_additional_data() {
    return $this->additional_data;
  }

  /**
   * Loads notification emails from all modules, override object values with data from db
   * If called without delta, returns all emails, with delta - only requested email
   *
   * @param string $delta
   * @return mix
   */
  private static function get($delta = null) {
    static $notifications;
    if (!isset($notifications)) {
      $notifications = module_invoke_all('notification_emails');
    }

    //$sql  = 'SELECT * FROM {notification_emails}';
    $sql  = db_select('notification_emails','n')->fields('n');

    if ($delta != null) {
      //$sql .= ' WHERE delta = \'%s\'';
      $sql->condition('delta', array($delta));
    }

    //$result = db_query($sql, array($delta));
    $result = $sql->execute();
    //while ($row = db_fetch_array($result)) {
    foreach ($result as $row) {
      if (is_a($notifications[$row->delta], 'notification_email')) {
        $notifications[$row->delta]->load_values($row);
      }
    }
    return $delta ? $notifications[$delta] : $notifications;
  }

  /**
   * Overrides object vars with values from db
   *
   * @param array $values
   */
  private function load_values($values) {
    foreach ($values as $key => $value) {
      $this->$key = $value;
    }
  }

  /**
   * Returns array with all notification emails defined in system
   *
   * @return array
   */
  static public function get_all() {
    static $emails;
    if (!isset($emails)) {
      $emails = notification_email::get();
    }
    return $emails;
  }

  /**
   * Sends notification email, adds supplied values in templates
   *
   *
   * @param stdClass $node
   * @param stdClass $user
   * @param array $additional_variables
   */
  public function send($node, $user, $additional_variables = array()) {
    $this->load();
    $to = (!$this->get_restrict_dynamic() && isset($this->dinamic_email)) ? $this->dinamic_email : $this->get_email();
    $from_email = (isset($this->from_email) && $this->from_email != '') ? $this->from_email : variable_get('site_mail', ini_get('sendmail_from'));

    // Tweak to avoid call to get_template_parameters() when message is from
    // max_download module. Reason is that it uses profile2 module, which is not
    // installed (and is not likely to be in the future).
    if ($this->delta == MAX_DOWNLOAD_NOTIFICATION_DELTA) {
      $additional_variables['!user_link'] = l($user->name, 'user/' . $user->uid, array('absolute' => TRUE));
      $params = array();
      $params['subject'] = t($this->get_subject(), $additional_variables);
      $params['body'] = t($this->get_body(), $additional_variables);
    }
    // General case.
    else {
      $params = $this->get_template_parameters($node, $user, $additional_variables);
      $params['subject'] = t($this->get_subject(), $params);
      $params['body'] = t($this->get_body(), $params);
    }
    $language = $user ? user_preferred_language($user) : language_default();
    return drupal_mail('notification_emails', $this->get_delta(), $to, $language, $params, $from_email);
  }

  /**
   * Array with available template variables
   *
   * @param stdClass $user
   * @param stdClass $node
   * @param array $additional_variables
   * @return array
   */
  public function get_template_parameters($node = null, $user = null, $additional_variables = array()) {
    $parameters = array();
    if ($this->use_user && $user) {
      //foreach (get_object_vars($user) as $user_key => $user_var) {
      foreach ($user as $user_key => $user_var) {
        //TODO: temp fix for locations, need to use tokens, even better - move to rules module
        if (is_array($user_var)) {
          foreach ($user_var as $k2 => $val2) {
            $parameters['!user_' . $user_key . '_' . $k2] = $val2;
          }
        }
        else {
          $parameters['!user_' . $user_key] = $user_var;
        }
      }
      $parameters['!user_link'] = l($user->name, 'user/' . $user->uid, array('query' => null, 'fragment' => null, 'absolute' => TRUE));

		//if (isset($user->profile_main)) $profile = $user->profile_main;
		//else $profile =profile2_load_by_user($user->uid, 'main');
		//$arr_profile = get_object_vars($profile);
		//if (count($arr_profile)>0 && $profile !== FALSE){
			//foreach ($arr_profile as &$item){
				//if (!is_array($item)) {
					//unset($item);
				//}
			//}
			//foreach ($arr_profile as $profile_key => $profile_var) {
			////TODO: temp fix for locations, need to use tokens, even better - move to rules module
			//if (strstr($profile_key, 'field_profile')==0 ) {
				//if (is_array($profile_var) && isset($profile_var['und'][0]['value'])){
					//$parameters['!' . $profile_key] = $profile_var['und'][0]['value'];
				//}
				//else {
					//$parameters['!' . $profile_key] = '';
				//}
			//}
			//}
		//}
	  //dprint_r($profile);
      unset($parameters['!user_pass']);
    }
    if ($this->use_node && $node) {
      foreach (get_object_vars($node) as $node_key => $node_var) {
        $parameters['!node_' . $node_key] = $node_var;
      }
      $parameters['!node_link'] = l($node->title, 'node/' . $node->nid, array('query' => null, 'fragment' => null, 'absolute' => TRUE));
      unset($parameters['!node_taxonomy']);
    }
    $parameters['!date'] = format_date(REQUEST_TIME);

    foreach ((array) $this->get_additional_data() as $index => $key) {
      $parameters[$key] = isset($additional_variables[$key]) ? $additional_variables[$key] : '';
    }
    return $parameters;
  }

  /**
   * Saves new values of changeable object vars in db
   *
   * @param string $email
   * @param string $subject
   * @param string $admin_message
   */
  public function save($email, $from_email, $subject, $admin_message, $restrict_dynamic) {
    $this->email = $email;
    $this->from_email = $from_email;
    $this->subject = $subject;
    $this->admin_message = $admin_message;
    $this->restrict_dynamic = $restrict_dynamic;
    // TODO Please review the conversion of this statement to the D7 database API syntax.
    /* db_query("DELETE FROM {notification_emails} WHERE delta = '%s'", $this->get_delta()) */
    db_delete('notification_emails')
	  ->condition('delta', $this->get_delta())
	  ->execute();
    //$sql = 'INSERT INTO {notification_emails} (delta, source, email, subject, admin_message, restrict_dynamic) VALUES (\'%s\', \'%s\', \'%s\', \'%s\', \'%s\', %d)';
    // TODO Please review the conversion of this statement to the D7 database API syntax.
    /* db_query($sql, $this->get_delta(), $this->get_source(), $this->get_email(), $this->get_subject(), $this->get_body(), $this->get_restrict_dynamic()) */
    $id = db_insert('notification_emails')
	  ->fields(array(
		  'delta' => $this->get_delta(),
		  'source' => $this->get_source(),
		  'email' => $this->get_email(),
		  'from_email' => $this->get_from_email(),
		  'subject' => $this->get_subject(),
		  'admin_message' => $this->get_body(),
		  'restrict_dynamic' => $this->get_restrict_dynamic(),
		))
	  ->execute();
  }
}

/**
 * Modify the drupal mail system to send HTML emails.
 */
class NotificationMailSystem implements MailSystemInterface {
  /**
   * Concatenate and wrap the e-mail body for plain-text mails.
   *
   * @param $message
   *   A message array, as described in hook_mail_alter().
   *
   * @return
   *   The formatted $message.
   */
  public function format(array $message) {
    $message['body'] = implode("\n\n", $message['body']);
    return $message;
  }

  /**
   * Send an e-mail message, using Drupal variables and default settings.
   *
   * @see <a href="http://php.net/manual/en/function.mail.php
" title="http://php.net/manual/en/function.mail.php
" rel="nofollow">http://php.net/manual/en/function.mail.php
</a>   * @see drupal_mail()
   *
   * @param $message
   *   A message array, as described in hook_mail_alter().
   * @return
   *   TRUE if the mail was successfully accepted, otherwise FALSE.
   */
  public function mail(array $message) {
    $mimeheaders = array();
    foreach ($message['headers'] as $name => $value) {
      $mimeheaders[] = $name . ': ' . mime_header_encode($value);
    }
    $line_endings = variable_get('mail_line_endings', MAIL_LINE_ENDINGS);
    return mail(
      $message['to'],
      mime_header_encode($message['subject']),
      // Note: e-mail uses CRLF for line-endings. PHP's API requires LF
      // on Unix and CRLF on Windows. Drupal automatically guesses the
      // line-ending format appropriate for your system. If you need to
      // override this, adjust $conf['mail_line_endings'] in settings.php.
      preg_replace('@\r?\n@', $line_endings, $message['body']),
      // For headers, PHP's API suggests that we use CRLF normally,
      // but some MTAs incorrectly replace LF with CRLF. See #234403.
      join("\n", $mimeheaders)
    );
  }
}
