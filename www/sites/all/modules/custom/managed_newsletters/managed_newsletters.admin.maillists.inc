<?php
// $Id$

/**
 * List of all maillists
 * @return string
 */
function managed_newsletters_maillists_overview() {
  $header = array(t('Title'), t('Operations'));
  $rows = array();
  foreach (managed_newsletters_get_all_maillist_classes() as $class => $title) {
    $rows[] = array(array('data' => $title), array('data' => l(t('Add new'), 'admin/managed_newsletters/maillists/add/' . $class)));
    foreach (managed_newsletter_maillist::get_all(null, $class) as $id => $maillist) {
      /*@var $maillist managed_newsletter_maillists*/
      $operations = array();
      $operations[] = l(t('Preview'), 'admin/managed_newsletters/maillists/preview/' . $maillist->get_lid());
      $operations[] = l(t('Edit'), 'admin/managed_newsletters/maillists/edit/' . $maillist->get_lid());
      $operations[] = l(t('Delete'), 'admin/managed_newsletters/maillists/delete/' . $maillist->get_lid());
      $cells = array();
      $cells[] = $maillist->get_title();
      $cells[] = implode(' ', $operations);
      $rows[] = $cells;
    }
  }

  return theme('table', array('header' => $header, 'rows' => $rows));
}

/**
 * Form for creation or edition of template
 *
 * @param array $form_state
 * @param string $class
 * @param int $tid
 * @return string
 */
function managed_newsletters_maillist_form($form, $form_state, $class = null, $lid = null) {
  if ($lid) {
    $maillist = managed_newsletter_maillist::get_all($lid);
  }
  elseif ($class && class_exists($class)) {
    $maillist = new $class();
  }
  if ($maillist) {
    return $maillist->get_edit_form();
  }
}

/**
 * Saves template in database
 *
 * @param $form
 * @param $form_state
 */
function managed_newsletters_maillist_form_submit($form, &$form_state) {
  $maillist = $form_state['values']['maillist'];

  /*@var $maillist managed_newsletter_maillist*/
  $maillist->update($form_state['values']);
  $maillist->save();
  drupal_set_message(t('The maillist has been saved.'));

  // rebuild administer menu
  menu_rebuild();

  $form_state['redirect'] = 'admin/managed_newsletters/maillists';
}

/**
 * Shows confimation form for template deletion
 *
 * @param array $form_state
 * @param int $tid
 * @return array
 */
function managed_newsletters_maillist_delete($form, $form_state, $lid) {
  $maillist = managed_newsletter_maillist::get_all($lid);
  if ($maillist->get_lid()) {
    $form = array();
    $form['maillist'] = array(
			'#type' => 'value',
			'#value' => $maillist,
		);
    return confirm_form($form, 
			t('Are you sure you want to delete maillist "%name"?', array('%name' => $maillist->get_title())), 
	    	'admin/managed_newsletters/maillists', 
			t('This action cannot be undone.'), 
			t('Delete'), t('Cancel'));
  }
  else {
    drupal_not_found();
    exit;
  }
}

/**
 * Removes template
 *
 * @param string $form
 * @param array $form_state
 */
function managed_newsletters_maillist_delete_submit($form, &$form_state) {
  $maillist = $form_state['values']['maillist'];

  $message = array(
		'Maillist "%name" has been removed.',
		array('%name' => $maillist->get_title())
	);
  watchdog('managed_newsletters', $message[0], $message[1]);
  drupal_set_message(t($message[0], $message[1]));

  $maillist->delete();

  // rebuild administer menu
  menu_rebuild();

  $form_state['redirect'] = 'admin/managed_newsletters/maillists';
}

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function managed_newsletters_maillist_preview($lid) {
  if ($maillist = managed_newsletter_maillist::get_all($lid)) {
    $output = '';
    $rows = array();
    foreach ($maillist->get_recipients() as $email => $uid) {
      $rows[] = array($uid, $email);
    }
    if (is_a($maillist, 'managed_newsletter_maillist_views')) {
      $view = views_get_view($maillist->get_view());
      if ($view) {
        if ($view->get_path()) {
          $output .= '<p>' . l('Open view', $view->get_path(), array('attributes' => array('target' => '_blank'))) . '</p>';
        }
      }
      else {
        drupal_set_message(t("View !view doesn't exist", array('!view' => $maillist->get_view())), 'error');
        return '';
      }
    }
    $header = array('uid', 'mail');
    $output .= theme('table', array('header' => $header, 'rows' => $rows));

    return $output;
  }
  else {
    drupal_not_found();
    exit;
  }
}
