<?php
/**
 * @file
 * feature_regulatory_news.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function feature_regulatory_news_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_trunk_categories|node|regulatory_news|Americas_Professional';
  $field_group->group_name = 'group_trunk_categories';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'regulatory_news';
  $field_group->mode = 'Americas_Professional';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Trunk Categories',
    'weight' => '5',
    'children' => array(
      0 => 'field_content',
      1 => 'field_geography',
      2 => 'field_sectors',
      3 => 'field_topics',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'formatter' => 'open',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
      ),
    ),
  );
  $export['group_trunk_categories|node|regulatory_news|Americas_Professional'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_trunk_categories|node|regulatory_news|anonymous';
  $field_group->group_name = 'group_trunk_categories';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'regulatory_news';
  $field_group->mode = 'anonymous';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Trunk Categories',
    'weight' => '3',
    'children' => array(
      0 => 'field_content',
      1 => 'field_geography',
      2 => 'field_sectors',
      3 => 'field_topics',
    ),
    'format_type' => 'hidden',
    'format_settings' => array(
      'label' => 'Trunk Categories',
      'instance_settings' => array(
        'classes' => '',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_trunk_categories|node|regulatory_news|anonymous'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_trunk_categories|node|regulatory_news|default';
  $field_group->group_name = 'group_trunk_categories';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'regulatory_news';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => '',
    'weight' => '6',
    'children' => array(
      0 => 'field_content',
      1 => 'field_geography',
      2 => 'field_sectors',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => '',
      'instance_settings' => array(
        'classes' => '',
        'description' => '',
        'show_label' => '1',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_trunk_categories|node|regulatory_news|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_trunk_categories|node|regulatory_news|full';
  $field_group->group_name = 'group_trunk_categories';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'regulatory_news';
  $field_group->mode = 'full';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Trunk Categories',
    'weight' => '3',
    'children' => array(
      0 => 'field_content',
      1 => 'field_geography',
      2 => 'field_sectors',
      3 => 'field_topics',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'formatter' => 'open',
      'instance_settings' => array(
        'description' => '',
        'show_label' => 1,
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
        'classes' => '',
      ),
    ),
  );
  $export['group_trunk_categories|node|regulatory_news|full'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_trunk_categories|node|regulatory_news|subscription';
  $field_group->group_name = 'group_trunk_categories';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'regulatory_news';
  $field_group->mode = 'subscription';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => '',
    'weight' => '3',
    'children' => array(
      0 => 'field_content',
      1 => 'field_geography',
      2 => 'field_sectors',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => '',
      'instance_settings' => array(
        'classes' => '',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_trunk_categories|node|regulatory_news|subscription'] = $field_group;

  return $export;
}
