<?php
/**
 * @file
 * gcl_roles.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function gcl_roles_user_default_roles() {
  $roles = array();

  // Exported role: Americas Premium.
  $roles['Americas Premium'] = array(
    'name' => 'Americas Premium',
    'weight' => '6',
  );

  // Exported role: Americas Professional.
  $roles['Americas Professional'] = array(
    'name' => 'Americas Professional',
    'weight' => '8',
  );

  // Exported role: Content Editor.
  $roles['Content Editor'] = array(
    'name' => 'Content Editor',
    'weight' => '4',
  );

  // Exported role: Content Manager.
  $roles['Content Manager'] = array(
    'name' => 'Content Manager',
    'weight' => '5',
  );

  // Exported role: Data.
  $roles['Data'] = array(
    'name' => 'Data',
    'weight' => '9',
  );

  // Exported role: Internal Author.
  $roles['Internal Author'] = array(
    'name' => 'Internal Author',
    'weight' => '3',
  );

  // Exported role: International Premium.
  $roles['International Premium'] = array(
    'name' => 'International Premium',
    'weight' => '7',
  );

  // Exported role: admin.
  $roles['admin'] = array(
    'name' => 'admin',
    'weight' => '10',
  );

  return $roles;
}
