<?php
// $Id$

class managed_newsletter_newsletter extends managed_newsletter_template {
  private $blocks = array();
  private $blocks_templates;
  private $subject;
  private $default_maillists;
  private $from;
  private $lid;
  private $send_email_id;

  private $blocks_content = array();

  public function get_subject() {
    return $this->subject;
  }

  public function set_subject($value) {
    $this->subject = $value;
  }

  public function get_from() {
    return $this->from;
  }

  public function set_from($value) {
    $this->from = $value;
  }

  /**
   * @return array
   */
  public function get_default_maillists() {
    return empty($this->default_maillists) ? array() : unserialize($this->default_maillists);
  }

  public function set_default_maillists($value) {
    $this->default_maillists = serialize(array_filter($value));
  }

  public function get_blocks() {
    return (array) $this->blocks;
  }

  public function set_blocks($value) {
    $this->blocks = array_filter($value);
  }

  public function get_edit_form() {
    $form = parent::get_edit_form();
    $form['subject'] = array(
		'#type' => 'textfield',
		'#title' => t('Subject'),
		'#default_value' => $this->get_subject(),
		'#description' => t('Email subject. Global and user tokens are available')
		);

    $form['from'] = array(
		'#type' => 'textfield',
		'#title' => t('From'),
		'#default_value' => $this->get_from(),
		'#description' => t('From field for email. Global tokens are available')
		);

    $maillist = array();
    foreach (managed_newsletter_maillist::get_all() as $item) {
      $maillist[$item->get_lid()] = $item->get_title() . ' ' . l('Preview', 'admin/managed_newsletters/maillists/preview/' . $item->get_lid(), array('attributes' => array('target' => '_blank')));
    }

    $form['default_maillists'] = array(
		'#type' => 'checkboxes',
		'#title' => t('Default Mail List'),
		'#options' => $maillist,
		'#default_value' => $this->get_default_maillists(),
		'#description' => t('Mail lists for quick send method')
		);

    $blocks = array();
    foreach (managed_newsletter_template::get_all(null, null, null, 1) as $block_template) {
      $blocks[$block_template->get_tid()] = $block_template->get_title();
    }

    $form['blocks'] = array(
		'#type' => 'checkboxes',
		'#title' => t('Enabled blocks'),
		'#options' => $blocks,
		'#default_value' => (count($this->get_blocks()) > 0 ) ? array_combine($this->get_blocks(), $this->get_blocks()) : array(),
		'#description' => t('Select blocks you want to use in this newsletter')
		);

    return $form;
  }

  public function load() {
    parent::load();
    $result = db_query('SELECT * FROM {managed_newsletters_template_blocks} WHERE tid = :tid', array(':tid' => $this->get_tid()));
    $blocks = array();
    while ($row = $result->fetchAssoc()) {
      $blocks[] = $row['bid'];
    }
    $this->set_blocks($blocks);

    $result = db_query('SELECT * FROM {managed_newsletters_block_node_settings} WHERE tid = :tid', array(':tid' => $this->get_tid()));
    while ($row = $result->fetchAssoc()) {
      $this->{$row['name']} = $row['value'];
    }
  }

  public function update($values) {
    parent::update($values);
    $this->set_blocks($values['blocks']);
    $this->set_subject($values['subject']);
    $this->set_from($values['from']);
    $this->set_default_maillists($values['default_maillists']);
  }

  public function save() {
    parent::save();
    // TODO Please review the conversion of this statement to the D7 database API syntax.
    /* db_query('DELETE FROM {managed_newsletters_template_blocks} WHERE tid = %d', $this->get_tid()) */
    db_delete('managed_newsletters_template_blocks')
	  ->condition('tid', $this->get_tid())
	  ->execute();
    foreach ($this->get_blocks() as $block) {
		db_insert('managed_newsletters_template_blocks')
				->fields(array(
							  //'tbid'=>'',
								'tid'=>$this->get_tid(),
								'bid'=> $block)
						)
				->execute();
    }

    db_delete('managed_newsletters_block_node_settings')
	  ->condition('tid', $this->get_tid())
	  ->execute();
    db_insert('managed_newsletters_block_node_settings')
  		->fields(array(
			//'tsid' => '',
			'tid' => $this->get_tid(),
			'name' => 'subject',
			'value' => $this->get_subject()
		))
		->execute();
    db_insert('managed_newsletters_block_node_settings')
  		->fields(array(
			//'tsid' => '',
			'tid' => $this->get_tid(),
			'name' => 'from',
			'value' => $this->get_from()
		))
		->execute();
    db_insert('managed_newsletters_block_node_settings')
  		->fields(array(
			//'tsid' => '',
			'tid' => $this->get_tid(),
			'name' => 'default_maillists',
			'value' => $this->default_maillists
		))
		->execute();
  }

  public function &get_blocks_templates($block_tid = null) {
    if (!isset($this->blocks_templates)) {
      $this->blocks_templates = array();
      foreach ($this->get_blocks() as $tid) {
        $block = managed_newsletter_template::get_all(null, $tid);
        $block->load();
        $this->blocks_templates[$block->get_tid()] = $block;
      }
    }

    $ret = ($block_tid == null ? $this->blocks_templates : $this->blocks_templates[$block_tid]);
	return $ret;
  }

  public function get_tokens() {
    $tokens = array(); //parent::get_tokens();
    $tokens += $this->get_class_tokens();
    return $tokens;
  }

  public function get_class_tokens() {
    $class = get_class($this);
    $tokens[$class] = array();
    foreach (managed_newsletter_template::get_all(null, null, null, 1) as $tid => $block) {
      $tokens[$class]['newsletter_blocks']['managed-newsletter-block-' . $block->get_name()] = $block->get_title();
    }
    return $tokens;
  }

  public function build_html_content($account) {
    $values = array();
    foreach ($this->get_blocks_templates() as $tid => $block) {
      $values['managed-newsletter:block-' . $block->get_name()] = $block->build_html_content($account);
    }
	  //todo need test
	  $objects = array(
		'global' => null,
		'user' => $account,
		'managed-newsletter' => $values,
		);
    $result = token_replace($this->get_html(), $objects);

    $objects = array(
		'global' => null,
		'user' => $account,
		'managed-newsletter' => get_object_vars($this),
		);
    $result = token_replace($result, $objects);
	module_invoke_all('managed_newsletters_body_alt',$result, $this->get_send_email_id());
    return $result;
  }

  public function build_text_content($account) {
    $values = array();
    foreach ($this->get_blocks_templates() as $tid => $block) {
      $values['managed-newsletter:block-' . $block->get_name()] = $block->build_text_content($account);
    }
	  //todo need test
	  $objects = array(
		'global' => null,
		'user' => $account,
		'managed-newsletter' => $values
		);
    $result = token_replace($this->get_text(), $objects, $values);

    $objects = array(
		'global' => null,
		'user' => $account,
		'managed_newsletter' => $this
		);
    $result = token_replace($result, $objects);
	module_invoke_all('managed_newsletters_body_alt',$result, $this->get_send_email_id());
    return $result;
  }

  public function build_subject(&$account) {
    $objects = array(
		'global' => null,
		'user' => $account,
		'managed_newsletter' => $this
		);
    //$result = token_replace_multiple($this->get_subject(), $objects);
    $result = token_replace($this->get_subject(), $objects);
    return $result;
  }

  public function build_from() {
    $objects = array(
		'global' => null,
		);
    //$result = token_replace_multiple($this->get_from(), $objects);
    $result = token_replace($this->get_from(), $objects);
    return $result;
  }

  public function set_send_process_id($value) {
    $this->lid = $value;
  }

  public function get_send_process_id() {
    return $this->lid;
  }

  public function set_send_email_id($value) {
    $this->send_email_id = $value;
  }

  public function get_send_email_id() {
    return $this->send_email_id;
  }
}
