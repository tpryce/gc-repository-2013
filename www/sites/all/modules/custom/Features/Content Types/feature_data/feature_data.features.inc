<?php
/**
 * @file
 * feature_data.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function feature_data_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function feature_data_node_info() {
  $items = array(
    'data' => array(
      'name' => t('Data'),
      'base' => 'node_content',
      'description' => t('Node type for data '),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
