<?php
/**
 * @file
 * feature_hero_slide.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function feature_hero_slide_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function feature_hero_slide_node_info() {
  $items = array(
    'hero_slide' => array(
      'name' => t('Hero Slide'),
      'base' => 'node_content',
      'description' => t('A large promotional slide for various landing pages'),
      'has_title' => '1',
      'title_label' => t('Headline'),
      'help' => '',
    ),
  );
  return $items;
}
