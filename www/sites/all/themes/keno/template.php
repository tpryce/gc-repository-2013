<?php

/**
 * @file
 * This file is empty by default because the base theme chain (Alpha & Omega) provides
 * all the basic functionality. However, in case you wish to customize the output that Drupal
 * generates through Alpha & Omega this file is a good place to do so.
 *
 * Alpha comes with a neat solution for keeping this file as clean as possible while the code
 * for your subtheme grows. Please read the README.txt in the /preprocess and /process subfolders
 * for more information on this topic.
 */

/**
  * Implementation of hook form alter
  */

function keno_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == 'search_block_form') {
    $form['actions']['submit']['#value'] = t('Go');
    $form['search_block_form']['#title'] = '';
    $form['search_block_form']['#size'] = 25;
    $form['search_block_form']['#default_value'] = 'What are you looking for?';
    $form['search_block_form']['#attributes']['onblur'] = "if (this.value == '') {this.value = 'What are you looking for?';}";
    $form['search_block_form']['#attributes']['onfocus'] = "if (this.value == 'What are you looking for?') {this.value = '';}";
  }
  // Customization to user_homepage module, to print the symbolset icons.
  switch ($form_id) {
    case 'user_homepage_button':
      $form['user_homepage_button']['#prefix'] = '<span class="ss-symbol ss-icon">&#x2302;</span>';
      $form['user_homepage_button']['#value'] = t('Save as my homepage');
      break;
    case 'user_homepage_reset_button':
      $form['user_homepage_reset_button']['#prefix'] = '<span class="ss-symbol ss-icon">&#x2421;</span>';
      $form['user_homepage_reset_button']['#value'] = t('Reset my homepage');
      break;
  }
}

/**
 * Implements hook_process_region().
 */
function keno_alpha_process_region(&$vars) {
  if (in_array($vars['elements']['#region'], array('content', 'menu', 'branding'))) {
    $theme = alpha_get_theme();

    switch ($vars['elements']['#region']) {

      case 'branding':
        $datalogo_array = array(
          'path' => 'sites/all/themes/keno/images/data_logo.png',
          'alt' => 'Gambling Data',
          'title' => 'Gambling Data',
          'width' => '199px',
          'height' => '27px',
          'attributes' => array('class' => 'data-img', 'id' => 'data-img'),
        );
        $gcl_domain_data = variable_get('gcl_domain_data', 'http://www.gamblingdata.com');
        $vars['datalogo'] = l(theme('image',$datalogo_array), $gcl_domain_data, array('attributes' => array('title' => 'Gambling Data'), 'html' => TRUE));
        break;
    }
  }
}


function keno_apachesolr_search_mlt_recommendation_block($vars) {
  $docs = $vars['docs'];
  $links = array();
  foreach ($docs as $result) {
    $searchnode = node_load($result->entity_id);
    // Suitable for single-site mode. Label is already safe.
    if($searchnode->created != ''){
      $links[] = l($result->label, $result->path, array('html' => TRUE)) . '<div class="related-content-date">' .t(format_date($searchnode->created,'custom','d M, Y')) . '</div>';
    }
    else{
      $links[] = l($result->label, $result->path, array('html' => TRUE));
    }
  }
  $links = array(
    '#theme' => 'item_list',
    '#items' => $links,
  );
  return render($links);
}

/**
 * Preprocess webforms.
 */
function keno_preprocess_webform_form(&$vars) {
  // Unfortunately, with webform we have to hard-code the nid to do anyhting useful.
  if ($vars['form']['form_id']['#value'] == 'webform_client_form_77') {
    // Turn off Drupal's resizing UI as we're limited by a fixed height.
    $vars['form']['submitted']['message']['#resizable'] = FALSE;
    $vars['form']['actions']['submit']['#suffix'] = '<span class="all-fields-required">' . t('All fields required') . '</span>';
  }
}
/**
 * Preprocess regions.
 */
function keno_preprocess_region(&$vars) {
//  dpm($vars);
}
/**
 * Implements hook_page_alter.
 */
function keno_page_alter(&$vars) {
  if ($footer_first = $vars['footer']['footer']['footer_first']) {
    $footer_first['locations'] = array(
      '#markup' => '<h2 class="locations-title">' . t('Locations') . '</h2>',
      '#weight' => 1.5,
    );
    $footer_first['#sorted'] = FALSE;
    $vars['footer']['footer']['footer_first'] = $footer_first;
  }

  // Search page tweaks
  if (strstr(current_path(), 'search/site')) {
    $searched_value = $vars['content']['content']['content']['system_main']['search_form']['basic']['keys']['#default_value'];

    // If no search string, but facet selected, put it in the "Results for "
    // message.
    if ($searched_value == ' ' && isset($_GET['f'][0])) {
      $selected_facet = $_GET['f'][0];
      $split_facet = preg_split('/:/', $selected_facet, 2);
      $facet_value = $split_facet[1];

      // Facet is a bundle (node type).
      if (strstr($selected_facet, 'bundle')) {
        $searched_value = ucwords(str_replace('_', ' ', $facet_value));
      }
      // Facet is a taxonomy_term.
      else {
        $facet_term = taxonomy_term_load($facet_value);
        $searched_value = $facet_term->name;
      }
    }

    // Set up proper search results message.
    $vars['content']['content']['search_page_title'] = array(
      '#markup'=> '<h1 id="page-title" class="grid-12">Results for <b>"' . $searched_value . '"</b></h1>',
      '#weight' => 0,
    );
    // Design's don't feature any search_form or suggestions.
    unset($vars['content']['content']['content']['system_main']['search_form']);
    unset($vars['content']['content']['content']['system_main']['suggestions']);
  }
}

/**
 * Preprocess pages.
 */
function keno_preprocess_page(&$vars) {
  // Search page tweaks
  if (strstr(current_path(), 'search/site')) {
    // Hide default title. Using custom one above the main sidebar / content
    // regions.
    $vars['title_hidden'] = TRUE;
  }
}

/**
 * Preprocess search results.
 */
function keno_preprocess_search_results(&$vars) {
  $vars['pager'] = theme('pager', array('quantity' => 3));
}

/**
 * Preprocess search result.
 */
function keno_preprocess_search_result(&$vars) {
  $marker = theme('mark', array('type' => node_mark($vars['result']['node']->nid, $vars['result']['node']->changed)));
  // using <span>, as <time> tag is not supported yet.
  $datetime = date("l jS F Y", $vars['result']['node']->created);
  $vars['info'] = '<span class="date">' . $datetime . '</span>' . $marker;
}

/**
 * Implements hook_preprocess_field().
 */
function keno_preprocess_field(&$vars) {
  if ($vars['element']['#view_mode'] == 'teaser' && $vars['element']['#field_name'] == 'body') {
      $summary = strip_tags($vars['items'][0]['#markup']);
      $vars['items'][0]['#markup'] = trim($summary);
  }

  // Need some extra classes in fields that will go on the grey box at the
  // bottom of end nodes.
  $trunk_categories = keno_trunk_category_fields();
  if (in_array($vars['element']['#field_name'], $trunk_categories)) {
    $counter = &drupal_static(__FUNCTION__);
    if (!isset($counter)) {
      $counter = 0;
    }
    $counter++;
    // Row for nodes where trunk categories are hold within a grid-8 body
    // which results in 4 columns per row.
    $quad_row_class = is_integer($counter / 4) ? ($counter / 4) : floor($counter / 4) + 1;

    $vars['classes_array'][] = 'trunk-category';
    // Need an integer for each field within the box, so we can put a clear:both
    // in css, and fields go properly to the left on the next row.
    $vars['classes_array'][] = 'trunk-category-' . $counter;
    $vars['classes_array'][] = 'trunk-category-quad-row-' . $quad_row_class;

    // In the case of taxonomy term references, if they are plain text, add a
    // span so that the styling applied to links can be applied also.
    if ('taxonomy_term_reference' === $vars['element']['#field_type']) {
      $current_active_domain = domain_get_domain();
      if (!$current_active_domain['is_default']) {
        foreach ($vars['items'] as $index => $item) {
          if (isset($item['#markup']) && !isset($item['#href'])) {
            $vars['items'][$index]['#markup'] = '<span>' . $item['#markup'] . '</span>';
          }
        }
      }
    }
  }
}

/**
 * Hard-coded names of the fields that will appear in a grey box at the bottom
 * of end nodes.
 *
 * @return array
 */
function keno_trunk_category_fields() {
  return array(
    'field_content',
    'field_geography',
    'field_sectors',
    'field_subscription_role',
    'field_subscription_level',
    'field_tags',
    'field_topics',
    'field_data_sources',
    'field_spotlight',
  );
}

/**
 * Override the link to the login page when user is viewing a node. Sets the
 * &destination param in the URL, so that user_homepage module doesn't redirect
 * users to their homepage if they came from a content node, and redirects them
 * to that node instead.
 *
 * Implements hook_block_view_alter().
 */
function keno_block_view_alter(&$data, $block) {
  // The alter done here is only needed on node pages.
  if (!node_is_page(menu_get_object())) {
    return;
  }
  // Only act for user-menu block links.
  if ($block->delta == 'user-menu') {
    foreach (element_children($data['content']) as $key) {
      // Get the link we're looking for.
      if ($data['content'][$key]['#href'] == 'user/login') {
        // Store original path.
        $href = $data['content'][$key]['#href'];
        // Unset everything, as we'll use custom link, without the default theme
        // function.
        unset($data['content'][$key]);
        // Set destination query options.
        $options = array(
          'query' => array(
            'destination' => current_path(),
          ),
        );
        // Display link
        $data['content'][$key]['#markup'] = '<li>' . l(t('Sign in'), $href, $options) . '</li>';
      }
    }
  }
}

function keno_views_pre_render(&$view) {
  if(($view->name == 'reports')) {
    switch ($view->current_display) {
      case "page_2":
          $view->display_handler->set_option('link_display', 'custom_url');
          // url format search/site/%20?f[0]=bundle:regulatory_news
          $view->display_handler->set_option('link_url', url($base_url . 'search/site', array('absolute' => TRUE, 'query' => array('f[0]' => 'bundle:regulatory_news'))));
          break;
      case "page_3":
          $view->display_handler->set_option('link_display', 'custom_url');
          $view->display_handler->set_option('link_url', url($base_url . 'search/site', array('absolute' => TRUE, 'query' => array('f[0]' => 'bundle:market_analysis'))));
          break;
      case "page_4":
          $view->display_handler->set_option('link_display', 'custom_url');
          $view->display_handler->set_option('link_url', url($base_url . 'search/site', array('absolute' => TRUE, 'query' => array('f[0]' => 'bundle:regulatory_reports'))));
          break;
    }
  }
}

function keno_preprocess_menu_link(&$variables) {
  if ($variables['element']['#title'] == 'Home') {
    global $user;
    global $base_url;
    if ($home = user_homepage_get_homepage($user->uid)) {
      $parsed_homepage = parse_url($home);
      $options = array();
      if (isset($parsed_homepage['query'])) {
        $options = array(
          'query' => drupal_get_query_array($parsed_homepage['query']),
        );
      }
      $url = url($parsed_homepage['path'], $options);
      $variables['element']['#href'] =  $base_url . $url;
    }
  }
}
