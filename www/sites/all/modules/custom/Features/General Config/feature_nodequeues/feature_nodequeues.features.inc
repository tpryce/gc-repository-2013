<?php
/**
 * @file
 * feature_nodequeues.features.inc
 */

/**
 * Implementation of hook_default_fe_nodequeue_queue().
 */
function feature_nodequeues_default_fe_nodequeue_queue() {
  $export = array();

  $fe_nodequeue_queue = new stdClass;
  $fe_nodequeue_queue->name = 'frontpage_market';
  $fe_nodequeue_queue->title = 'Frontpage Market';
  $fe_nodequeue_queue->subqueue_title = '';
  $fe_nodequeue_queue->size = '7';
  $fe_nodequeue_queue->link = 'Add to Frontpage Market';
  $fe_nodequeue_queue->link_remove = 'Remove from Frontpage Market';
  $fe_nodequeue_queue->owner = 'nodequeue';
  $fe_nodequeue_queue->show_in_ui = '1';
  $fe_nodequeue_queue->show_in_tab = '1';
  $fe_nodequeue_queue->show_in_links = '1';
  $fe_nodequeue_queue->reference = '0';
  $fe_nodequeue_queue->reverse = '1';
  $fe_nodequeue_queue->i18n = '1';
  $fe_nodequeue_queue->types = array(
    0 => 'market_analysis',
  );
  $fe_nodequeue_queue->roles = array();
  $fe_nodequeue_queue->count = 0;
  $fe_nodequeue_queue->machine_name = 'frontpage_market_queue';

  $export['frontpage_market_queue'] = $fe_nodequeue_queue;

  $fe_nodequeue_queue = new stdClass;
  $fe_nodequeue_queue->name = 'frontpage_regulatory';
  $fe_nodequeue_queue->title = 'Frontpage Regulatory';
  $fe_nodequeue_queue->subqueue_title = '';
  $fe_nodequeue_queue->size = '7';
  $fe_nodequeue_queue->link = 'Add to Frontpage Reg';
  $fe_nodequeue_queue->link_remove = 'Remove from Frontpage Reg';
  $fe_nodequeue_queue->owner = 'nodequeue';
  $fe_nodequeue_queue->show_in_ui = '1';
  $fe_nodequeue_queue->show_in_tab = '1';
  $fe_nodequeue_queue->show_in_links = '1';
  $fe_nodequeue_queue->reference = '0';
  $fe_nodequeue_queue->reverse = '1';
  $fe_nodequeue_queue->i18n = '1';
  $fe_nodequeue_queue->types = array(
    0 => 'regulatory_news',
  );
  $fe_nodequeue_queue->roles = array();
  $fe_nodequeue_queue->count = 0;
  $fe_nodequeue_queue->machine_name = 'frontpage_regulatory_queue';

  $export['frontpage_regulatory_queue'] = $fe_nodequeue_queue;

  $fe_nodequeue_queue = new stdClass;
  $fe_nodequeue_queue->name = 'regulatory_reports';
  $fe_nodequeue_queue->title = 'Regulatory Reports';
  $fe_nodequeue_queue->subqueue_title = '';
  $fe_nodequeue_queue->size = '7';
  $fe_nodequeue_queue->link = 'Add to Frontpage Reg reports';
  $fe_nodequeue_queue->link_remove = 'Remove from Frontpage Reg reports';
  $fe_nodequeue_queue->owner = 'nodequeue';
  $fe_nodequeue_queue->show_in_ui = '1';
  $fe_nodequeue_queue->show_in_tab = '1';
  $fe_nodequeue_queue->show_in_links = '1';
  $fe_nodequeue_queue->reference = '0';
  $fe_nodequeue_queue->reverse = '1';
  $fe_nodequeue_queue->i18n = '1';
  $fe_nodequeue_queue->types = array(
    0 => 'regulatory_reports',
  );
  $fe_nodequeue_queue->roles = array();
  $fe_nodequeue_queue->count = 0;
  $fe_nodequeue_queue->machine_name = 'regulatory_reports_queue';

  $export['regulatory_reports_queue'] = $fe_nodequeue_queue;

  $fe_nodequeue_queue = new stdClass;
  $fe_nodequeue_queue->name = 'related_newsletter_content';
  $fe_nodequeue_queue->title = 'Related Newsletter Content';
  $fe_nodequeue_queue->subqueue_title = '';
  $fe_nodequeue_queue->size = '5';
  $fe_nodequeue_queue->link = 'Add to Related Newsletter Content';
  $fe_nodequeue_queue->link_remove = 'Remove from Related Newsletter Content';
  $fe_nodequeue_queue->owner = 'nodequeue';
  $fe_nodequeue_queue->show_in_ui = '1';
  $fe_nodequeue_queue->show_in_tab = '1';
  $fe_nodequeue_queue->show_in_links = '1';
  $fe_nodequeue_queue->reference = '0';
  $fe_nodequeue_queue->reverse = '0';
  $fe_nodequeue_queue->i18n = '1';
  $fe_nodequeue_queue->types = array(
    0 => 'market_analysis',
    1 => 'press_release',
    2 => 'regulatory_news',
    3 => 'regulatory_reports',
  );
  $fe_nodequeue_queue->roles = array(
    0 => '15',
    1 => '4',
  );
  $fe_nodequeue_queue->count = 0;
  $fe_nodequeue_queue->machine_name = 'related_newsletter_queue';

  $export['related_newsletter_queue'] = $fe_nodequeue_queue;

  $fe_nodequeue_queue = new stdClass;
  $fe_nodequeue_queue->name = 'us_newsletter_reg_reports';
  $fe_nodequeue_queue->title = 'US Newsletter Reg Reports';
  $fe_nodequeue_queue->subqueue_title = '';
  $fe_nodequeue_queue->size = '6';
  $fe_nodequeue_queue->link = 'Add to US Newsletter Reg Reports';
  $fe_nodequeue_queue->link_remove = 'Remove from US Newsletter Reg Reports';
  $fe_nodequeue_queue->owner = 'nodequeue';
  $fe_nodequeue_queue->show_in_ui = '1';
  $fe_nodequeue_queue->show_in_tab = '1';
  $fe_nodequeue_queue->show_in_links = '1';
  $fe_nodequeue_queue->reference = '0';
  $fe_nodequeue_queue->reverse = '0';
  $fe_nodequeue_queue->i18n = '1';
  $fe_nodequeue_queue->types = array(
    0 => 'regulatory_reports',
  );
  $fe_nodequeue_queue->roles = array(
    0 => '15',
    1 => '4',
  );
  $fe_nodequeue_queue->count = 0;
  $fe_nodequeue_queue->machine_name = 'us_regulatory_reports_queue';

  $export['us_regulatory_reports_queue'] = $fe_nodequeue_queue;

  $fe_nodequeue_queue = new stdClass;
  $fe_nodequeue_queue->name = 'us_newsletter_top_read';
  $fe_nodequeue_queue->title = 'US Newsletter Top Read';
  $fe_nodequeue_queue->subqueue_title = '';
  $fe_nodequeue_queue->size = '5';
  $fe_nodequeue_queue->link = 'Add to US Newsletter Top Read';
  $fe_nodequeue_queue->link_remove = 'Remove from US Newsletter Top Read';
  $fe_nodequeue_queue->owner = 'nodequeue';
  $fe_nodequeue_queue->show_in_ui = '1';
  $fe_nodequeue_queue->show_in_tab = '1';
  $fe_nodequeue_queue->show_in_links = '1';
  $fe_nodequeue_queue->reference = '0';
  $fe_nodequeue_queue->reverse = '0';
  $fe_nodequeue_queue->i18n = '1';
  $fe_nodequeue_queue->types = array(
    0 => 'market_analysis',
    1 => 'regulatory_news',
    2 => 'regulatory_reports',
  );
  $fe_nodequeue_queue->roles = array(
    0 => '15',
    1 => '4',
  );
  $fe_nodequeue_queue->count = 0;
  $fe_nodequeue_queue->machine_name = 'us_top_read_queue';

  $export['us_top_read_queue'] = $fe_nodequeue_queue;

  return $export;
}
