<?php
/**
 * @file
 * feature_press_release.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function feature_press_release_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_anonymous_press_release';
  $strongarm->value = 0;
  $export['comment_anonymous_press_release'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_mode_press_release';
  $strongarm->value = 1;
  $export['comment_default_mode_press_release'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_per_page_press_release';
  $strongarm->value = '50';
  $export['comment_default_per_page_press_release'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_form_location_press_release';
  $strongarm->value = 1;
  $export['comment_form_location_press_release'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_press_release';
  $strongarm->value = '2';
  $export['comment_press_release'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_preview_press_release';
  $strongarm->value = '1';
  $export['comment_preview_press_release'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_subject_field_press_release';
  $strongarm->value = 1;
  $export['comment_subject_field_press_release'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'field_bundle_settings_node__press_release';
  $strongarm->value = array(
    'view_modes' => array(
      'teaser' => array(
        'custom_settings' => TRUE,
      ),
      'search_result' => array(
        'custom_settings' => TRUE,
      ),
      'full' => array(
        'custom_settings' => FALSE,
      ),
      'rss' => array(
        'custom_settings' => FALSE,
      ),
      'search_index' => array(
        'custom_settings' => FALSE,
      ),
      'anonymous user' => array(
        'custom_settings' => FALSE,
      ),
      'authenticated user' => array(
        'custom_settings' => FALSE,
      ),
      'Lite Subscription' => array(
        'custom_settings' => FALSE,
      ),
      'Full Subscription' => array(
        'custom_settings' => FALSE,
      ),
      'Internal Author' => array(
        'custom_settings' => FALSE,
      ),
      'Content Editor' => array(
        'custom_settings' => FALSE,
      ),
      'Content Manager' => array(
        'custom_settings' => FALSE,
      ),
      'admin' => array(
        'custom_settings' => FALSE,
      ),
      'token' => array(
        'custom_settings' => FALSE,
      ),
      'subscription' => array(
        'custom_settings' => TRUE,
      ),
      'anonymous' => array(
        'custom_settings' => TRUE,
      ),
      'Americas_Premium' => array(
        'custom_settings' => FALSE,
      ),
      'International_Premium' => array(
        'custom_settings' => FALSE,
      ),
      'Americas_Professional' => array(
        'custom_settings' => FALSE,
      ),
      'International_Professional' => array(
        'custom_settings' => FALSE,
      ),
      'Gambling_Data' => array(
        'custom_settings' => FALSE,
      ),
      'print' => array(
        'custom_settings' => FALSE,
      ),
    ),
    'extra_fields' => array(
      'form' => array(),
      'display' => array(
        'subscriptions_ui' => array(
          'default' => array(
            'weight' => '100',
            'visible' => TRUE,
          ),
          'search_result' => array(
            'weight' => '100',
            'visible' => TRUE,
          ),
        ),
        'domain' => array(
          'default' => array(
            'weight' => '1',
            'visible' => TRUE,
          ),
        ),
        'freetrialform' => array(
          'default' => array(
            'weight' => '5',
            'visible' => TRUE,
          ),
        ),
      ),
    ),
  );
  $export['field_bundle_settings_node__press_release'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_press_release';
  $strongarm->value = array(
    0 => 'promote',
    1 => 'moderation',
    2 => 'revision',
  );
  $export['node_options_press_release'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_preview_press_release';
  $strongarm->value = '1';
  $export['node_preview_press_release'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_submitted_press_release';
  $strongarm->value = 1;
  $export['node_submitted_press_release'] = $strongarm;

  return $export;
}
