<?php
/**
 * @file
 * user_homepage .install
 * Install and Uninstall hooks for user_homepage module.
 */


/**
 * Implements hook_install().
 */
function user_homepage_install() {
  //
}

/**
 * Implements hook_schema().
 */
function user_homepage_schema() {
  $schema['user_homepage'] = array(
    'description' => "Storage of user's custom homepages",
    'fields' => array(
      'uid' => array(
        'description' => 'The User ID for which a custom homepage is stored.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'path' => array(
        'description' => 'The internal stored path.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
      ),
      'domain_id' => array(
        'description' => 'Domain to store the homepage against',
        'type' => 'varchar',
        'length' => 20,
        'not null' => FALSE,
      ),
      'uid_domain_id' => array(
        'description' => 'Domain to store the homepage against',
        'type' => 'varchar',
        'length' => 20,
        'not null' => FALSE,
      ),
    ),
    'primary key' => array('uid_domain_id'),
  );

  return $schema;
}

/**
 * Implements hook_uninstall().
 */
function user_homepage_uninstall() {
  //
}

/**
 * Add new column domain_id to table.
 */
function user_homepage_update_7100() {
  $spec = array(
    'type' => 'varchar',
    'description' => "Domain to store the homepage against",
    'length' => 20,
    'not null' => FALSE,
  ); 
  db_add_field('user_homepage', 'domain_id', $spec);
}

/**
 * Add new column uid_domain_id
 */
function user_homepage_update_7101() {
  $spec = array(
    'type' => 'varchar',
    'description' => "Domain to store the homepage against",
    'length' => 20,
    'not null' => FALSE,
  ); 
  db_add_field('user_homepage', 'uid_domain_id', $spec);
}

/**
 * Add new primary key uid_domain_id
 */
function user_homepage_update_7102() {
  // This update previously failed because of a NULL value in uid_domain_id
  // column, remove before db_change_field().
  db_delete('user_homepage')
    ->condition('uid_domain_id', NULL)
    ->execute();

  // Now perform the alter.
  db_drop_primary_key('user_homepage');
  db_change_field('user_homepage', 'uid_domain_id', 'uid_domain_id',
  array('type' => 'int', 'not null' => TRUE),
  array('primary key' => array('uid_domain_id')));
}
