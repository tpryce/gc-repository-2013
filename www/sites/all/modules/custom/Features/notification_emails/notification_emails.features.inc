<?php

function notification_emails_features_export_options()
{
	$result = db_select('notification_emails', 'ne')->fields('ne', array('delta'))->execute();
	$options = array();
	foreach ($result as $row)
	{
		$options[$row->delta] = $row->delta;
	}
	return $options;
}

function notification_emails_features_export($data, &$export, $module_name)
{
	$export['dependencies']['notification_emails'] = 'notification_emails';
	foreach ($data as $component)
	{
		$export['features']['notification_emails'][$component] = $component;
	}
	return array();
}

function notification_emails_features_export_render($module_name, $data, $export = NULL)
{
	$code = array();
	$code[] = '  $notification_emails = array();';
	$code[] = '';
	foreach ($data as $sys_name)
	{
		$item = _notification_emails_get_data($sys_name);
		$code[] = '  $notification_emails[] = '. features_var_export($item, '  ') .';';
	}
	$code[] = '  return $notification_emails;';
	$code = implode("\n", $code);
	return array('notification_emails_features_default_settings' => $code);
}

function notification_emails_features_rebuild($module)
{
	$items = module_invoke($module, 'notification_emails_features_default_settings');
	foreach ($items as $item)
	{
		$saved = _notification_emails_set_data($item);
	}
}

function notification_emails_features_revert($module)
{
	notification_emails_features_rebuild($module);
}

function _notification_emails_get_data($delta)
{
	$data = array();
	$result = db_select('notification_emails', 'ne')
			->fields('ne', array('delta', 'source', 'email', 'from_email', 'subject', 'admin_message', 'restrict_dynamic'))
			->condition('delta', $delta)
			->execute();
	foreach ($result as $row)
	{
		$data['delta'] = $row->delta;
		$data['source'] = $row->source;
		$data['email'] = $row->email;
		$data['from_email'] = $row->from_email;
		$data['subject'] = $row->subject;
		$data['admin_message'] = $row->admin_message;
		$data['restrict_dynamic'] = $row->restrict_dynamic;
	}
	return $data;
}

function _notification_emails_set_data($items)
{

	db_delete('notification_emails')
		->condition('delta', $items['delta'])
		->execute();

	db_insert('notification_emails')
		->fields(array('delta'=>$items['delta'],
						'source'=>$items['source'],
						'email'=>$items['email'],
						'from_email'=>$items['from_email'],
						'subject'=>$items['subject'],
						'admin_message'=>$items['admin_message'],
						'restrict_dynamic'=>$items['restrict_dynamic'],
					))
	->execute();
	return true;
}
