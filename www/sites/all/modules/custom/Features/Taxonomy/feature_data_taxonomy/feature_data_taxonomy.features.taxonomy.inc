<?php
/**
 * @file
 * feature_data_taxonomy.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function feature_data_taxonomy_taxonomy_default_vocabularies() {
  return array(
    'company_data' => array(
      'name' => 'Company Data',
      'machine_name' => 'company_data',
      'description' => '',
      'hierarchy' => '1',
      'module' => 'taxonomy',
      'weight' => '9',
    ),
    'data_sources' => array(
      'name' => 'Data Sources',
      'machine_name' => 'data_sources',
      'description' => 'Different source types for data entities',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '-8',
    ),
    'topics_data' => array(
      'name' => 'Topics',
      'machine_name' => 'topics_data',
      'description' => 'Topic terms for Data content',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '10',
    ),
  );
}
