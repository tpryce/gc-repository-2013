<?php
/**
 * @file
 * feature_market_analysis.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function feature_market_analysis_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_anonymous_market_analysis';
  $strongarm->value = 0;
  $export['comment_anonymous_market_analysis'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_mode_market_analysis';
  $strongarm->value = 1;
  $export['comment_default_mode_market_analysis'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_per_page_market_analysis';
  $strongarm->value = '50';
  $export['comment_default_per_page_market_analysis'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_form_location_market_analysis';
  $strongarm->value = 1;
  $export['comment_form_location_market_analysis'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_market_analysis';
  $strongarm->value = '2';
  $export['comment_market_analysis'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_preview_market_analysis';
  $strongarm->value = '1';
  $export['comment_preview_market_analysis'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_subject_field_market_analysis';
  $strongarm->value = 1;
  $export['comment_subject_field_market_analysis'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'field_bundle_settings_node__market_analysis';
  $strongarm->value = array(
    'view_modes' => array(
      'teaser' => array(
        'custom_settings' => TRUE,
      ),
      'subscription' => array(
        'custom_settings' => TRUE,
      ),
      'full' => array(
        'custom_settings' => FALSE,
      ),
      'rss' => array(
        'custom_settings' => FALSE,
      ),
      'search_index' => array(
        'custom_settings' => FALSE,
      ),
      'search_result' => array(
        'custom_settings' => TRUE,
      ),
      'token' => array(
        'custom_settings' => FALSE,
      ),
      'anonymous user' => array(
        'custom_settings' => FALSE,
      ),
      'authenticated user' => array(
        'custom_settings' => FALSE,
      ),
      'Lite Subscription' => array(
        'custom_settings' => FALSE,
      ),
      'Full Subscription' => array(
        'custom_settings' => FALSE,
      ),
      'Internal Author' => array(
        'custom_settings' => FALSE,
      ),
      'Content Editor' => array(
        'custom_settings' => FALSE,
      ),
      'Content Manager' => array(
        'custom_settings' => FALSE,
      ),
      'admin' => array(
        'custom_settings' => FALSE,
      ),
      'print' => array(
        'custom_settings' => FALSE,
      ),
      'anonymous' => array(
        'custom_settings' => TRUE,
      ),
      'Americas_Premium' => array(
        'custom_settings' => FALSE,
      ),
      'International_Premium' => array(
        'custom_settings' => FALSE,
      ),
      'Americas_Professional' => array(
        'custom_settings' => FALSE,
      ),
      'International_Professional' => array(
        'custom_settings' => FALSE,
      ),
      'Gambling_Data' => array(
        'custom_settings' => FALSE,
      ),
    ),
    'extra_fields' => array(
      'form' => array(),
      'display' => array(
        'subscriptions_ui' => array(
          'default' => array(
            'weight' => '9',
            'visible' => TRUE,
          ),
          'teaser' => array(
            'weight' => '100',
            'visible' => TRUE,
          ),
          'search_result' => array(
            'weight' => '100',
            'visible' => TRUE,
          ),
          'subscription' => array(
            'weight' => '6',
            'visible' => TRUE,
          ),
          'anonymous' => array(
            'weight' => '4',
            'visible' => FALSE,
          ),
        ),
        'flippy_pager' => array(
          'teaser' => array(
            'weight' => '5',
            'visible' => TRUE,
          ),
          'default' => array(
            'weight' => '5',
            'visible' => TRUE,
          ),
          'search_result' => array(
            'weight' => '5',
            'visible' => TRUE,
          ),
          'subscription' => array(
            'weight' => '2',
            'visible' => TRUE,
          ),
          'anonymous' => array(
            'weight' => '1',
            'visible' => TRUE,
          ),
        ),
        'domain' => array(
          'default' => array(
            'weight' => '3',
            'visible' => TRUE,
          ),
          'subscription' => array(
            'weight' => '7',
            'visible' => FALSE,
          ),
          'anonymous' => array(
            'weight' => '12',
            'visible' => FALSE,
          ),
        ),
        'freetrialform' => array(
          'default' => array(
            'weight' => '10',
            'visible' => FALSE,
          ),
          'subscription' => array(
            'weight' => '8',
            'visible' => FALSE,
          ),
          'anonymous' => array(
            'weight' => '2',
            'visible' => TRUE,
          ),
        ),
      ),
    ),
  );
  $export['field_bundle_settings_node__market_analysis'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_options_market_analysis';
  $strongarm->value = array(
    0 => 'main-menu',
  );
  $export['menu_options_market_analysis'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_parent_market_analysis';
  $strongarm->value = 'main-menu:0';
  $export['menu_parent_market_analysis'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_market_analysis';
  $strongarm->value = array(
    0 => 'moderation',
    1 => 'revision',
  );
  $export['node_options_market_analysis'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_preview_market_analysis';
  $strongarm->value = '1';
  $export['node_preview_market_analysis'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_submitted_market_analysis';
  $strongarm->value = 1;
  $export['node_submitted_market_analysis'] = $strongarm;

  return $export;
}
