// $Id$

function solr_search_crop_title() {
    (function ($) {
        $('.panel-solr-searsh-saved-result-header').each(function() {
            // Save text of title
            var text = $('h2', this).attr('title');
            // Write to the title 1 sumbol and save height of this row
            $('h2', this).text('1');
            // This is height of 1 row of title text
            var goal_height = $('h2', this).height();
            // Restore original title
            $('h2', this).text(text);

            // While height of title more when 1 row height - make croping of title
            while (goal_height < $('h2', this).height()) {
                // Delete from title 1 last letter
                text = text.substring(0, text.length - 1);
                // Save croped title and add '...', to show user what this title was croped
                $('h2', this).text(text + '...');
            }
        });
    })(jQuery);
}

(function ($) {

    $(document).ready(function() {
        setTimeout(function() {
            (function ($) {
                // Save text into title attribute, there user can read full title on mouseover if we crop it
                $('.panel-solr-searsh-saved-result-header h2').each(function() {
                    var text = $(this).text();
                    $(this).attr('title', text);
                });

                solr_search_crop_title();
            })(jQuery);
        },
                520
                );
    }
            );

    $(window).resize(function() {
        // Use timeout 520, because other script also runed with timeout (equal to 500)
        setTimeout('solr_search_crop_title();', 520);
    }
            );


})(jQuery);