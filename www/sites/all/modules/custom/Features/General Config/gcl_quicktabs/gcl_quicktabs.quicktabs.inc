<?php
/**
 * @file
 * gcl_quicktabs.quicktabs.inc
 */

/**
 * Implements hook_quicktabs_default_quicktabs().
 */
function gcl_quicktabs_quicktabs_default_quicktabs() {
  $export = array();

  $quicktabs = new stdClass();
  $quicktabs->disabled = FALSE; /* Edit this to true to make a default quicktabs disabled initially */
  $quicktabs->api_version = 1;
  $quicktabs->machine_name = 'homepage';
  $quicktabs->ajax = 0;
  $quicktabs->hide_empty_tabs = FALSE;
  $quicktabs->default_tab = 0;
  $quicktabs->title = 'Homepage';
  $quicktabs->tabs = array(
    0 => array(
      'vid' => 'reports',
      'display' => 'page_2',
      'args' => '',
      'title' => 'Regulatory News',
      'weight' => '-100',
      'type' => 'view',
    ),
    1 => array(
      'vid' => 'reports',
      'display' => 'page_3',
      'args' => '',
      'title' => 'Market Analysis',
      'weight' => '-99',
      'type' => 'view',
    ),
    2 => array(
      'vid' => 'reports',
      'display' => 'page_4',
      'args' => '',
      'title' => 'Regulatory Reports',
      'weight' => '-98',
      'type' => 'view',
    ),
  );
  $quicktabs->renderer = 'quicktabs';
  $quicktabs->style = 'nostyle';
  $quicktabs->options = array();

  // Translatables
  // Included for use with string extractors like potx.
  t('Homepage');
  t('Market Analysis');
  t('Regulatory News');
  t('Regulatory Reports');

  $export['homepage'] = $quicktabs;

  return $export;
}
