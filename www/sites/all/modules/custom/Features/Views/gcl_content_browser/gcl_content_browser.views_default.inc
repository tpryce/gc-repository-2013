<?php
/**
 * @file
 * gcl_content_browser.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function gcl_content_browser_views_default_views() {
  $export = array();

  $view = new view;
  $view->name = 'content_browser';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Content Browser';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Content Browser';
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '20';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'node';
  $handler->display->display_options['row_options']['links'] = FALSE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['title']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['title']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = 0;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = 0;
  $handler->display->display_options['fields']['title']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['title']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['title']['alter']['html'] = 0;
  $handler->display->display_options['fields']['title']['hide_empty'] = 0;
  $handler->display->display_options['fields']['title']['empty_zero'] = 0;
  $handler->display->display_options['fields']['title']['link_to_node'] = 1;
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Contextual filter: Content: Created year */
  $handler->display->display_options['arguments']['created_year']['id'] = 'created_year';
  $handler->display->display_options['arguments']['created_year']['table'] = 'node';
  $handler->display->display_options['arguments']['created_year']['field'] = 'created_year';
  $handler->display->display_options['arguments']['created_year']['title_enable'] = 1;
  $handler->display->display_options['arguments']['created_year']['title'] = 'Regulatory News %1';
  $handler->display->display_options['arguments']['created_year']['breadcrumb_enable'] = 1;
  $handler->display->display_options['arguments']['created_year']['breadcrumb'] = '%1';
  $handler->display->display_options['arguments']['created_year']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['created_year']['default_argument_skip_url'] = 0;
  $handler->display->display_options['arguments']['created_year']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['created_year']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['created_year']['summary_options']['items_per_page'] = '25';
  /* Contextual filter: Content: Created month */
  $handler->display->display_options['arguments']['created_month']['id'] = 'created_month';
  $handler->display->display_options['arguments']['created_month']['table'] = 'node';
  $handler->display->display_options['arguments']['created_month']['field'] = 'created_month';
  $handler->display->display_options['arguments']['created_month']['title_enable'] = 1;
  $handler->display->display_options['arguments']['created_month']['title'] = 'Regulatory News %1 %2';
  $handler->display->display_options['arguments']['created_month']['breadcrumb_enable'] = 1;
  $handler->display->display_options['arguments']['created_month']['breadcrumb'] = '%2';
  $handler->display->display_options['arguments']['created_month']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['created_month']['default_argument_skip_url'] = 0;
  $handler->display->display_options['arguments']['created_month']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['created_month']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['created_month']['summary_options']['items_per_page'] = '25';
  /* Contextual filter: Content: Created day */
  $handler->display->display_options['arguments']['created_day']['id'] = 'created_day';
  $handler->display->display_options['arguments']['created_day']['table'] = 'node';
  $handler->display->display_options['arguments']['created_day']['field'] = 'created_day';
  $handler->display->display_options['arguments']['created_day']['title_enable'] = 1;
  $handler->display->display_options['arguments']['created_day']['title'] = 'Regulatory News for %1 %2 %3';
  $handler->display->display_options['arguments']['created_day']['breadcrumb_enable'] = 1;
  $handler->display->display_options['arguments']['created_day']['breadcrumb'] = '%3';
  $handler->display->display_options['arguments']['created_day']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['created_day']['default_argument_skip_url'] = 0;
  $handler->display->display_options['arguments']['created_day']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['created_day']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['created_day']['summary_options']['items_per_page'] = '25';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;

  /* Display: Regulatory News */
  $handler = $view->new_display('page', 'Regulatory News', 'page');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Regulatory News';
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  /* Contextual filter: Content: Created year */
  $handler->display->display_options['arguments']['created_year']['id'] = 'created_year';
  $handler->display->display_options['arguments']['created_year']['table'] = 'node';
  $handler->display->display_options['arguments']['created_year']['field'] = 'created_year';
  $handler->display->display_options['arguments']['created_year']['title_enable'] = 1;
  $handler->display->display_options['arguments']['created_year']['title'] = 'Regulatory News for %1';
  $handler->display->display_options['arguments']['created_year']['breadcrumb_enable'] = 1;
  $handler->display->display_options['arguments']['created_year']['breadcrumb'] = 'Regulatory News';
  $handler->display->display_options['arguments']['created_year']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['created_year']['default_argument_skip_url'] = 0;
  $handler->display->display_options['arguments']['created_year']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['created_year']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['created_year']['summary_options']['items_per_page'] = '25';
  /* Contextual filter: Content: Created month */
  $handler->display->display_options['arguments']['created_month']['id'] = 'created_month';
  $handler->display->display_options['arguments']['created_month']['table'] = 'node';
  $handler->display->display_options['arguments']['created_month']['field'] = 'created_month';
  $handler->display->display_options['arguments']['created_month']['title_enable'] = 1;
  $handler->display->display_options['arguments']['created_month']['title'] = 'Regulatory News for %2 %1';
  $handler->display->display_options['arguments']['created_month']['breadcrumb_enable'] = 1;
  $handler->display->display_options['arguments']['created_month']['breadcrumb'] = '%1';
  $handler->display->display_options['arguments']['created_month']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['created_month']['default_argument_skip_url'] = 0;
  $handler->display->display_options['arguments']['created_month']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['created_month']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['created_month']['summary_options']['items_per_page'] = '25';
  /* Contextual filter: Content: Created day */
  $handler->display->display_options['arguments']['created_day']['id'] = 'created_day';
  $handler->display->display_options['arguments']['created_day']['table'] = 'node';
  $handler->display->display_options['arguments']['created_day']['field'] = 'created_day';
  $handler->display->display_options['arguments']['created_day']['title_enable'] = 1;
  $handler->display->display_options['arguments']['created_day']['title'] = 'Regulatory News for %3 %2 %1';
  $handler->display->display_options['arguments']['created_day']['breadcrumb_enable'] = 1;
  $handler->display->display_options['arguments']['created_day']['breadcrumb'] = '%2';
  $handler->display->display_options['arguments']['created_day']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['created_day']['default_argument_skip_url'] = 0;
  $handler->display->display_options['arguments']['created_day']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['created_day']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['created_day']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'regulatory_news' => 'regulatory_news',
  );
  $handler->display->display_options['path'] = 'regulatory-news';

  /* Display: Regulatory Reports */
  $handler = $view->new_display('page', 'Regulatory Reports', 'page_1');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Regulatory Reports';
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  /* Contextual filter: Content: Created year */
  $handler->display->display_options['arguments']['created_year']['id'] = 'created_year';
  $handler->display->display_options['arguments']['created_year']['table'] = 'node';
  $handler->display->display_options['arguments']['created_year']['field'] = 'created_year';
  $handler->display->display_options['arguments']['created_year']['title_enable'] = 1;
  $handler->display->display_options['arguments']['created_year']['title'] = 'Regulatory News %1';
  $handler->display->display_options['arguments']['created_year']['breadcrumb_enable'] = 1;
  $handler->display->display_options['arguments']['created_year']['breadcrumb'] = 'Regulatory News';
  $handler->display->display_options['arguments']['created_year']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['created_year']['default_argument_skip_url'] = 0;
  $handler->display->display_options['arguments']['created_year']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['created_year']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['created_year']['summary_options']['items_per_page'] = '25';
  /* Contextual filter: Content: Created month */
  $handler->display->display_options['arguments']['created_month']['id'] = 'created_month';
  $handler->display->display_options['arguments']['created_month']['table'] = 'node';
  $handler->display->display_options['arguments']['created_month']['field'] = 'created_month';
  $handler->display->display_options['arguments']['created_month']['title_enable'] = 1;
  $handler->display->display_options['arguments']['created_month']['title'] = 'Regulatory News %1 %2';
  $handler->display->display_options['arguments']['created_month']['breadcrumb_enable'] = 1;
  $handler->display->display_options['arguments']['created_month']['breadcrumb'] = 'Regulatory News %1';
  $handler->display->display_options['arguments']['created_month']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['created_month']['default_argument_skip_url'] = 0;
  $handler->display->display_options['arguments']['created_month']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['created_month']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['created_month']['summary_options']['items_per_page'] = '25';
  /* Contextual filter: Content: Created day */
  $handler->display->display_options['arguments']['created_day']['id'] = 'created_day';
  $handler->display->display_options['arguments']['created_day']['table'] = 'node';
  $handler->display->display_options['arguments']['created_day']['field'] = 'created_day';
  $handler->display->display_options['arguments']['created_day']['title_enable'] = 1;
  $handler->display->display_options['arguments']['created_day']['title'] = 'Regulatory News for %1 %2 %3';
  $handler->display->display_options['arguments']['created_day']['breadcrumb_enable'] = 1;
  $handler->display->display_options['arguments']['created_day']['breadcrumb'] = 'Regulatory News %2';
  $handler->display->display_options['arguments']['created_day']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['created_day']['default_argument_skip_url'] = 0;
  $handler->display->display_options['arguments']['created_day']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['created_day']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['created_day']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'regulatory_reports' => 'regulatory_reports',
  );
  $handler->display->display_options['path'] = 'regulatory-reports';

  /* Display: Market Analysis */
  $handler = $view->new_display('page', 'Market Analysis', 'page_2');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Market Analysis';
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  /* Contextual filter: Content: Created year */
  $handler->display->display_options['arguments']['created_year']['id'] = 'created_year';
  $handler->display->display_options['arguments']['created_year']['table'] = 'node';
  $handler->display->display_options['arguments']['created_year']['field'] = 'created_year';
  $handler->display->display_options['arguments']['created_year']['title_enable'] = 1;
  $handler->display->display_options['arguments']['created_year']['title'] = 'Market Analysis %1';
  $handler->display->display_options['arguments']['created_year']['breadcrumb_enable'] = 1;
  $handler->display->display_options['arguments']['created_year']['breadcrumb'] = 'Market Analysis';
  $handler->display->display_options['arguments']['created_year']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['created_year']['default_argument_skip_url'] = 0;
  $handler->display->display_options['arguments']['created_year']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['created_year']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['created_year']['summary_options']['items_per_page'] = '25';
  /* Contextual filter: Content: Created month */
  $handler->display->display_options['arguments']['created_month']['id'] = 'created_month';
  $handler->display->display_options['arguments']['created_month']['table'] = 'node';
  $handler->display->display_options['arguments']['created_month']['field'] = 'created_month';
  $handler->display->display_options['arguments']['created_month']['title_enable'] = 1;
  $handler->display->display_options['arguments']['created_month']['title'] = 'Market Analysis %1 %2';
  $handler->display->display_options['arguments']['created_month']['breadcrumb_enable'] = 1;
  $handler->display->display_options['arguments']['created_month']['breadcrumb'] = 'Market Analysis %1';
  $handler->display->display_options['arguments']['created_month']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['created_month']['default_argument_skip_url'] = 0;
  $handler->display->display_options['arguments']['created_month']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['created_month']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['created_month']['summary_options']['items_per_page'] = '25';
  /* Contextual filter: Content: Created day */
  $handler->display->display_options['arguments']['created_day']['id'] = 'created_day';
  $handler->display->display_options['arguments']['created_day']['table'] = 'node';
  $handler->display->display_options['arguments']['created_day']['field'] = 'created_day';
  $handler->display->display_options['arguments']['created_day']['title_enable'] = 1;
  $handler->display->display_options['arguments']['created_day']['title'] = 'Market Analysis for %1 %2 %3';
  $handler->display->display_options['arguments']['created_day']['breadcrumb_enable'] = 1;
  $handler->display->display_options['arguments']['created_day']['breadcrumb'] = 'Market Analysis %2';
  $handler->display->display_options['arguments']['created_day']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['created_day']['default_argument_skip_url'] = 0;
  $handler->display->display_options['arguments']['created_day']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['created_day']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['created_day']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'market_analysis' => 'market_analysis',
  );
  $handler->display->display_options['path'] = 'market-analysis';

  /* Display: Data */
  $handler = $view->new_display('page', 'Data', 'page_3');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Data';
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  /* Contextual filter: Content: Created year */
  $handler->display->display_options['arguments']['created_year']['id'] = 'created_year';
  $handler->display->display_options['arguments']['created_year']['table'] = 'node';
  $handler->display->display_options['arguments']['created_year']['field'] = 'created_year';
  $handler->display->display_options['arguments']['created_year']['title_enable'] = 1;
  $handler->display->display_options['arguments']['created_year']['title'] = 'Data %1';
  $handler->display->display_options['arguments']['created_year']['breadcrumb_enable'] = 1;
  $handler->display->display_options['arguments']['created_year']['breadcrumb'] = '%1';
  $handler->display->display_options['arguments']['created_year']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['created_year']['default_argument_skip_url'] = 0;
  $handler->display->display_options['arguments']['created_year']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['created_year']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['created_year']['summary_options']['items_per_page'] = '25';
  /* Contextual filter: Content: Created month */
  $handler->display->display_options['arguments']['created_month']['id'] = 'created_month';
  $handler->display->display_options['arguments']['created_month']['table'] = 'node';
  $handler->display->display_options['arguments']['created_month']['field'] = 'created_month';
  $handler->display->display_options['arguments']['created_month']['title_enable'] = 1;
  $handler->display->display_options['arguments']['created_month']['title'] = 'Data %1 %2';
  $handler->display->display_options['arguments']['created_month']['breadcrumb_enable'] = 1;
  $handler->display->display_options['arguments']['created_month']['breadcrumb'] = '%2';
  $handler->display->display_options['arguments']['created_month']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['created_month']['default_argument_skip_url'] = 0;
  $handler->display->display_options['arguments']['created_month']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['created_month']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['created_month']['summary_options']['items_per_page'] = '25';
  /* Contextual filter: Content: Created day */
  $handler->display->display_options['arguments']['created_day']['id'] = 'created_day';
  $handler->display->display_options['arguments']['created_day']['table'] = 'node';
  $handler->display->display_options['arguments']['created_day']['field'] = 'created_day';
  $handler->display->display_options['arguments']['created_day']['title_enable'] = 1;
  $handler->display->display_options['arguments']['created_day']['title'] = 'Data for %1 %2 %3';
  $handler->display->display_options['arguments']['created_day']['breadcrumb_enable'] = 1;
  $handler->display->display_options['arguments']['created_day']['breadcrumb'] = '%3';
  $handler->display->display_options['arguments']['created_day']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['created_day']['default_argument_skip_url'] = 0;
  $handler->display->display_options['arguments']['created_day']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['created_day']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['created_day']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'data' => 'data',
  );
  $handler->display->display_options['path'] = 'data';
  $export['content_browser'] = $view;

  return $export;
}
