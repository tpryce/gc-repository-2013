<?php
$menu = array (
  'menu_name' => 'menu-sector-menu',
  'title' => 'Industry Sector',
  'description' => 'Mega-menu industry sector section.',
  'items' =>
  array (
    0 =>
    array (
      'menu_name' => 'menu-sector-menu',
      'mlid' => '1536',
      'plid' => '0',
      'link_path' => 'search/site/',
      'router_path' => 'search/site/%',
      'link_title' => 'Bingo',
      'options' =>
      array (
        'attributes' =>
        array (
          'title' => '',
        ),
        'query' =>
        array (
          'f[0]' => 'im_field_sectors:51',
        ),
      ),
      'module' => 'menu',
      'hidden' => '0',
      'external' => '0',
      'has_children' => '0',
      'expanded' => '0',
      'weight' => '-50',
      'depth' => '1',
      'customized' => '1',
      'p1' => '1536',
      'p2' => '0',
      'p3' => '0',
      'p4' => '0',
      'p5' => '0',
      'p6' => '0',
      'p7' => '0',
      'p8' => '0',
      'p9' => '0',
      'updated' => '0',
      'load_functions' =>
      array (
        2 => NULL,
      ),
      'to_arg_functions' => '',
      'access_callback' => 'user_access',
      'access_arguments' => 'a:1:{i:0;s:14:"search content";}',
      'page_callback' => 'apachesolr_search_custom_page',
      'page_arguments' => 'a:3:{i:0;s:11:"core_search";i:1;i:2;i:2;b:0;}',
      'delivery_callback' => '',
      'tab_parent' => 'search/site',
      'tab_root' => 'search/site/%',
      'title' => 'Bingo',
      'title_callback' => 't',
      'title_arguments' => '',
      'theme_callback' => '',
      'theme_arguments' => 'a:0:{}',
      'type' => '132',
      'description' => '',
      'in_active_trail' => false,
      'href' => 'search/site/',
      'access' => true,
      'localized_options' =>
      array (
        'attributes' =>
        array (
          'title' => '',
        ),
        'query' =>
        array (
          'f[0]' => 'im_field_sectors:51',
        ),
      ),
    ),
    1 =>
    array (
      'menu_name' => 'menu-sector-menu',
      'mlid' => '1551',
      'plid' => '0',
      'link_path' => 'search/site/',
      'router_path' => 'search/site/%',
      'link_title' => 'Card Rooms/Poker',
      'options' =>
      array (
        'attributes' =>
        array (
          'title' => '',
        ),
        'query' =>
        array (
          'f[0]' => 'im_field_sectors:325',
        ),
      ),
      'module' => 'menu',
      'hidden' => '0',
      'external' => '0',
      'has_children' => '0',
      'expanded' => '0',
      'weight' => '-49',
      'depth' => '1',
      'customized' => '1',
      'p1' => '1551',
      'p2' => '0',
      'p3' => '0',
      'p4' => '0',
      'p5' => '0',
      'p6' => '0',
      'p7' => '0',
      'p8' => '0',
      'p9' => '0',
      'updated' => '0',
      'load_functions' =>
      array (
        2 => NULL,
      ),
      'to_arg_functions' => '',
      'access_callback' => 'user_access',
      'access_arguments' => 'a:1:{i:0;s:14:"search content";}',
      'page_callback' => 'apachesolr_search_custom_page',
      'page_arguments' => 'a:3:{i:0;s:11:"core_search";i:1;i:2;i:2;b:0;}',
      'delivery_callback' => '',
      'tab_parent' => 'search/site',
      'tab_root' => 'search/site/%',
      'title' => 'Card Rooms/Poker',
      'title_callback' => 't',
      'title_arguments' => '',
      'theme_callback' => '',
      'theme_arguments' => 'a:0:{}',
      'type' => '132',
      'description' => '',
      'in_active_trail' => false,
      'href' => 'search/site/',
      'access' => true,
      'localized_options' =>
      array (
        'attributes' =>
        array (
          'title' => '',
        ),
        'query' =>
        array (
          'f[0]' => 'im_field_sectors:325',
        ),
      ),
    ),
    2 =>
    array (
      'menu_name' => 'menu-sector-menu',
      'mlid' => '1535',
      'plid' => '0',
      'link_path' => 'search/site/',
      'router_path' => 'search/site/%',
      'link_title' => 'Casino',
      'options' =>
      array (
        'attributes' =>
        array (
          'title' => '',
        ),
        'query' =>
        array (
          'f[0]' => 'im_field_sectors:52',
        ),
      ),
      'module' => 'menu',
      'hidden' => '0',
      'external' => '0',
      'has_children' => '0',
      'expanded' => '0',
      'weight' => '-48',
      'depth' => '1',
      'customized' => '1',
      'p1' => '1535',
      'p2' => '0',
      'p3' => '0',
      'p4' => '0',
      'p5' => '0',
      'p6' => '0',
      'p7' => '0',
      'p8' => '0',
      'p9' => '0',
      'updated' => '0',
      'load_functions' =>
      array (
        2 => NULL,
      ),
      'to_arg_functions' => '',
      'access_callback' => 'user_access',
      'access_arguments' => 'a:1:{i:0;s:14:"search content";}',
      'page_callback' => 'apachesolr_search_custom_page',
      'page_arguments' => 'a:3:{i:0;s:11:"core_search";i:1;i:2;i:2;b:0;}',
      'delivery_callback' => '',
      'tab_parent' => 'search/site',
      'tab_root' => 'search/site/%',
      'title' => 'Casino',
      'title_callback' => 't',
      'title_arguments' => '',
      'theme_callback' => '',
      'theme_arguments' => 'a:0:{}',
      'type' => '132',
      'description' => '',
      'in_active_trail' => false,
      'href' => 'search/site/',
      'access' => true,
      'localized_options' =>
      array (
        'attributes' =>
        array (
          'title' => '',
        ),
        'query' =>
        array (
          'f[0]' => 'im_field_sectors:52',
        ),
      ),
    ),
    3 =>
    array (
      'menu_name' => 'menu-sector-menu',
      'mlid' => '1552',
      'plid' => '0',
      'link_path' => 'search/site/',
      'router_path' => 'search/site/%',
      'link_title' => 'Charitable Gaming',
      'options' =>
      array (
        'attributes' =>
        array (
          'title' => '',
        ),
        'query' =>
        array (
          'f[0]' => 'im_field_sectors:14909',
        ),
      ),
      'module' => 'menu',
      'hidden' => '0',
      'external' => '0',
      'has_children' => '0',
      'expanded' => '0',
      'weight' => '-47',
      'depth' => '1',
      'customized' => '1',
      'p1' => '1552',
      'p2' => '0',
      'p3' => '0',
      'p4' => '0',
      'p5' => '0',
      'p6' => '0',
      'p7' => '0',
      'p8' => '0',
      'p9' => '0',
      'updated' => '0',
      'load_functions' =>
      array (
        2 => NULL,
      ),
      'to_arg_functions' => '',
      'access_callback' => 'user_access',
      'access_arguments' => 'a:1:{i:0;s:14:"search content";}',
      'page_callback' => 'apachesolr_search_custom_page',
      'page_arguments' => 'a:3:{i:0;s:11:"core_search";i:1;i:2;i:2;b:0;}',
      'delivery_callback' => '',
      'tab_parent' => 'search/site',
      'tab_root' => 'search/site/%',
      'title' => 'Charitable Gaming',
      'title_callback' => 't',
      'title_arguments' => '',
      'theme_callback' => '',
      'theme_arguments' => 'a:0:{}',
      'type' => '132',
      'description' => '',
      'in_active_trail' => false,
      'href' => 'search/site/',
      'access' => true,
      'localized_options' =>
      array (
        'attributes' =>
        array (
          'title' => '',
        ),
        'query' =>
        array (
          'f[0]' => 'im_field_sectors:14909',
        ),
      ),
    ),
    4 =>
    array (
      'menu_name' => 'menu-sector-menu',
      'mlid' => '1539',
      'plid' => '0',
      'link_path' => 'search/site/',
      'router_path' => 'search/site/%',
      'link_title' => 'Lottery',
      'options' =>
      array (
        'attributes' =>
        array (
          'title' => '',
        ),
        'query' =>
        array (
          'f[0]' => 'im_field_sectors:53',
        ),
      ),
      'module' => 'menu',
      'hidden' => '0',
      'external' => '0',
      'has_children' => '0',
      'expanded' => '0',
      'weight' => '-46',
      'depth' => '1',
      'customized' => '1',
      'p1' => '1539',
      'p2' => '0',
      'p3' => '0',
      'p4' => '0',
      'p5' => '0',
      'p6' => '0',
      'p7' => '0',
      'p8' => '0',
      'p9' => '0',
      'updated' => '0',
      'load_functions' =>
      array (
        2 => NULL,
      ),
      'to_arg_functions' => '',
      'access_callback' => 'user_access',
      'access_arguments' => 'a:1:{i:0;s:14:"search content";}',
      'page_callback' => 'apachesolr_search_custom_page',
      'page_arguments' => 'a:3:{i:0;s:11:"core_search";i:1;i:2;i:2;b:0;}',
      'delivery_callback' => '',
      'tab_parent' => 'search/site',
      'tab_root' => 'search/site/%',
      'title' => 'Lottery',
      'title_callback' => 't',
      'title_arguments' => '',
      'theme_callback' => '',
      'theme_arguments' => 'a:0:{}',
      'type' => '132',
      'description' => '',
      'in_active_trail' => false,
      'href' => 'search/site/',
      'access' => true,
      'localized_options' =>
      array (
        'attributes' =>
        array (
          'title' => '',
        ),
        'query' =>
        array (
          'f[0]' => 'im_field_sectors:53',
        ),
      ),
    ),
    5 =>
    array (
      'menu_name' => 'menu-sector-menu',
      'mlid' => '1540',
      'plid' => '0',
      'link_path' => 'search/site/',
      'router_path' => 'search/site/%',
      'link_title' => 'Online Gambling',
      'options' =>
      array (
        'attributes' =>
        array (
          'title' => '',
        ),
        'query' =>
        array (
          'f[0]' => 'im_field_sectors:55',
        ),
      ),
      'module' => 'menu',
      'hidden' => '0',
      'external' => '0',
      'has_children' => '0',
      'expanded' => '0',
      'weight' => '-45',
      'depth' => '1',
      'customized' => '1',
      'p1' => '1540',
      'p2' => '0',
      'p3' => '0',
      'p4' => '0',
      'p5' => '0',
      'p6' => '0',
      'p7' => '0',
      'p8' => '0',
      'p9' => '0',
      'updated' => '0',
      'load_functions' =>
      array (
        2 => NULL,
      ),
      'to_arg_functions' => '',
      'access_callback' => 'user_access',
      'access_arguments' => 'a:1:{i:0;s:14:"search content";}',
      'page_callback' => 'apachesolr_search_custom_page',
      'page_arguments' => 'a:3:{i:0;s:11:"core_search";i:1;i:2;i:2;b:0;}',
      'delivery_callback' => '',
      'tab_parent' => 'search/site',
      'tab_root' => 'search/site/%',
      'title' => 'Online Gambling',
      'title_callback' => 't',
      'title_arguments' => '',
      'theme_callback' => '',
      'theme_arguments' => 'a:0:{}',
      'type' => '132',
      'description' => '',
      'in_active_trail' => false,
      'href' => 'search/site/',
      'access' => true,
      'localized_options' =>
      array (
        'attributes' =>
        array (
          'title' => '',
        ),
        'query' =>
        array (
          'f[0]' => 'im_field_sectors:55',
        ),
      ),
    ),
    6 =>
    array (
      'menu_name' => 'menu-sector-menu',
      'mlid' => '1538',
      'plid' => '0',
      'link_path' => 'search/site/',
      'router_path' => 'search/site/%',
      'link_title' => 'OTB and Betting Shops',
      'options' =>
      array (
        'attributes' =>
        array (
          'title' => '',
        ),
        'query' =>
        array (
          'f[0]' => 'im_field_sectors:433',
        ),
      ),
      'module' => 'menu',
      'hidden' => '0',
      'external' => '0',
      'has_children' => '0',
      'expanded' => '0',
      'weight' => '-44',
      'depth' => '1',
      'customized' => '1',
      'p1' => '1538',
      'p2' => '0',
      'p3' => '0',
      'p4' => '0',
      'p5' => '0',
      'p6' => '0',
      'p7' => '0',
      'p8' => '0',
      'p9' => '0',
      'updated' => '0',
      'load_functions' =>
      array (
        2 => NULL,
      ),
      'to_arg_functions' => '',
      'access_callback' => 'user_access',
      'access_arguments' => 'a:1:{i:0;s:14:"search content";}',
      'page_callback' => 'apachesolr_search_custom_page',
      'page_arguments' => 'a:3:{i:0;s:11:"core_search";i:1;i:2;i:2;b:0;}',
      'delivery_callback' => '',
      'tab_parent' => 'search/site',
      'tab_root' => 'search/site/%',
      'title' => 'OTB and Betting Shops',
      'title_callback' => 't',
      'title_arguments' => '',
      'theme_callback' => '',
      'theme_arguments' => 'a:0:{}',
      'type' => '132',
      'description' => '',
      'in_active_trail' => false,
      'href' => 'search/site/',
      'access' => true,
      'localized_options' =>
      array (
        'attributes' =>
        array (
          'title' => '',
        ),
        'query' =>
        array (
          'f[0]' => 'im_field_sectors:433',
        ),
      ),
    ),
    7 =>
    array (
      'menu_name' => 'menu-sector-menu',
      'mlid' => '1550',
      'plid' => '0',
      'link_path' => 'search/site/',
      'router_path' => 'search/site/%',
      'link_title' => 'Racetrack/Racino',
      'options' =>
      array (
        'attributes' =>
        array (
          'title' => '',
        ),
        'query' =>
        array (
          'f[0]' => 'im_field_sectors:327',
        ),
      ),
      'module' => 'menu',
      'hidden' => '0',
      'external' => '0',
      'has_children' => '0',
      'expanded' => '0',
      'weight' => '0',
      'depth' => '1',
      'customized' => '1',
      'p1' => '1550',
      'p2' => '0',
      'p3' => '0',
      'p4' => '0',
      'p5' => '0',
      'p6' => '0',
      'p7' => '0',
      'p8' => '0',
      'p9' => '0',
      'updated' => '0',
      'load_functions' =>
      array (
        2 => NULL,
      ),
      'to_arg_functions' => '',
      'access_callback' => 'user_access',
      'access_arguments' => 'a:1:{i:0;s:14:"search content";}',
      'page_callback' => 'apachesolr_search_custom_page',
      'page_arguments' => 'a:3:{i:0;s:11:"core_search";i:1;i:2;i:2;b:0;}',
      'delivery_callback' => '',
      'tab_parent' => 'search/site',
      'tab_root' => 'search/site/%',
      'title' => 'Racetrack/Racino',
      'title_callback' => 't',
      'title_arguments' => '',
      'theme_callback' => '',
      'theme_arguments' => 'a:0:{}',
      'type' => '132',
      'description' => '',
      'in_active_trail' => false,
      'href' => 'search/site/',
      'access' => true,
      'localized_options' =>
      array (
        'attributes' =>
        array (
          'title' => '',
        ),
        'query' =>
        array (
          'f[0]' => 'im_field_sectors:327',
        ),
      ),
    ),
    8 =>
    array (
      'menu_name' => 'menu-sector-menu',
      'mlid' => '1542',
      'plid' => '0',
      'link_path' => 'search/site/',
      'router_path' => 'search/site/%',
      'link_title' => 'Skill Gaming',
      'options' =>
      array (
        'attributes' =>
        array (
          'title' => '',
        ),
        'query' =>
        array (
          'f[0]' => 'im_field_sectors:329',
        ),
      ),
      'module' => 'menu',
      'hidden' => '0',
      'external' => '0',
      'has_children' => '0',
      'expanded' => '0',
      'weight' => '0',
      'depth' => '1',
      'customized' => '1',
      'p1' => '1542',
      'p2' => '0',
      'p3' => '0',
      'p4' => '0',
      'p5' => '0',
      'p6' => '0',
      'p7' => '0',
      'p8' => '0',
      'p9' => '0',
      'updated' => '0',
      'load_functions' =>
      array (
        2 => NULL,
      ),
      'to_arg_functions' => '',
      'access_callback' => 'user_access',
      'access_arguments' => 'a:1:{i:0;s:14:"search content";}',
      'page_callback' => 'apachesolr_search_custom_page',
      'page_arguments' => 'a:3:{i:0;s:11:"core_search";i:1;i:2;i:2;b:0;}',
      'delivery_callback' => '',
      'tab_parent' => 'search/site',
      'tab_root' => 'search/site/%',
      'title' => 'Skill Gaming',
      'title_callback' => 't',
      'title_arguments' => '',
      'theme_callback' => '',
      'theme_arguments' => 'a:0:{}',
      'type' => '132',
      'description' => '',
      'in_active_trail' => false,
      'href' => 'search/site/',
      'access' => true,
      'localized_options' =>
      array (
        'attributes' =>
        array (
          'title' => '',
        ),
        'query' =>
        array (
          'f[0]' => 'im_field_sectors:329',
        ),
      ),
    ),
    9 =>
    array (
      'menu_name' => 'menu-sector-menu',
      'mlid' => '1541',
      'plid' => '0',
      'link_path' => 'search/site/',
      'router_path' => 'search/site/%',
      'link_title' => 'Slot Machines',
      'options' =>
      array (
        'attributes' =>
        array (
          'title' => '',
        ),
        'query' =>
        array (
          'f[0]' => 'im_field_sectors:57',
        ),
      ),
      'module' => 'menu',
      'hidden' => '0',
      'external' => '0',
      'has_children' => '0',
      'expanded' => '0',
      'weight' => '0',
      'depth' => '1',
      'customized' => '1',
      'p1' => '1541',
      'p2' => '0',
      'p3' => '0',
      'p4' => '0',
      'p5' => '0',
      'p6' => '0',
      'p7' => '0',
      'p8' => '0',
      'p9' => '0',
      'updated' => '0',
      'load_functions' =>
      array (
        2 => NULL,
      ),
      'to_arg_functions' => '',
      'access_callback' => 'user_access',
      'access_arguments' => 'a:1:{i:0;s:14:"search content";}',
      'page_callback' => 'apachesolr_search_custom_page',
      'page_arguments' => 'a:3:{i:0;s:11:"core_search";i:1;i:2;i:2;b:0;}',
      'delivery_callback' => '',
      'tab_parent' => 'search/site',
      'tab_root' => 'search/site/%',
      'title' => 'Slot Machines',
      'title_callback' => 't',
      'title_arguments' => '',
      'theme_callback' => '',
      'theme_arguments' => 'a:0:{}',
      'type' => '132',
      'description' => '',
      'in_active_trail' => false,
      'href' => 'search/site/',
      'access' => true,
      'localized_options' =>
      array (
        'attributes' =>
        array (
          'title' => '',
        ),
        'query' =>
        array (
          'f[0]' => 'im_field_sectors:57',
        ),
      ),
    ),
    10 =>
    array (
      'menu_name' => 'menu-sector-menu',
      'mlid' => '1543',
      'plid' => '0',
      'link_path' => 'search/site/',
      'router_path' => 'search/site/%',
      'link_title' => 'Spread Betting',
      'options' =>
      array (
        'attributes' =>
        array (
          'title' => '',
        ),
        'query' =>
        array (
          'f[0]' => 'im_field_sectors:328',
        ),
      ),
      'module' => 'menu',
      'hidden' => '0',
      'external' => '0',
      'has_children' => '0',
      'expanded' => '0',
      'weight' => '0',
      'depth' => '1',
      'customized' => '1',
      'p1' => '1543',
      'p2' => '0',
      'p3' => '0',
      'p4' => '0',
      'p5' => '0',
      'p6' => '0',
      'p7' => '0',
      'p8' => '0',
      'p9' => '0',
      'updated' => '0',
      'load_functions' =>
      array (
        2 => NULL,
      ),
      'to_arg_functions' => '',
      'access_callback' => 'user_access',
      'access_arguments' => 'a:1:{i:0;s:14:"search content";}',
      'page_callback' => 'apachesolr_search_custom_page',
      'page_arguments' => 'a:3:{i:0;s:11:"core_search";i:1;i:2;i:2;b:0;}',
      'delivery_callback' => '',
      'tab_parent' => 'search/site',
      'tab_root' => 'search/site/%',
      'title' => 'Spread Betting',
      'title_callback' => 't',
      'title_arguments' => '',
      'theme_callback' => '',
      'theme_arguments' => 'a:0:{}',
      'type' => '132',
      'description' => '',
      'in_active_trail' => false,
      'href' => 'search/site/',
      'access' => true,
      'localized_options' =>
      array (
        'attributes' =>
        array (
          'title' => '',
        ),
        'query' =>
        array (
          'f[0]' => 'im_field_sectors:328',
        ),
      ),
    ),
    11 =>
    array (
      'menu_name' => 'menu-sector-menu',
      'mlid' => '1537',
      'plid' => '0',
      'link_path' => 'search/site/',
      'router_path' => 'search/site/%',
      'link_title' => 'Tribal Gaming',
      'options' =>
      array (
        'attributes' =>
        array (
          'title' => '',
        ),
        'query' =>
        array (
          'f[0]' => 'im_field_sectors:435',
        ),
      ),
      'module' => 'menu',
      'hidden' => '0',
      'external' => '0',
      'has_children' => '0',
      'expanded' => '0',
      'weight' => '0',
      'depth' => '1',
      'customized' => '1',
      'p1' => '1537',
      'p2' => '0',
      'p3' => '0',
      'p4' => '0',
      'p5' => '0',
      'p6' => '0',
      'p7' => '0',
      'p8' => '0',
      'p9' => '0',
      'updated' => '0',
      'load_functions' =>
      array (
        2 => NULL,
      ),
      'to_arg_functions' => '',
      'access_callback' => 'user_access',
      'access_arguments' => 'a:1:{i:0;s:14:"search content";}',
      'page_callback' => 'apachesolr_search_custom_page',
      'page_arguments' => 'a:3:{i:0;s:11:"core_search";i:1;i:2;i:2;b:0;}',
      'delivery_callback' => '',
      'tab_parent' => 'search/site',
      'tab_root' => 'search/site/%',
      'title' => 'Tribal Gaming',
      'title_callback' => 't',
      'title_arguments' => '',
      'theme_callback' => '',
      'theme_arguments' => 'a:0:{}',
      'type' => '132',
      'description' => '',
      'in_active_trail' => false,
      'href' => 'search/site/',
      'access' => true,
      'localized_options' =>
      array (
        'attributes' =>
        array (
          'title' => '',
        ),
        'query' =>
        array (
          'f[0]' => 'im_field_sectors:435',
        ),
      ),
    ),
  ),
);