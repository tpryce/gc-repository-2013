// $Id$

function solr_search_saved_panel_move_up(sid) {
  (function ($) {
    var el = $("#solr-search-saved-panel-" + sid);
    var prevel = el.prev();
    prevel.before(el);
    solr_search_saved_panel_update_order(el.parent());
    solr_search_saved_panel_hide_move_buttons(el.parent());
  })(jQuery);
}

function solr_search_saved_panel_move_down(sid) {
  (function ($) {
    var el = $("#solr-search-saved-panel-" + sid);
    var prevel = el.next();
    prevel.after(el);
    solr_search_saved_panel_update_order(el.parent());
    solr_search_saved_panel_hide_move_buttons(el.parent());
  })(jQuery);
}
function solr_search_saved_panel_move_to(o1, o2) {
  (function ($) {
    //		console.log([o1,o2]);
    var col1 = o1.parent();
    var col2 = o2.parent();

    o1.after(o2);

    solr_search_saved_panel_update_order(col2);
    solr_search_saved_panel_hide_move_buttons(col2);

    if (col1.attr('column') != col2.attr('column')) {
      solr_search_saved_panel_update_order(col1);
      solr_search_saved_panel_hide_move_buttons(col1);
    }
  })(jQuery);

}

function solr_search_saved_panel_remove(sid) {
  (function ($) {
    jQuery.get("/solr_search_saved/hide/" + sid);
    var el = $("#solr-search-saved-panel-" + sid);
    el.slideUp("slow", function() {
      var container = $(this).parent();
      $(this).remove();
      solr_search_saved_panel_hide_move_buttons(container);
    });
  })(jQuery);
}


function solr_search_saved_panel_update_order(container) {
  (function ($) {
    var sids = "";
    container.children(".panel-solr-searsh-saved-result").each(function() {
      var sid = $(this).attr("id").replace("solr-search-saved-panel-", "");
      sids += sid + ":";
    });

    sids = container.attr("column") + ":" + sids;
    jQuery.get("/solr_search_saved/reorder/" + sids);
  })(jQuery);
}

function solr_search_saved_panel_hide_move_buttons(container) {
  (function ($) {
    container.find(".panel-solr-searsh-saved-result-buttons IMG").show();
    container.find(".panel-solr-searsh-saved-result-buttons IMG.panel-solr-searsh-saved-result-button-up:first").hide();
    container.find(".panel-solr-searsh-saved-result-buttons IMG.panel-solr-searsh-saved-result-button-down:last").hide();
  })(jQuery);
}

function checkEmptyColumn(col) {
  (function ($) {

    $(".panel-solr-search-saved-result-content").find(".solr_search_saved_dynamic_column").each(function() {
      var h = col.children('div.panel-solr-searsh-saved-result').length;
      if (h == 0) {
        col.css('height', '200px');
      } else {
        col.css('height', 'auto');
      }
    });
  })(jQuery);
}
(function ($) {
  /**
   * o - jQuery object
   * os - jQuery object set to check height
   */
  function _fix_height(o, os) {
    os.each(
      function() {
        $(this).height("auto");
      }
    );

    var maxHeight = 0;
    os.each(
      function() {
        if (maxHeight < $(this).height()) {
          maxHeight = $(this).height();
        }
      }
    );
    os.each(
      function() {
        if (maxHeight != 0) {
          if ($(this).height() <= maxHeight) {
            $(this).height(maxHeight);
          }
        }
      }
    );

    //		console.log([maxHeight, o.height()]);

    if (maxHeight != 0) {
      if (o.height() <= maxHeight) {
        o.height(maxHeight);
      }
    }
  }

  $(document).ready(
    function() {
      (function ($) {
        $(".solr_search_saved_dynamic_column").each(function() {
          solr_search_saved_panel_hide_move_buttons($(this).parent());
        });

        $(".solr_search_saved_dynamic_column").find(".solr-search-results").each(
          function() {
            var collapsible = $(this).parent().parent();
            //					console.log(collapsible);
            //fix html theme inconsistences
            if (collapsible.hasClass("solrssr-collapsed") && $(this).find(".solr-search-results-item-teaser").css("display") != "none") {
              $(this).find(".solr-search-results-item-teaser").css("display", "none");
            }
          }
        );

        $(".solrssr-collapsible .panel-solr-searsh-saved-result-header h2").click(
          function() {
            var header = $(this).parent().parent();

            if (header.hasClass("solrssr-collapsible-sync")) {
              //						console.log(header);
              $(".solrssr-collapsible-sync").find(".solr-search-results").each(

                function() {
                  //								console.log($(this));
                  $(this).find(".solr-search-results-item-teaser").slideToggle("normal");
                }
              );

              $("#homepage-panel .homepage-main-block").height("auto");

              setTimeout(
                function() {
                  $("#homepage-panel").find(".panels-flexible-row-5-row_1-middle").find(".solrssr-collapsible-sync")
                    .find(".solr-search-results").each(
                    function() {
                      _fix_height($(this),
                        $("#homepage-reg-news, #homepage-market-news, #homepage-reg-reports").find(".solr-search-results")
                      );
                    });
                },
                500);
              $(".solrssr-collapsible-sync").toggleClass("solrssr-collapsed");
            } else {
              header.find(".solr-search-results").each(
                function() {
                  $(this).find(".solr-search-results-item-teaser").slideToggle("normal");
                }
              );
              header.toggleClass("solrssr-collapsed");

            }

          }
        );

        $("div.solr_search_saved_dynamic_column").each(function() {
          $(this).sortable(
            {
              items: '.panel-solr-searsh-saved-result',
              helper: 'helper',
              placeholder: 'solr-sort-helper',
              forcePlaceholderSize: true,
              handle: '.panel-solr-searsh-saved-result-header',
              cursor: 'pointer',
              //tolerance: 'pointer',
              connectWith: '.solr_search_saved_dynamic_column',
              stop: function(e, ui) {
                $('.solr_search_saved_dynamic_column').each(
                  function() {
                    solr_search_saved_panel_update_order($(this));
                    solr_search_saved_panel_hide_move_buttons($(this));
                  }
                );
              },
              activate: function(e, ui) {
                //					console.log($(this));
                $(".solr_search_saved_dynamic_column").height("auto");

                $(this).addClass("droppable-can-receive");
              },
              deactivate: function(e, ui) {
                $(this).removeClass("droppable-can-receive");
                $(".solr_search_saved_dynamic_column").height("auto");
              }

            }
          ).disableSelection();
        });

        var userAgent = navigator.userAgent.toLowerCase();
        if (userAgent.match(/firefox/)) {
          $(".solr_search_saved_dynamic_column").sortable({
            connectWith: 'div.solr_search_saved_dynamic_column',
            sort: function(event, ui) {
              ui.helper.css({'top' : ui.position.top + $(window).scrollTop() + 'px'});
            }
          });
        }
      })(jQuery);

    }
  );
  if (!($.browser.msie && $.browser.version == "6.0")) {
    $(window).resize(function() {
        setTimeout(
          function() {
            (function ($) {
              $("#homepage-panel").find(".panels-flexible-row-5-row_1-middle").find(".solrssr-collapsible-sync")
                .find(".solr-search-results").each(
                function() {
                  _fix_height($(this),
                    $("#homepage-reg-news, #homepage-market-news, #homepage-reg-reports").find(".solr-search-results")
                  );
                });
            })(jQuery);
          },
          500);
      }
    );
    $(document).ready(function() {
        setTimeout(
          function() {
            (function ($) {
              $("#homepage-panel").find(".panels-flexible-row-5-row_1-middle").find(".solrssr-collapsible-sync")
                .find(".solr-search-results").each(
                function() {
                  _fix_height($(this),
                    $("#homepage-reg-news, #homepage-market-news, #homepage-reg-reports").find(".solr-search-results")
                  );
                });
            })(jQuery);
          },
          500);
      }
    );
  }
})(jQuery);
