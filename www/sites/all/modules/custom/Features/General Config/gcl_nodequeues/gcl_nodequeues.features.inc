<?php
/**
 * @file
 * gcl_nodequeues.features.inc
 */

/**
 * Implements hook_fe_nodequeue_export_fields().
 */
function gcl_nodequeues_fe_nodequeue_export_fields() {
  $nodequeues = array();

  // Exported nodequeues: frontpage_market
  $nodequeues['frontpage_market'] = array(
    'qid' => '1',
    'name' => 'frontpage_market',
    'title' => 'Frontpage Market',
    'subqueue_title' => '',
    'size' => '7',
    'link' => 'Add to Frontpage Market',
    'link_remove' => 'Remove from Frontpage Market',
    'owner' => 'nodequeue',
    'show_in_ui' => '1',
    'show_in_tab' => '1',
    'show_in_links' => '1',
    'reference' => '0',
    'reverse' => '1',
    'i18n' => '0',
    'subqueues' => '1',
    'types' => array(
      0 => 'market_analysis',
    ),
    'roles' => array(),
    'count' => 0,
  );

  // Exported nodequeues: frontpage_regulatory
  $nodequeues['frontpage_regulatory'] = array(
    'qid' => '2',
    'name' => 'frontpage_regulatory',
    'title' => 'Frontpage Regulatory',
    'subqueue_title' => '',
    'size' => '7',
    'link' => 'Add to Frontpage Reg',
    'link_remove' => 'Remove from Frontpage Reg',
    'owner' => 'nodequeue',
    'show_in_ui' => '1',
    'show_in_tab' => '1',
    'show_in_links' => '1',
    'reference' => '0',
    'reverse' => '1',
    'i18n' => '0',
    'subqueues' => '1',
    'types' => array(
      0 => 'regulatory_news',
    ),
    'roles' => array(),
    'count' => 0,
  );

  // Exported nodequeues: landingpage_slideshow
  $nodequeues['landingpage_slideshow'] = array(
    'qid' => '3',
    'name' => 'landingpage_slideshow',
    'title' => 'Landingpage Slideshow',
    'subqueue_title' => '',
    'size' => '5',
    'link' => '',
    'link_remove' => '',
    'owner' => 'nodequeue',
    'show_in_ui' => '1',
    'show_in_tab' => '1',
    'show_in_links' => '0',
    'reference' => '0',
    'reverse' => '0',
    'i18n' => '0',
    'subqueues' => '1',
    'types' => array(
      0 => 'static_page',
    ),
    'roles' => array(),
    'count' => 0,
  );

  // Exported nodequeues: regulatory_reports
  $nodequeues['regulatory_reports'] = array(
    'qid' => '4',
    'name' => 'regulatory_reports',
    'title' => 'Regulatory Reports',
    'subqueue_title' => '',
    'size' => '7',
    'link' => 'Add to Frontpage Reg reports',
    'link_remove' => 'Remove from Frontpage Reg reports',
    'owner' => 'nodequeue',
    'show_in_ui' => '1',
    'show_in_tab' => '1',
    'show_in_links' => '1',
    'reference' => '0',
    'reverse' => '1',
    'i18n' => '0',
    'subqueues' => '1',
    'types' => array(
      0 => 'regulatory_reports',
    ),
    'roles' => array(),
    'count' => 0,
  );

  // Exported nodequeues: related_newsletter_content
  $nodequeues['related_newsletter_content'] = array(
    'qid' => '5',
    'name' => 'related_newsletter_content',
    'title' => 'Related Newsletter Content',
    'subqueue_title' => '',
    'size' => '5',
    'link' => 'Add to Related Newsletter Content',
    'link_remove' => 'Remove from Related Newsletter Content',
    'owner' => 'nodequeue',
    'show_in_ui' => '1',
    'show_in_tab' => '1',
    'show_in_links' => '1',
    'reference' => '0',
    'reverse' => '0',
    'i18n' => '0',
    'subqueues' => '1',
    'types' => array(
      0 => 'market_analysis',
      1 => 'press_release',
      2 => 'regulatory_news',
      3 => 'regulatory_reports',
    ),
    'roles' => array(),
    'count' => 0,
  );

  // Exported nodequeues: spotlight_top_stories
  $nodequeues['spotlight_top_stories'] = array(
    'qid' => '6',
    'name' => 'spotlight_top_stories',
    'title' => 'Spotlight Top Stories',
    'subqueue_title' => '%subqueue Spotlight Top Stories',
    'size' => '5',
    'link' => 'Add in %subqueue Top Stories',
    'link_remove' => 'Remove from %subqueue Top Stories',
    'owner' => 'smartqueue_taxonomy',
    'show_in_ui' => '1',
    'show_in_tab' => '1',
    'show_in_links' => '1',
    'reference' => 'field_spotlight',
    'reverse' => '1',
    'i18n' => '0',
    'subqueues' => '6',
    'types' => array(
      0 => 'market_analysis',
      1 => 'product',
      2 => 'regulatory_news',
      3 => 'regulatory_reports',
      4 => 'white_paper',
    ),
    'roles' => array(),
    'count' => 0,
    'use_parents' => '0',
  );

  // Exported nodequeues: us_newsletter_reg_reports
  $nodequeues['us_newsletter_reg_reports'] = array(
    'qid' => '7',
    'name' => 'us_newsletter_reg_reports',
    'title' => 'US Newsletter Reg Reports',
    'subqueue_title' => '',
    'size' => '6',
    'link' => 'Add to US Newsletter Reg Reports',
    'link_remove' => 'Remove from US Newsletter Reg Reports',
    'owner' => 'nodequeue',
    'show_in_ui' => '1',
    'show_in_tab' => '1',
    'show_in_links' => '1',
    'reference' => '0',
    'reverse' => '0',
    'i18n' => '0',
    'subqueues' => '1',
    'types' => array(
      0 => 'regulatory_reports',
    ),
    'roles' => array(),
    'count' => 0,
  );

  // Exported nodequeues: us_newsletter_top_read
  $nodequeues['us_newsletter_top_read'] = array(
    'qid' => '8',
    'name' => 'us_newsletter_top_read',
    'title' => 'US Newsletter Top Read',
    'subqueue_title' => '',
    'size' => '5',
    'link' => 'Add to US Newsletter Top Read',
    'link_remove' => 'Remove from US Newsletter Top Read',
    'owner' => 'nodequeue',
    'show_in_ui' => '1',
    'show_in_tab' => '1',
    'show_in_links' => '1',
    'reference' => '0',
    'reverse' => '0',
    'i18n' => '0',
    'subqueues' => '1',
    'types' => array(
      0 => 'market_analysis',
      1 => 'regulatory_news',
      2 => 'regulatory_reports',
    ),
    'roles' => array(),
    'count' => 0,
  );

  return $nodequeues;
}
