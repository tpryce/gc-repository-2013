<?php
/**
 * @file
 * gcl_menus.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function gcl_menus_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-about-us.
  $menus['menu-about-us'] = array(
    'menu_name' => 'menu-about-us',
    'title' => 'About us',
    'description' => 'The about us menu.',
  );
  // Exported menu: menu-helpful-links.
  $menus['menu-helpful-links'] = array(
    'menu_name' => 'menu-helpful-links',
    'title' => 'Helpful Links',
    'description' => '',
  );
  // Exported menu: user-menu.
  $menus['user-menu'] = array(
    'menu_name' => 'user-menu',
    'title' => 'User menu',
    'description' => 'The <em>User</em> menu contains links related to the user\'s account, as well as the \'Log out\' link.',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('About us');
  t('Helpful Links');
  t('The <em>User</em> menu contains links related to the user\'s account, as well as the \'Log out\' link.');
  t('The about us menu.');
  t('User menu');


  return $menus;
}
