<?php
/**
 * @file
 * feature_data.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function feature_data_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_anonymous_data';
  $strongarm->value = 0;
  $export['comment_anonymous_data'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_data';
  $strongarm->value = '1';
  $export['comment_data'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_mode_data';
  $strongarm->value = 1;
  $export['comment_default_mode_data'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_per_page_data';
  $strongarm->value = '50';
  $export['comment_default_per_page_data'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_form_location_data';
  $strongarm->value = 1;
  $export['comment_form_location_data'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_preview_data';
  $strongarm->value = '1';
  $export['comment_preview_data'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_subject_field_data';
  $strongarm->value = 1;
  $export['comment_subject_field_data'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'field_bundle_settings_node__data';
  $strongarm->value = array(
    'view_modes' => array(
      'teaser' => array(
        'custom_settings' => TRUE,
      ),
      'subscription' => array(
        'custom_settings' => TRUE,
      ),
      'full' => array(
        'custom_settings' => FALSE,
      ),
      'rss' => array(
        'custom_settings' => FALSE,
      ),
      'search_index' => array(
        'custom_settings' => FALSE,
      ),
      'search_result' => array(
        'custom_settings' => TRUE,
      ),
      'token' => array(
        'custom_settings' => FALSE,
      ),
      'anonymous user' => array(
        'custom_settings' => FALSE,
      ),
      'authenticated user' => array(
        'custom_settings' => FALSE,
      ),
      'Lite Subscription' => array(
        'custom_settings' => FALSE,
      ),
      'Full Subscription' => array(
        'custom_settings' => FALSE,
      ),
      'Internal Author' => array(
        'custom_settings' => FALSE,
      ),
      'Content Editor' => array(
        'custom_settings' => FALSE,
      ),
      'Content Manager' => array(
        'custom_settings' => FALSE,
      ),
      'admin' => array(
        'custom_settings' => FALSE,
      ),
      'anonymous' => array(
        'custom_settings' => TRUE,
      ),
      'Americas_Premium' => array(
        'custom_settings' => FALSE,
      ),
      'International_Premium' => array(
        'custom_settings' => FALSE,
      ),
      'Americas_Professional' => array(
        'custom_settings' => FALSE,
      ),
      'International_Professional' => array(
        'custom_settings' => FALSE,
      ),
      'Gambling_Data' => array(
        'custom_settings' => FALSE,
      ),
      'print' => array(
        'custom_settings' => FALSE,
      ),
    ),
    'extra_fields' => array(
      'form' => array(),
      'display' => array(
        'subscriptions_ui' => array(
          'default' => array(
            'weight' => '7',
            'visible' => FALSE,
          ),
          'teaser' => array(
            'weight' => '100',
            'visible' => FALSE,
          ),
          'search_result' => array(
            'weight' => '100',
            'visible' => TRUE,
          ),
          'anonymous' => array(
            'weight' => '5',
            'visible' => FALSE,
          ),
          'subscription' => array(
            'weight' => '15',
            'visible' => FALSE,
          ),
        ),
        'flippy_pager' => array(
          'teaser' => array(
            'weight' => '5',
            'visible' => FALSE,
          ),
          'default' => array(
            'weight' => '1',
            'visible' => TRUE,
          ),
          'search_result' => array(
            'weight' => '5',
            'visible' => TRUE,
          ),
          'anonymous' => array(
            'weight' => '1',
            'visible' => TRUE,
          ),
          'subscription' => array(
            'weight' => '1',
            'visible' => TRUE,
          ),
        ),
        'domain' => array(
          'anonymous' => array(
            'weight' => '4',
            'visible' => FALSE,
          ),
          'subscription' => array(
            'weight' => '13',
            'visible' => FALSE,
          ),
          'default' => array(
            'weight' => '9',
            'visible' => FALSE,
          ),
          'teaser' => array(
            'weight' => '1',
            'visible' => FALSE,
          ),
        ),
        'freetrialform' => array(
          'anonymous' => array(
            'weight' => '2',
            'visible' => TRUE,
          ),
          'subscription' => array(
            'weight' => '14',
            'visible' => FALSE,
          ),
          'default' => array(
            'weight' => '8',
            'visible' => FALSE,
          ),
          'teaser' => array(
            'weight' => '5',
            'visible' => FALSE,
          ),
        ),
      ),
    ),
  );
  $export['field_bundle_settings_node__data'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_options_data';
  $strongarm->value = array(
    0 => 'main-menu',
  );
  $export['menu_options_data'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_parent_data';
  $strongarm->value = 'main-menu:0';
  $export['menu_parent_data'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_data';
  $strongarm->value = array(
    0 => 'moderation',
    1 => 'revision',
  );
  $export['node_options_data'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_preview_data';
  $strongarm->value = '1';
  $export['node_preview_data'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_submitted_data';
  $strongarm->value = 1;
  $export['node_submitted_data'] = $strongarm;

  return $export;
}
