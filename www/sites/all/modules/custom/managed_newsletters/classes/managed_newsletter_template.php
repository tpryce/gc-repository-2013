<?php
// $Id$

abstract class managed_newsletter_template {
  protected $tid;
  protected $name;
  protected $title;
  protected $html;
  protected $text;

  public function __construct($tid = null) {
    if ($tid) {
      $this->tid = $tid;
      $this->load();
    }
  }

  public function get_tid() {
    return $this->tid;
  }

  public function get_name() {
    return $this->name;
  }

  public function get_title() {
    return $this->title;
  }

  public function get_html() {
    return $this->html;
  }

  public function get_text() {
    return $this->text;
  }


  public function set_name($value) {
    $this->name = $value;
  }

  public function set_title($value) {
    $this->title = $value;
  }

  public function set_html($value) {
    $this->html = $value;
  }

  public function set_text($value) {
    $this->text = $value;
  }

  public function get_edit_form() {
    $this->load();
    $form = array();
    $form['template'] = array(
		'#type' => 'value',
		'#value' => $this
		);
    $form['name'] = array(
		'#type' => 'textfield',
		'#title' => t('Name'),
		'#required' => true,
		'#default_value' => $this->name,
		'#description' => t('Machine-readable name of template, used as name of a token, must be unique')
		);
    $form['title'] = array(
		'#type' => 'textfield',
		'#title' => t('Title'),
		'#default_value' => $this->title,
		'#required' => true,
		'#description' => t('Template title, used only for presentation')
		);
    $form['main_template'] = array(
		'#type' => 'fieldset',
		'#title' => t('Templates')
		);
    $form['main_template']['html'] = array(
		'#type' => 'textarea',
		'#title' => t('Html template'),
		'#default_value' => $this->html,
		'#required' => true,
		'#description' => t('Html template, used for generation of html part of newsletters')
		);

    $form['main_template']['text'] = array(
		'#type' => 'textarea',
		'#title' => t('Text template'),
		'#required' => true,
		'#default_value' => $this->text,
		'#description' => t('Text template, used for generation of plain text part of newsletters')
		);

    $form['main_template']['tokens'] = array(
		'#type' => 'fieldset',
		'#title' => t('Available tokens'),
		'#collapsible' => true,
		'#collapsed' => true
		);
    $form['main_template']['tokens']['tokens'] = array(
		'#type' => 'markup',
		'#markup' => $this->get_tokens_table()
		);
    $form['submit'] = array(
		'#type' => 'submit',
		'#value' => t('Save'),
		'#weight' => 10
		);
    return $form;
  }

  public static function get_all($class = null, $tid = null,   $name = null, $isblock = null) {
    $conditions = array();
    //$sql = 'SELECT tid, class, name, title FROM {managed_newsletters_templates}';
    $sql = db_select('managed_newsletters_templates', 'm')
			->fields('m', array('tid', 'class', 'name', 'title'));
    //$where = array();
    if ($class) {
      /*$where[] =  ' class = \'%s\'';
      $conditions[] = $class;*/
		$sql->condition('class',$class);
    }

    if ($tid) {
      /*$where[] =  ' tid = \'%d\'';
      $conditions[] = $tid;*/
		$sql->condition('tid',$tid);
    }

    if ($name) {
      /*$where[] =  ' name = \'%s\'';
      $conditions[] = $name;*/
		$sql->condition('name',$name);
    }

    if ($isblock !== null) {
      /*$where[] =  ' block = \'%s\'';
      $conditions[] = (int) $isblock;*/
		$sql->condition('block', (int) $isblock);
    }

    /*if (!empty($where)) {
      $sql .= ' WHERE ' . implode(' AND ', $where);
    }*/
    // TODO Please convert this statement to the D7 database API syntax.
    //$result = db_query($sql, $conditions);
	$result = $sql->execute();
    $return = array();
    //while ($row = db_fetch_array($result)) {
    while ($row = $result->fetchAssoc()) {
      if (class_exists($row['class'])) {
        $return[$row['tid']] = new $row['class']();
        unset($row['class']);
        foreach ($row as $key => $value) {
          $return[$row['tid']]->$key = $value;
        }
      }
      else {
        // throw exception
      }
    }
    return isset($tid) ? (isset($return[$tid]) ? $return[$tid] : false) : $return;
  }

  public function load() {
	//todo need test
    //$values = db_query('SELECT * FROM {managed_newsletters_templates} WHERE tid = :tid', array(':tid' => $this->tid));
    $values = db_select('managed_newsletters_templates', 'mnt')->condition('tid',$this->tid)->fields('mnt',array())->execute()->fetchAssoc();
    if($values){
	    foreach ($values as $key => $value) {
	    $this->$key = $value;
    }    
    }
  }

  public function validate_values($values) {
    // check that the name is unique
    $templates = managed_newsletter_template::load(null, null, $values['name']);
    if (count($templates) == 1 && !isset($templates[$this->get_tid()])) {
      form_set_error('name', t('Template name is already in use'));
    }
  }

  public function update($values) {
    $this->set_name($values['name']);
    $this->set_title($values['title']);
    $this->set_text($values['text']);
    $this->set_html($values['html']);
  }

  public function save() {
    if ($this->tid) {
      db_update('managed_newsletters_templates')
	  ->fields(array(
			'name' => $this->name,
			'title' => $this->title,
			'html' => $this->html,
			'text' => $this->text,
		  ))
	  ->condition('tid', $this->tid)
	  ->execute();
    }
    else {
      $id = db_insert('managed_newsletters_templates')
	  ->fields(array(
			'tid' => $this->tid,
			'block' => (int) is_a($this, 'managed_newsletter_block'),
			'class' => get_class($this),
			'name' => $this->name,
			'title' => $this->title,
			'html' => $this->html,
			'text' => $this->text,
		  ))
	  ->execute();
      $this->tid = $id;
    }
    menu_rebuild();
  }

  public function delete() {
    db_delete('managed_newsletters_template_blocks')
	  ->condition(db_or()
			->condition('tid', $this->get_tid())
			->condition('tbid', $this->get_tid())
	  )
	  ->execute();
    db_delete('managed_newsletters_templates')
	  ->condition('tid', $this->get_tid())
	  ->execute();
    menu_rebuild();
  }

  public function get_tokens() {
    $tokens = array();
    $tokens['global'] = token_get_info('global');
    $tokens['user'] = token_get_info('user');
    $tokens['managed_newsletter'] = token_get_info('managed_newsletter');
    return $tokens;
  }

  public function get_tokens_table($tokens_list = null) {
    /*$tokens_list = $tokens_list == null ? $this->get_tokens() : $tokens_list;
    $header = array(t('Token'), t('Descriptions'));
    $rows = array();
    foreach ($tokens_list as $type => $tokens_set) {
      foreach ($tokens_set as $type => $tokens) {
        $rows[] = array(array('data' => $type, 'colspan' => 2, 'style' => 'text-align:center;font-weight:bold;'));
        foreach ($tokens as $token => $description) {
          //$rows[] = array(array('data' => '[' . $token . ']'), array('data' => $description));
          $rows[] = array('[' . $token . ']', $description);
        }
      }
    }
    return theme('table', array('header' => $header, 'rows' => $rows));*/
    //return theme('token_tree', array('#token_types' => array('user', 'global', 'managed_newsletter', 'site')));
    return theme('token_tree', array('#token_types' => 'all'));
  }

  public abstract function build_html_content($account);
  public abstract function build_text_content($account);
}
