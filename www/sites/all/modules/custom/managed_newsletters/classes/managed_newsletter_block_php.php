<?php
// $Id$

class managed_newsletter_block_php extends managed_newsletter_block {
  public function build_html_content($account) {
    $objects = array(
		'global' => new stdClass(),
		'user' => $account
		);
    $content = $this->get_html();
	$content = token_replace($content, $objects);
    if (module_exists('php')) {
      $content = php_eval($content);

    }
    //$result = token_replace_multiple($content, $objects);
    $result = token_replace($content, $objects);
    return $result;
  }

  public function build_text_content($account) {
    $objects = array(
		'global' => new stdClass(),
		'user' => $account
		);
    $content = $this->get_text();
	$content = token_replace($content, $objects);
    if (module_exists('php')) {
      $content = php_eval($content);

    }
    //$result = token_replace_multiple($content, $objects);
    $result = token_replace($content, $objects);
    return $result;
  }
}
