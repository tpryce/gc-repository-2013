Plan for #810
=========================================

Load all nodes into the batch queue for processing
Load the current nid and check its legacy nid
Load the current node->body and do a regex search for a tags pointing to node/NID
Save the node / ensure the revision goes live
Move onto next node
Finish batch process after all nodes have been updated


UI
=========================================
Menu: admin/content/migrate/remap
Form: Dropdown with number of nodes to batch 1000 | 2000 | 4000 | 8000 | 16000
Form: Submit button kicks of the process
Watchdog: Writes any fails to watchdog under 'Remap'
Table: Need a table to monitor what nodes have been completed, last action shold be to write the nid to a remap_done table
Table: First action should be to check the remap_done table for the current node.
Form: Shows a counter for total nodes in the system and total nodes in the remap_done table

/node\/[0-9]+(?:\.[0-9]*)?"/


Notes
==========================================

mysql> select count(*) from field_data_body where body_value LIKE '%node/%';
 +----------+
 | count(*) |
 +----------+
 |     6826 |
 +----------+
 1 row in set (3.09 sec)
 
 
Problems
=========================================
How can we handle nodes with multiple inline nid links?

Loop over all nodes = problem if one node has multiple inline links
Loop over each inline link individually = problem keeping track of which inline link has been changed already, especially if two inline links are present. 

eg. node 1234 has two inline links 9876 and 4563 when we loop through and change the first(9876) we need to store the nid it gets changed to and skip it on any further run. i.e mark as done.