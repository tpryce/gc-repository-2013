<?php
// $Id: vma.module,v 1.8 2011/01/10 09:49:55 berdir Exp $

/**
 * @file
 * This module enables you to show different content to different users based on role
 */

function vma_menu() {

  $items['admin/structure/vma'] = array(
    'title' => 'VMA Settings',
    'type' => MENU_NORMAL_ITEM,
    'page callback' => 'drupal_get_form',
    'page arguments' => array('vma_settings'),
    'access arguments' => array('administer site'),
    'file' => 'vma.admin.inc',
  );

  return $items;
}


/**
 * Implements hook_permission().
 */
function vma_permission() {
  return array(
    'administer view mode access' => array(
      'title' => t('Administer view mode access'),
      'description' => t('Configure view mode access'),
    ),
    'bypass view mode change' => array(
      'title' => t('Bypass node view mode change'),
      'description' => t('Bypass view mode node view change'),
    ),
    'role used for view mode access' => array(
      'title' => t('Role to be used for view mode access'),
      'description' => t('Role to be used for view mode access'),
    ),
  );
}

function vma_node_view_alter(&$build) {
  $node = $build['#node'];
    // We don't want the view alter to act on non-node pages, unless someone
    // explicitly wants that (e.g: the vma_field_present_in_vm needs to get the
    // the node as the user would see it, even if the node page is not the
    // requested resource, to check if a field is present in it. This property
    // allows helper functions or any other function calling node_view(), with
    // the property set to TRUE, to bypass the node_is_page(), and enforce the
    // view_mode override that vma is designed to do.
  if (!$node->treat_as_page && !node_is_page($node)) {
    return;
  }
  $is_active = variable_get('vma_' . $node->type . '_enabled',FALSE);

  if ($is_active == 0) {
    return;
  }

  global $user;
  // Set the default user view
  $user_view = 'anonymous';
  $user_data = user_load($user->uid);

  // If the user has the permission to bypass the node view change show the full/default node
  if (user_access('bypass view mode change')) {
    $user_view = 'default';
    $node->original_view_mode = $build['#view_mode'];
    $node->new_build_mode = $user_view;
    // Keep the original links.
    $links = $build['links'];
    node_build_content($node, $user_view);
    $build = $node->content;
    $build['#view_mode'] = $user_view;
    // We don't need duplicate rendering info in node->content.
    unset($node->content);
    $build += array(
        '#theme' => 'node',
        '#node' => $node,
        '#view_mode' => $user_view,
    );
    // Re-add links, keep existing ones.
    if (is_array($links)) {
      $build['links'] += $links;
    }
    return;
  }

  // If the is the author of the current node show the 'default' / 'full' view mode
  if ($node->uid == $user->uid) {
    $user_view = 'default';
    $node->original_view_mode = $build['#view_mode'];
    $node->new_build_mode = $user_view;
    // Keep the original links.
    $links = $build['links'];
    node_build_content($node, $user_view);
    $build = $node->content;
    $build['#view_mode'] = $user_view;
    unset($node->content);
    // Apply default values.
    $build += array(
        '#theme' => 'node',
        '#node' => $node,
        '#view_mode' => $user_view,
    );
    // Re-add links, keep existing ones.
    if (is_array($links)) {
      $build['links'] += $links;
    }
    return;
  }

  // If the user doesn't have an account / is anonymous show the 'anonymous' view mode
  if (!$user->uid) {
    $node->original_view_mode = $build['#view_mode'];
    $node->new_build_mode = $user_view;
    // Keep the original links.
    $links = $build['links'];
    node_build_content($node, $user_view);
    $build = $node->content;
    //unset($node->content);
    $build += array(
        '#theme' => 'node',
        '#node' => $node,
        '#view_mode' => $user_view,
    );
    // Re-add links, keep existing ones.
    if (is_array($links)) {
      $build['links'] += $links;
    }
    return;
  }

  $roles = $user->roles;
  $key = array_search('authenticated user', $roles);
  if ($key !== false) {
    unset($roles[$key]);
  }
  foreach ($roles as $role) {
    $role = str_replace(' ','_',$role);

    //TODO
    /*
    if ($user_data->field_purchased_articles != NULL) {
      foreach ($user_data->field_purchased_articles['und'] as $target) {
        if ($target['target_id'] == $node->nid) {
          $user_view = variable_get('vma_' . $node->type . '_' . $role . '_view','anonymous');
          $node->original_view_mode = $build['#view_mode'];
          $node->new_build_mode = $user_view;
          // Keep the original links.
          $links = $build['links'];
          node_build_content($node, $user_view);
          $build = $node->content;
          unset($node->content);
          return;
        }
      }
    }
    */

    // TODO
    /*
    if ($node->field_subscription_level) {
      foreach ($node->field_subscription_level['und'] as $roleexception) {
        if (in_array($roleexception['taxonomy_term']->name, $user->roles, FALSE)) {
          $user_view = variable_get('vma_' . $node->type . '_' . $role . '_view','anonymous');
        }
      }
      //print_r('<p>Your Role is ' . $role . ' and your view mode is ' . $user_view) . '</p>';
      $node->original_view_mode = $build['#view_mode'];
      $node->new_build_mode = $user_view;
      // Keep the original links.
      $links = $build['links'];
      node_build_content($node, $user_view);
      $build = $node->content;
      unset($node->content);
      return;
    }
    */

    // Get all taxonomy term names that have been saved in our VMA settings form.
    // For now only Geography is actually needed.
    // TODO: See admin form, one day we should have active vocab settings.
    // Support for the Domain module.
    if (module_exists('domain')) {
      // Get active domain.
      $domain = domain_get_domain();
      $vma_tax_map = variable_get($role . 'geography' . $domain['machine_name'], '');
    }
    else {
      $vma_tax_map = variable_get($role . 'geography', '');
    }

    //Get all taxonomy terms associated with the current node
    $vma_node_terms = vma_node_get_terms($node);
    foreach ($vma_node_terms as $vmaterm) {
      // Set the view mode if any of our terms match
      if (array_search($vmaterm->name,$vma_tax_map) !== FALSE) {
        $user_view = variable_get('vma_' . $node->type . '_' . $role . '_view','anonymous');
        $match_found = TRUE;
      }
    }
    $node->original_view_mode = $build['#view_mode'];
    $node->new_build_mode = $user_view;
    
    if($match_found == TRUE) {
      break;
    }
  }

  // Keep the original links.
  $links = $build['links'];
  node_build_content($node, $user_view);
  $build = $node->content;
  unset($node->content);
  $build += array(
      '#theme' => 'node',
      '#node' => $node,
      '#view_mode' => $user_view,
  );
  // Re-add links, keep existing ones.
  if (is_array($links)) {
    $build['links'] += $links;
  }
}

function vma_node_get_terms($node, $key = 'tid') {
  $terms = &drupal_static(__FUNCTION__);

  if (!isset($terms[$node->nid][$key])) {
    // Get tids from all taxonomy_term_reference fields.
    $tids = array();
    $fields = field_info_fields();
    foreach ($fields as $field_name => $field) {
      if ($field['type'] == 'taxonomy_term_reference' && field_info_instance('node', $field_name, $node->type)) {
        if (($items = field_get_items('node', $node, $field_name)) && is_array($items)) {
          $tids = array_merge($tids, array_map('_taxonomy_extract_tid', $items));
        }
      }
    }

    // Load terms and cache them in static var.
    $curr_terms = taxonomy_term_load_multiple($tids);
    $terms[$node->nid][$key] = array();
    foreach ($curr_terms as $term) {
      $terms[$node->nid][$key][$term->$key] = $term;
    }
  }
  return $terms[$node->nid][$key];
}

function _taxonomy_extract_tid($item) {
  return $item['tid'];
}

function vma_node_view($node, $view_mode, $langcode) {
  // Append comments when we are building a node on its own node detail
  // page.
  if ($node->comment && node_is_page($node) && empty($node->in_preview)) {
  $node->content['comments'] = comment_node_page_additions($node);
  }
}

/**
 * Preprocess function to add classes to the node.
 */
function vma_preprocess_node(&$variables) {
  global $user;

  $node = $variables['elements']['#node'];

  if (node_is_page($node)) {
    $variables['page'] = 1;
  }

  // If the original view mode was teaser, add the node-teaser class to
  // keep the default theming of nodes in the teaser list.
  if (!empty($variables['original_view_mode']) && $variables['original_view_mode'] == 'teaser') {
    $variables['classes_array'][] = 'node-teaser';
  }
}


function vma_form_node_type_form_alter(&$form, &$form_state) {
	// If we dont have permission to administer the title then we need to abort this alter now!
	if (!user_access('administer view mode access')) return;

	$userroles = user_roles(FALSE,'role used for view mode access');
	$entinfo = entity_get_info('node');
	$entinfo = array_keys($entinfo['view modes']);
	foreach ($entinfo as $mode) {
		$options[$mode] = $mode;
	}

	$form['vma'] = array(
    '#type' => 'fieldset',
    '#title' => t('View Mode Access'),
    '#collapsible' => TRUE,
    '#group' => 'additional_settings',
  );
  $form['vma']['vma_' . $form['#node_type']->type . '_enabled'] = array(
  '#type' => 'checkbox',
  '#title' => t('Enabled'),
  '#default_value' => variable_get('vma_' . $form['#node_type']->type . '_enabled', 0),
  '#description' => t('If checked, access view modes will be active'),
  );
  foreach ($userroles as $role) {
	  $role = str_replace(' ','_',$role);
	  	//dpr(variable_get('vma_' . $form['#node_type']->type . '_' . $role . '_view','notset'));
		  $form['vma']['vma_' . $form['#node_type']->type . '_' . $role . '_view'] = array(
		    '#title' => $role,
		    '#type' => 'select',
		    '#required' => FALSE,
		    '#options' => $options,
		    '#default_value' => variable_get('vma_' . $form['#node_type']->type . '_' . $role . '_view','notset'),
		  );
  }
	$form['#submit'][] = 'vma_node_type_form_submit';
}

function vma_node_type_form_submit($form, &$form_state) {
	variable_set('vma_' . $form['#node_type']->type . '_enabled',$form_state['values']['vma_' . $form['#node_type']->type . '_enabled']);
	$userroles = user_roles(FALSE,'role used for view mode access');
	foreach ($userroles as $role) {
		$role = str_replace(' ','_',$role);
		variable_set('vma_' . $form['#node_type']->type . '_' . $role . '_view',$form_state['values']['vma_' . $form['#node_type']->type . '_' . $role . '_view']);
	}
}

/**
 * Implements hook_entity_info().
 */
function vma_entity_info() {
	$userroles = user_roles(FALSE,'role used for view mode access');
  /* 	if (empty($userroles)) {return;} */
	foreach ($userroles as $role) {
		$roleshort = str_replace(' ','_',$role);
		$return['node']['view modes'][$roleshort] = array(
      'label' => $role . t(' View Mode'),
      'custom settings' => FALSE,
    );
	}
	$return['node']['view modes']['subscription'] = array(
      'label' => t('Subscription View Mode'),
      'custom settings' => TRUE,
    );
  $return['node']['view modes']['anonymous'] = array(
      'label' => t('Anonymous View Mode'),
      'custom settings' => TRUE,
  );
  return $return;
}

/**
 * Checks whether a specific field is included in the view mode that the actual
 * user would see when going to the page of the node specified in $node_id.
 *
 * @param $field_id A string with the machine name of the field asked for.
 * @param $node_id ID of the node for which we need to know if the field is
 *  present.
 * @return bool TRUE, if the field is present in the view_mode the user would
 *  see, FALSE otherwise.
 */
function vma_field_present_in_vm($field_id, $node_id) {
  $node = node_load($node_id);
  // mark the node as if we were on the node_page. This allows the
  // vma_node_view_alter() to do what it would do when viewing a node, which is
  // what we need in order to check if the field_id is present on the view mode.
  $node->treat_as_page = TRUE;
  $node_render_array = node_view($node);

  if (isset($node_render_array[$field_id]) && !empty($node_render_array[$field_id])) {
    return TRUE;
  }
  else {
    return FALSE;
  }
}

/**
 * Implements hook_field_display_ENTITY_TYPE_alter().
 */
function vma_field_display_node_alter(&$display, $context) {
  // Taxonomy term links normally link to Spotlight pages, this is undesirable
  // for the Data domain so we render them as plain text.

  // Bail out if we're not processing a taxonomy term reference field.
  if ('taxonomy_term_reference' !== $context['field']['type']) {
    return;
  }

  // Bail out if the display type is not a link.
  if ('taxonomy_term_reference_link' !== $display['type']) {
    return;
  }

  // Bail out if we're not on the gambling data domain, i.e. if the current
  // active domain is the default.
  $current_active_domain = domain_get_domain();
  if ($current_active_domain['is_default']) {
    return;
  }

  // Finally, change any term reference links to render as plain text.
  $display['type'] = 'taxonomy_term_reference_plain';
}
