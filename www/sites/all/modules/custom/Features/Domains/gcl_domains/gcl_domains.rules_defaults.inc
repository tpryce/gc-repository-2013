<?php
/**
 * @file
 * gcl_domains.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function gcl_domains_default_rules_configuration() {
  $items = array();
  $items['rules_update_node_domain'] = entity_import('rules_config', '{ "rules_update_node_domain" : {
      "LABEL" : "Update node To Compliance Source",
      "PLUGIN" : "action set",
      "TAGS" : [ "Domain" ],
      "REQUIRES" : [ "domain_rules" ],
      "USES VARIABLES" : { "node" : { "label" : "Node", "type" : "node" } },
      "ACTION SET" : [
        { "domain_rules_action_set_node_domain" : {
            "subdomain" : "gambling-compliance-d7.stage.stage1.codeenigma.com",
            "node" : [ "node" ]
          }
        }
      ]
    }
  }');
  $items['rules_update_node_to_data_source_cloned_'] = entity_import('rules_config', '{ "rules_update_node_to_data_source_cloned_" : {
      "LABEL" : "Update node To Data Source (cloned)",
      "PLUGIN" : "action set",
      "TAGS" : [ "Domain" ],
      "REQUIRES" : [ "domain_rules" ],
      "USES VARIABLES" : { "node" : { "label" : "Node", "type" : "node" } },
      "ACTION SET" : [
        { "domain_rules_action_set_node_domain" : {
            "subdomain" : "gambling-data-new.stage.stage1.codeenigma.com",
            "node" : [ "node" ]
          }
        }
      ]
    }
  }');
  return $items;
}
