<?php
/**
 * Admin form.
 *
 * @param array $form
 * @param array $form_state
 */
function vma_settings($form, &$form_state) {
  $roles = user_roles(TRUE,'role used for view mode access');
  if(empty($roles)){
    drupal_set_message(t('There are currently no roles activated for VMA.  Please go to the role page and give a role "role used for view mode access"'),'notice');
    return;
  }
  if (module_exists('domain')) {
    // Load Domain module available domains.
    $domains = domain_domains();
  }
  foreach($roles as $role){
    $roleshort = str_replace(' ','_',$role);
    $form[$roleshort] = array(
      '#type' => 'fieldset',
      '#title' => $role,
      '#weight' => 5,
      '#collapsible' => FALSE,
      '#description' => t('Enter taxonomy terms this user role is permitted to view.'),
    );
    // TODO: One day this really ought to have an admin panel to select relevant vocabularies and the vocab name should be loaded via the API.
    // If the Domain module is enabled, we need a settings box per vocab, PER DOMAIN.
    if (module_exists('domain')) {
      foreach ($domains as $domain) {
        $form[$roleshort][$roleshort . 'geography' . $domain['machine_name']] = array(
          '#type' => 'textarea',
          '#title' => $domain['sitename'] . ': ' . t('Geography'),
          '#default_value' => implode(', ', variable_get($roleshort . 'geography' . $domain['machine_name'], '')),
          '#weight' => -5,
          '#description' => t('A comma-separated list of taxonomy terms allowed for this role on the %domain domain.', array('%domain' => $domain['sitename'])),
        );
      }
    }
    else {
      $form[$roleshort][$roleshort .'geography'] = array(
        '#type' => 'textarea',
        '#title' => t('Geography'),
        '#default_value' => implode(', ', variable_get($roleshort . 'geography','')),
        '#weight' => -5,
        '#description' => t('A comma-separated list of taxonomy terms allowed for this role.'),
      );
    }
    // Commented out for now, as not actually used on node alter.
    //$form[$roleshort][$roleshort . 'subscription_role'] = array(
    //    '#type' => 'textarea',
    //    '#title' => t('Subscription Role'),
    //    '#default_value' => implode(', ', variable_get($roleshort . 'subscription_role','')),
    //    '#weight' => -5,
    //);
    //$form[$roleshort][$roleshort . 'content'] = array(
    //    '#type' => 'textarea',
    //    '#title' => t('Content'),
    //    '#default_value' => implode(', ', variable_get($roleshort . 'content','')),
    //    '#weight' => -5,
    //);
    $form['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Save'),
        '#weight' => 10,
    );
  }
  return $form;
}

/**
 * Implements hook_submit().
 *
 * @param array $form
 * @param array $form_state
 *
 * Explores the comma-separated string of terms into an array
 * and saves as a Drupal variable.
 */
function vma_settings_submit($form, &$form_state) {
  // Define vocabs we want to store (should match form).
  $vocabs = array(
    'geography',
    // Commented out for now, as not actually used on node alter.
    //'subscription_role',
    //'content',
  );

  // Loop through available role groups.
  foreach ($form_state['groups'] as $groupname => $group) {
    // Each group has a vocab field of its own, so loop through them.
    foreach ($vocabs as $vocab) {
      $varname = $groupname . $vocab;
      // Support for per-domain settings using the Domain module.
      if (module_exists('domain')) {
        // Load Domain module available domains.
        $domains = domain_domains();
        // Loop through and append domain machine name to variable name.
        foreach ($domains as $domain) {
          $domainvarname = $varname . $domain['machine_name'];
          // Check we have data to process.
          if (!empty($form_state['values'][$domainvarname])) {
            // Explode the comma-separated string into an array for storage.
            $vma_tax_map = explode(',', $form_state['values'][$domainvarname]);
            foreach ($vma_tax_map as $key => $term) {
              // Trim any erroneous white space from array values.
              $vma_tax_map[$key] = trim($term);
            }
            // Save.
            variable_set($domainvarname, $vma_tax_map);
          }
        }
      }
      else {
        // Check we have data to process.
        if (!empty($form_state['values'][$varname])) {
          // Explode the comma-separated string into an array for storage.
          $vma_tax_map = explode(',', $form_state['values'][$varname]);
          foreach ($vma_tax_map as $key => $term) {
            // Trim any erroneous white space from array values.
            $vma_tax_map[$key] = trim($term);
          }
          // Save.
          variable_set($varname, $vma_tax_map);
        }
      }
    }
  }
  drupal_set_message(t('Settings have been saved.'));
}
