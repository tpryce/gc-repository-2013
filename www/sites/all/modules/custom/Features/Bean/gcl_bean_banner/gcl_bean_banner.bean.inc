<?php
/**
 * @file
 * gcl_bean_banner.bean.inc
 */

/**
 * Implements hook_bean_admin_ui_types().
 */
function gcl_bean_banner_bean_admin_ui_types() {
  $export = array();

  $bean_type = new stdClass();
  $bean_type->disabled = FALSE; /* Edit this to true to make a default bean_type disabled initially */
  $bean_type->api_version = 5;
  $bean_type->name = 'banner';
  $bean_type->label = 'Banner';
  $bean_type->options = '';
  $bean_type->description = 'A large promotional image encouraging the user to click through to a promoted article / page.';
  $export['banner'] = $bean_type;

  $bean_type = new stdClass();
  $bean_type->disabled = FALSE; /* Edit this to true to make a default bean_type disabled initially */
  $bean_type->api_version = 5;
  $bean_type->name = 'feature';
  $bean_type->label = 'Feature';
  $bean_type->options = '';
  $bean_type->description = '';
  $export['feature'] = $bean_type;

  $bean_type = new stdClass();
  $bean_type->disabled = FALSE; /* Edit this to true to make a default bean_type disabled initially */
  $bean_type->api_version = 5;
  $bean_type->name = 'promo';
  $bean_type->label = 'Promo';
  $bean_type->options = '';
  $bean_type->description = 'A promo block, containing simply an image that links to a piece of content or external website.';
  $export['promo'] = $bean_type;

  return $export;
}
