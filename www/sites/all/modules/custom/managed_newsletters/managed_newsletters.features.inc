<?php

function managed_newsletters_maillists_features_export_options()
{
	$result = db_select('managed_newsletters_maillists', 'm')->fields('m', array('lid', 'title'))->execute();
	//watchdog('debug', 'dfd ', array());
	foreach ($result as $row)
	{
		$options['maillists-' . $row->lid] =  'maillists-' . $row->lid . '-' . $row->title;
	}
	return $options;
}

function managed_newsletters_templates_features_export_options()
{
	$result = db_select('managed_newsletters_templates', 't')->fields('t', array('tid', 'name'))->execute();
	foreach ($result as $row)
	{
		$options['templates-' . $row->tid] =  'templates-' . $row->name;
	}
	return $options;
}

function managed_newsletters_blocks_features_export_options()
{
	$result = db_select('managed_newsletters_block_node_settings', 'bs')->fields('bs', array('tsid', 'name', 'tid'))->execute();
	foreach ($result as $row)
	{
		$options['block-settings-' . $row->tsid] =  'block-settings-' . $row->tid . '-' . $row->name;
	}

	return $options;
}

function managed_newsletters_maillists_features_export($data, &$export, $module_name)
{
	$export['dependencies']['managed_newsletters_maillists'] = 'managed_newsletters';
	foreach ($data as $component)
	{
		$export['features']['managed_newsletters_maillists'][$component] = $component;
	}
	return array();
}

function managed_newsletters_maillists_settings_features_export($data, &$export, $module_name)
{
	$export['dependencies']['managed_newsletters_maillists_settings'] = 'managed_newsletters';
	foreach ($data as $component)
	{
		$export['features']['managed_newsletters_maillists_settings'][$component] = $component;
	}
	return array();
}

function managed_newsletters_templates_features_export($data, &$export, $module_name)
{
	$export['dependencies']['managed_newsletters_templates'] = 'managed_newsletters';
	foreach ($data as $component)
	{
		$export['features']['managed_newsletters_templates'][$component] = $component;
	}
	return array();
}

function managed_newsletters_templates_blocks_features_export($data, &$export, $module_name)
{
	$export['dependencies']['managed_newsletters_templates_blocks'] = 'managed_newsletters';
	foreach ($data as $component)
	{
		$export['features']['managed_newsletters_templates_blocks'][$component] = $component;
	}
	return array();
}

function managed_newsletters_blocks_features_export($data, &$export, $module_name)
{
	$export['dependencies']['managed_newsletters_blocks'] = 'managed_newsletters';
	foreach ($data as $component)
	{
		$export['features']['managed_newsletters_blocks'][$component] = $component;
	}
	return array();
}

function managed_newsletters_maillists_features_export_render($module_name, $data, $export = NULL)
{
	//create maillists
	$maillists_code = array();
	$maillists_code[] = '  $managed_newsletters_maillists = array();';
	$maillists_code[] = '';
	$settings_code = array();
	$settings_code[] =  '  $managed_newsletters_maillists_settings = array();';
	$settings_code[] = '';
	//foreach ($items['maillists'] as $sys_name)
	foreach ($data as $sys_name)
	{
		$names = explode('-', $sys_name);
		$id = array_pop($names);
		$item = _managed_newsletters_get_data_maillists($id);
		$maillists_code[] = '  $managed_newsletters_maillists[] = '. features_var_export($item, '  ') .';';
		$item_settings = _managed_newsletters_get_data_maillists_settings($id);
		$settings_code[] = '  $managed_newsletters_maillists_settings[] = '. features_var_export($item_settings, '  ') .';';
	}
	$maillists_code[] = '  return $managed_newsletters_maillists;';
	$maillists_code = implode("\n", $maillists_code);
	$settings_code[] = '  return $managed_newsletters_maillists_settings;';
	$settings_code = implode("\n", $settings_code);
	return array('managed_newsletters_maillists' => $maillists_code, 'managed_newsletters_maillists_settings' => $settings_code);
}

function managed_newsletters_templates_features_export_render($module_name, $data, $export = NULL)
{
	//createtemplates
	$templates_code = array();
	$templates_code[] = '  $managed_newsletters_templates = array();';
	$templates_code[] = '';
	$settings_code = array();
	$settings_code[] =  '  $managed_newsletters_templates_blocks = array();';
	$settings_code[] = '';
	//foreach ($items['templates'] as $sys_name)
	foreach ($data as $sys_name)
	{
		$names = explode('-', $sys_name);
		$id = array_pop($names);
		$item = _managed_newsletters_get_data_templates($id);
		$templates_code[] = '  $managed_newsletters_templates[] = '. features_var_export($item, '  ') .';';
		$item_blocks = _managed_newsletters_get_data_templates_blocks($id);
		$settings_code[] = '  $managed_newsletters_templates_blocks[] = '. features_var_export($item_blocks, '  ') .';';
	}
	$templates_code[] = '  return $managed_newsletters_templates;';
	$templates_code = implode("\n", $templates_code);
	$settings_code[] = '  return $managed_newsletters_templates_blocks;';
	$settings_code = implode("\n", $settings_code);
	return array('managed_newsletters_templates' => $templates_code, 'managed_newsletters_templates_settings' => $settings_code);
}

function managed_newsletters_blocks_features_export_render($module_name, $data, $export = NULL)
{
	$items[] = array();

	//create block-settings
	$block_code = array();
	$block_code[] = '  $managed_newsletters_blocks = array();';
	$block_code[] = '';
	//foreach ($items['block-settings'] as $sys_name)
	foreach ($data as $sys_name)
	{
		$names = explode('-', $sys_name);
		$id = array_pop($names);
		$item = _managed_newsletters_get_data_block_settings($id);
		$block_code[] = '  $managed_newsletters_blocks[] = '. features_var_export($item, '  ') .';';
	}
	$block_code[] = '  return $managed_newsletters_blocks;';
	$block_code = implode("\n", $block_code);

	return array('managed_newsletters_block_settings' => $block_code);
}

function managed_newsletters_maillists_features_rebuild($module)
{
	$items = module_invoke($module, 'managed_newsletters_maillists');
	foreach ($items as $item)
	{
		$saved = _managed_newsletters_set_data_maillists($item);
	}
	$items = module_invoke($module, 'managed_newsletters_maillists_settings');
	foreach ($items as $item)
	{
		$saved = _managed_newsletters_set_data_maillists_settings($item);
	}
}


function managed_newsletters_templates_features_rebuild($module)
{
	$items = module_invoke($module, 'managed_newsletters_templates');
	foreach ($items as $item)
	{
		$saved = _managed_newsletters_set_data_templates($item);
	}

	$items = module_invoke($module, 'managed_newsletters_templates_settings');
	foreach ($items as $item)
	{
		$saved = _managed_newsletters_set_data_templates_blocks($item);
	}
}

function managed_newsletters_blocks_features_rebuild($module)
{
	$items = module_invoke($module, 'managed_newsletters_block_settings');
	foreach ($items as $item)
	{
		$saved = _managed_newsletters_set_data_block_settings($item);
	}
}

function managed_newsletters_maillists_features_revert($module)
{
	managed_newsletters_maillists_features_rebuild($module);
}

function managed_newsletters_maillists_settings_features_revert($module)
{
	managed_newsletters_maillists_settings_features_rebuild($module);
}

function managed_newsletters_templates_features_revert($module)
{
	managed_newsletters_templates_features_rebuild($module);
}

function managed_newsletters_templates_blocks_features_revert($module)
{
	managed_newsletters_templates_blocks_features_rebuild($module);
}

function managed_newsletters_blocks_features_revert($module)
{
	managed_newsletters_blocks_features_rebuild($module);
}

function _managed_newsletters_get_data_block_settings($tsid)
{
	$data = array();
	$result = db_select('managed_newsletters_block_node_settings', 'bs')
			->fields('bs', array('tsid', 'tid', 'name', 'value'))
			->condition('tsid', $tsid)
			->orderBy('tsid')
			->execute();
	foreach ($result as $row)
	{
		$data['tsid'] = $row->tsid;
		$data['tid'] = $row->tid;
		$data['name'] = $row->name;
		$data['value'] = $row->value;
	}
	return $data;
}

function _managed_newsletters_get_data_maillists($lid)
{
	$data = array();
	$result = db_select('managed_newsletters_maillists', 'm')
			->fields('m', array('lid', 'class', 'title'))
			->condition('lid', $lid)
			->orderBy('lid')
			->execute();
	foreach ($result as $row)
	{
		$data['lid'] = $row->lid;
		$data['class'] = $row->class;
		$data['title'] = $row->title;
	}
	return $data;
}

function _managed_newsletters_get_data_maillists_settings($lid)
{
	$data = array();
	$result = db_select('managed_newsletters_maillists_settings', 'ms')
			->fields('ms', array('lsid', 'lid', 'name', 'value'))
			->condition('lid', $lid)
			->orderBy('lid')
			->execute();
	foreach ($result as $row)
	{
		$data['lsid'] = $row->lsid;
		$data['lid'] = $row->lid;
		$data['name'] = $row->name;
		$data['value'] = $row->value;
	}
	return $data;
}

function _managed_newsletters_get_data_templates($tid)
{
	$data = array();
	$result = db_select('managed_newsletters_templates', 't')
			->fields('t', array('tid', 'block', 'class', 'name', 'title', 'html', 'text'))
			->condition('tid', $tid)
			->orderBy('tid')
			->execute();
	foreach ($result as $row)
	{
		$data['tid'] = $row->tid;
		$data['block'] = $row->block;
		$data['class'] = $row->class;
		$data['name'] = $row->name;
		$data['title'] = $row->title;
		$data['html'] = $row->html;
		$data['text'] = $row->text;
	}
	return $data;
}

function _managed_newsletters_get_data_templates_blocks($tid)
{
	$data = array();
	$result = db_select('managed_newsletters_template_blocks', 'tb')
			->fields('tb', array('tbid', 'tid', 'bid'))
			->condition('tid', $tid)
			->orderBy('tid')
			->execute();

	$blocks = array();
	foreach ($result as $row)
	{
		$data['tbid'] = $row->tbid;
		$data['tid'] = $row->tid;
		$data['bid'] = $row->bid;
		$blocks[] = $data;
	}
	return $blocks;
}

function _managed_newsletters_set_data_maillists($items){
	db_delete('managed_newsletters_maillists')
	->condition('lid', $items['lid'])
	->execute();

	db_insert('managed_newsletters_maillists')
	->fields(array('lid'=>$items['lid'],
					'class'=>$items['class'],
					'title'=>$items['title'],
				))
	->execute();
}

function _managed_newsletters_set_data_maillists_settings($items){
	db_delete('managed_newsletters_maillists_settings')
	->condition('lid', $items['lid'])
	->execute();

	db_insert('managed_newsletters_maillists_settings')
	->fields(array('lsid'=>$items['lsid'],
					'lid'=>$items['lid'],
					'name'=>$items['name'],
					'value'=>$items['value'],
				))
	->execute();
}

function _managed_newsletters_set_data_templates($items){
	db_delete('managed_newsletters_templates')
	->condition('tid', $items['tid'])
	->execute();

	db_insert('managed_newsletters_templates')
	->fields(array('tid'=>$items['tid'],
					'block'=>$items['block'],
					'class'=>$items['class'],
					'name'=>$items['name'],
					'title'=>$items['title'],
					'html'=>$items['html'],
					'text'=>$items['text'],
				))
	->execute();
}

function _managed_newsletters_set_data_templates_blocks($items){
	if (count($items)>0){
		db_delete('managed_newsletters_template_blocks')
			->condition('tid', $items['0']['tid'])
			->execute();
		foreach ($items as $item)
		{
			db_insert('managed_newsletters_template_blocks')
			->fields(array('tbid'=>$item['tbid'],
							'tid'=>$item['tid'],
							'bid'=>$item['bid'],
						))
			->execute();
		}
	}
}

function _managed_newsletters_set_data_block_settings($items){
	db_delete('managed_newsletters_block_node_settings')
	->condition('tsid', $items['tsid'])
	->execute();

	db_insert('managed_newsletters_block_node_settings')
	->fields(array('tsid'=>$items['tsid'],
					'tid'=>$items['tid'],
					'name'=>$items['name'],
					'value'=>$items['value'],
				))
	->execute();
}