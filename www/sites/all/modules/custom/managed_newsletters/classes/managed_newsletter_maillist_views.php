<?php
// $Id$

class managed_newsletter_maillist_views extends managed_newsletter_maillist {
  private $view;

  public function get_view() {
    return $this->view;
  }

  public function set_view($value) {
    $this->view = $value;
  }

  public function update($values) {
    parent::update($values);
    $this->set_view($values['view']);
  }

  public function save() {
    parent::save();
    // TODO Please review the conversion of this statement to the D7 database API syntax.
    /* db_query('DELETE FROM {managed_newsletters_maillists_settings} WHERE lid = %d', $this->get_lid()) */
    db_delete('managed_newsletters_maillists_settings')
	  ->condition('lid', $this->get_lid())
	  ->execute();
    db_insert('managed_newsletters_maillists_settings')
		  ->fields(array('lid' => $this->get_lid(),
						'name' => 'view',
						'value' => $this->get_view()
					))
			->execute();
  }

  public function load() {
    parent::load();
    $result = db_query('SELECT * FROM {managed_newsletters_maillists_settings} WHERE lid = :lid', array(':lid' => $this->get_lid()));
    //while ($row = db_fetch_array($result)) {
    while ($row = $result->fetchAssoc()) {
      $this->{$row['name']} = $row['value'];
    }
  }

  public function delete() {
    // TODO Please review the conversion of this statement to the D7 database API syntax.
    /* db_query('DELETE FROM {managed_newsletters_maillists_settings} WHERE lid = %d', $this->get_lid()) */
    db_delete('managed_newsletters_maillists_settings')
	  ->condition('lid', $this->get_lid())
	  ->execute();
    parent::delete();
  }

  public function get_edit_form() {
    $form = parent::get_edit_form();
    $views = array();

    foreach (views_get_all_views() as $key => $view) {
      if ($view->base_table == 'users') {
        $views[$view->name] = $view->name;
      }
    }
    $form['view'] = array(
			'#type' => 'select',
			'#title' => t('View'),
			'#description' => t('Select a view with list of users for the maillist. The view must contain user records with 2 fields: user uid and mail. Create new view !link ', array('!link' => l(t('here'), 'admin/structure/views'))),
			'#options' => $views,
			'#required' => true,
			'#default_value' => $this->get_view()
		);
    return $form;
  }

  function get_recipients() {
    $this->load();
    $recipients = array();
    if ($view = views_get_view($this->get_view())) {
      unset($view->pager['items_per_page']);

	  $view->execute();
      foreach ($view->result as $key => $value) {
        $recipients[$value->users_mail] = $value->uid;
      }
    }
    return $recipients;
  }
}
