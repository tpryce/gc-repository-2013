<?php
/**
 * @file
 * gcl_context.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function gcl_context_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'about_us';
  $context->description = 'A context for the about us page.';
  $context->tag = 'content';
  $context->conditions = array(
    'menu' => array(
      'values' => array(
        'node/57662' => 'node/57662',
        'node/57694' => 'node/57694',
        'node/57711' => 'node/57711',
        'node/57715' => 'node/57715',
        'press-releases' => 'press-releases',
      ),
    ),
    'path' => array(
      'values' => array(
        'about-us' => 'about-us',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'system-main' => array(
          'module' => 'system',
          'delta' => 'main',
          'region' => 'content',
          'weight' => '-10',
        ),
        'menu-menu-about-us' => array(
          'module' => 'menu',
          'delta' => 'menu-about-us',
          'region' => 'sidebar_first',
          'weight' => '-10',
        ),
        'bean-promo-one' => array(
          'module' => 'bean',
          'delta' => 'promo-one',
          'region' => 'sidebar_second',
          'weight' => '-42',
        ),
        'views-topics-block' => array(
          'module' => 'views',
          'delta' => 'topics-block',
          'region' => 'sidebar_second',
          'weight' => '-41',
        ),
        'views-events-block_1' => array(
          'module' => 'views',
          'delta' => 'events-block_1',
          'region' => 'sidebar_second',
          'weight' => '-40',
        ),
      ),
    ),
    'delta' => array(
      'delta_template' => 'about_us',
    ),
    'theme_html' => array(
      'class' => 'gclaboutus',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('A context for the about us page.');
  t('content');
  $export['about_us'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'data_endnode';
  $context->description = 'Block structure when a user is viewing end nodes';
  $context->tag = 'content';
  $context->conditions = array(
    'domain' => array(
      'values' => array(
        2 => 2,
      ),
    ),
    'node' => array(
      'values' => array(
        'analyst_view' => 'analyst_view',
        'chart' => 'chart',
        'data' => 'data',
        'data_report' => 'data_report',
        'market_analysis' => 'market_analysis',
        'press_release' => 'press_release',
        'product' => 'product',
        'regulatory_news' => 'regulatory_news',
        'regulatory_reports' => 'regulatory_reports',
        'white_paper' => 'white_paper',
      ),
      'options' => array(
        'node_form' => '0',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'gcl_node_actions-node_actions' => array(
          'module' => 'gcl_node_actions',
          'delta' => 'node_actions',
          'region' => 'sidebar_second',
          'weight' => '-50',
        ),
        'tableofcontents_block-tableofcontents_block' => array(
          'module' => 'tableofcontents_block',
          'delta' => 'tableofcontents_block',
          'region' => 'sidebar_second',
          'weight' => '-49',
        ),
        'apachesolr_search-mlt-002' => array(
          'module' => 'apachesolr_search',
          'delta' => 'mlt-002',
          'region' => 'sidebar_second',
          'weight' => '-48',
        ),
        'apachesolr_search-mlt-003' => array(
          'module' => 'apachesolr_search',
          'delta' => 'mlt-003',
          'region' => 'sidebar_second',
          'weight' => '-47',
        ),
      ),
    ),
    'delta' => array(
      'delta_template' => 'dataarticle',
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('Block structure when a user is viewing end nodes');
  t('content');
  $export['data_endnode'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'data_frontpage';
  $context->description = '';
  $context->tag = 'anonymous';
  $context->conditions = array(
    'domain' => array(
      'values' => array(
        2 => 2,
      ),
    ),
    'path' => array(
      'values' => array(
        '<front>' => '<front>',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-hero_promotion-block' => array(
          'module' => 'views',
          'delta' => 'hero_promotion-block',
          'region' => 'content',
          'weight' => '-10',
        ),
        'system-main' => array(
          'module' => 'system',
          'delta' => 'main',
          'region' => 'content',
          'weight' => '0',
        ),
        'views-charts-block' => array(
          'module' => 'views',
          'delta' => 'charts-block',
          'region' => 'content',
          'weight' => '37',
        ),
        'views-charts-block_1' => array(
          'module' => 'views',
          'delta' => 'charts-block_1',
          'region' => 'content',
          'weight' => '38',
        ),
        'bean-promo-one' => array(
          'module' => 'bean',
          'delta' => 'promo-one',
          'region' => 'sidebar_second',
          'weight' => '-54',
        ),
        'views-data-block' => array(
          'module' => 'views',
          'delta' => 'data-block',
          'region' => 'sidebar_second',
          'weight' => '-53',
        ),
      ),
    ),
    'delta' => array(
      'delta_template' => 'datadefault',
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('anonymous');
  $export['data_frontpage'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'data_in_depth';
  $context->description = 'The data in-depth page block layout';
  $context->tag = 'Data';
  $context->conditions = array(
    'domain' => array(
      'values' => array(
        2 => 2,
      ),
    ),
    'views' => array(
      'values' => array(
        'data:page_2' => 'data:page_2',
        'data:page_4' => 'data:page_4',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'bean-promo-one' => array(
          'module' => 'bean',
          'delta' => 'promo-one',
          'region' => 'sidebar_second',
          'weight' => '-55',
        ),
        'bean-promo-two' => array(
          'module' => 'bean',
          'delta' => 'promo-two',
          'region' => 'sidebar_second',
          'weight' => '-54',
        ),
        'views-data-block_1' => array(
          'module' => 'views',
          'delta' => 'data-block_1',
          'region' => 'sidebar_second',
          'weight' => '-53',
        ),
        'menu-menu-sector-menu' => array(
          'module' => 'menu',
          'delta' => 'menu-sector-menu',
          'region' => 'sidebar_second',
          'weight' => '-52',
        ),
      ),
    ),
    'delta' => array(
      'delta_template' => 'datadefault',
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('Data');
  t('The data in-depth page block layout');
  $export['data_in_depth'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'data_search_results';
  $context->description = '';
  $context->tag = 'search';
  $context->conditions = array(
    'domain' => array(
      'values' => array(
        2 => 2,
      ),
    ),
    'path' => array(
      'values' => array(
        'search/*' => 'search/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'apachesolr_search-sort' => array(
          'module' => 'apachesolr_search',
          'delta' => 'sort',
          'region' => 'sidebar_first',
          'weight' => '-10',
        ),
        'facetapi-GiIy4zr9Gu0ZSa0bumw1Y9qIIpIDf1wu' => array(
          'module' => 'facetapi',
          'delta' => 'GiIy4zr9Gu0ZSa0bumw1Y9qIIpIDf1wu',
          'region' => 'sidebar_first',
          'weight' => '-9',
        ),
        'facetapi-6rL94KYR8rhFidmZzQ9CrPh5gdJMqMdr' => array(
          'module' => 'facetapi',
          'delta' => '6rL94KYR8rhFidmZzQ9CrPh5gdJMqMdr',
          'region' => 'sidebar_first',
          'weight' => '-8',
        ),
        'facetapi-n18E4x0qRRaIP7nV6I0SDYiCJsq77HCB' => array(
          'module' => 'facetapi',
          'delta' => 'n18E4x0qRRaIP7nV6I0SDYiCJsq77HCB',
          'region' => 'sidebar_first',
          'weight' => '-7',
        ),
        'facetapi-UOgmuLoCTvbzwcB0XZ82hUXQqCKMnUhH' => array(
          'module' => 'facetapi',
          'delta' => 'UOgmuLoCTvbzwcB0XZ82hUXQqCKMnUhH',
          'region' => 'sidebar_first',
          'weight' => '-6',
        ),
        'facetapi-R3lWVKZ0loVbYjsmiQUwPYESeoFz9xTm' => array(
          'module' => 'facetapi',
          'delta' => 'R3lWVKZ0loVbYjsmiQUwPYESeoFz9xTm',
          'region' => 'sidebar_first',
          'weight' => '-5',
        ),
        'facetapi-QidlyULqfM3HFv1vZeSCTTA0FG7mZKwk' => array(
          'module' => 'facetapi',
          'delta' => 'QidlyULqfM3HFv1vZeSCTTA0FG7mZKwk',
          'region' => 'sidebar_first',
          'weight' => '-4',
        ),
        'gcl_spotlight-gcl_spotlight_page_link' => array(
          'module' => 'gcl_spotlight',
          'delta' => 'gcl_spotlight_page_link',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
        'user_homepage-user_homepage_button' => array(
          'module' => 'user_homepage',
          'delta' => 'user_homepage_button',
          'region' => 'sidebar_second',
          'weight' => '-9',
        ),
        'user_homepage-user_homepage_reset_button' => array(
          'module' => 'user_homepage',
          'delta' => 'user_homepage_reset_button',
          'region' => 'sidebar_second',
          'weight' => '-8',
        ),
        'solr_search_saved-save-search' => array(
          'module' => 'solr_search_saved',
          'delta' => 'save-search',
          'region' => 'sidebar_second',
          'weight' => '-7',
        ),
        'solr_search_saved-saved-searches' => array(
          'module' => 'solr_search_saved',
          'delta' => 'saved-searches',
          'region' => 'sidebar_second',
          'weight' => '-6',
        ),
        'gcl_solr-gcl_opposite_domain' => array(
          'module' => 'gcl_solr',
          'delta' => 'gcl_opposite_domain',
          'region' => 'sidebar_second',
          'weight' => '-5',
        ),
      ),
    ),
    'delta' => array(
      'delta_template' => 'datadefault',
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('search');
  $export['data_search_results'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'endnode';
  $context->description = 'Block structure when a user is viewing end nodes';
  $context->tag = 'content';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'analyst_view' => 'analyst_view',
        'data' => 'data',
        'data_report' => 'data_report',
        'market_analysis' => 'market_analysis',
        'press_release' => 'press_release',
        'product' => 'product',
        'regulatory_news' => 'regulatory_news',
        'regulatory_reports' => 'regulatory_reports',
        'white_paper' => 'white_paper',
      ),
      'options' => array(
        'node_form' => '0',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'gcl_node_actions-node_actions' => array(
          'module' => 'gcl_node_actions',
          'delta' => 'node_actions',
          'region' => 'sidebar_second',
          'weight' => '-49',
        ),
        'tableofcontents_block-tableofcontents_block' => array(
          'module' => 'tableofcontents_block',
          'delta' => 'tableofcontents_block',
          'region' => 'sidebar_second',
          'weight' => '-49',
        ),
        'apachesolr_search-mlt-002' => array(
          'module' => 'apachesolr_search',
          'delta' => 'mlt-002',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
      ),
    ),
    'delta' => array(
      'delta_template' => 'article',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Block structure when a user is viewing end nodes');
  t('content');
  $export['endnode'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'events';
  $context->description = 'Events listings section';
  $context->tag = 'content';
  $context->conditions = array(
    'views' => array(
      'values' => array(
        'events:page' => 'events:page',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-topics-block' => array(
          'module' => 'views',
          'delta' => 'topics-block',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
        'views-videos-block' => array(
          'module' => 'views',
          'delta' => 'videos-block',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Events listings section');
  t('content');
  $export['events'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'gcl_frontpage';
  $context->description = '';
  $context->tag = 'anonymous';
  $context->conditions = array(
    'domain' => array(
      'values' => array(
        1 => 1,
      ),
    ),
    'path' => array(
      'values' => array(
        '<front>' => '<front>',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-hero_promotion-block' => array(
          'module' => 'views',
          'delta' => 'hero_promotion-block',
          'region' => 'content',
          'weight' => '-10',
        ),
        'system-main' => array(
          'module' => 'system',
          'delta' => 'main',
          'region' => 'content',
          'weight' => '-9',
        ),
        'views-topics-block' => array(
          'module' => 'views',
          'delta' => 'topics-block',
          'region' => 'content',
          'weight' => '-8',
        ),
        'views-videos-block' => array(
          'module' => 'views',
          'delta' => 'videos-block',
          'region' => 'content',
          'weight' => '-7',
        ),
        'bean-promo-one' => array(
          'module' => 'bean',
          'delta' => 'promo-one',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
        'bean-internet-gambline-taxes-us-lawma' => array(
          'module' => 'bean',
          'delta' => 'internet-gambline-taxes-us-lawma',
          'region' => 'sidebar_second',
          'weight' => '-9',
        ),
        'bean-in-play-tracker-winter-2012-repo' => array(
          'module' => 'bean',
          'delta' => 'in-play-tracker-winter-2012-repo',
          'region' => 'sidebar_second',
          'weight' => '-8',
        ),
        'views-events-block_1' => array(
          'module' => 'views',
          'delta' => 'events-block_1',
          'region' => 'sidebar_second',
          'weight' => '-7',
        ),
      ),
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('anonymous');
  $export['gcl_frontpage'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'gcl_frontpage_auth';
  $context->description = '';
  $context->tag = 'authenticated';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '<front>' => '<front>',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-hero_promotion-block' => array(
          'module' => 'views',
          'delta' => 'hero_promotion-block',
          'region' => 'content',
          'weight' => '-36',
        ),
        'views-topics-block' => array(
          'module' => 'views',
          'delta' => 'topics-block',
          'region' => 'content',
          'weight' => '35',
        ),
        'views-videos-block' => array(
          'module' => 'views',
          'delta' => 'videos-block',
          'region' => 'content',
          'weight' => '36',
        ),
        'bean-internet-gambline-taxes-us-lawma' => array(
          'module' => 'bean',
          'delta' => 'internet-gambline-taxes-us-lawma',
          'region' => 'sidebar_second',
          'weight' => '-34',
        ),
        'bean-in-play-tracker-winter-2012-repo' => array(
          'module' => 'bean',
          'delta' => 'in-play-tracker-winter-2012-repo',
          'region' => 'sidebar_second',
          'weight' => '-33',
        ),
        'bean-best-of-gamblingcompliance-12' => array(
          'module' => 'bean',
          'delta' => 'best-of-gamblingcompliance-12',
          'region' => 'sidebar_second',
          'weight' => '-32',
        ),
        'views-events-block_1' => array(
          'module' => 'views',
          'delta' => 'events-block_1',
          'region' => 'sidebar_second',
          'weight' => '-31',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('authenticated');
  $export['gcl_frontpage_auth'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'gcldata_global';
  $context->description = 'Sitewide context';
  $context->tag = 'global';
  $context->conditions = array(
    'domain' => array(
      'values' => array(
        2 => 2,
      ),
    ),
    'sitewide' => array(
      'values' => array(
        1 => 1,
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'menu-menu-type' => array(
          'module' => 'menu',
          'delta' => 'menu-type',
          'region' => 'menu',
          'weight' => '-10',
        ),
        'menu-menu-sector-menu' => array(
          'module' => 'menu',
          'delta' => 'menu-sector-menu',
          'region' => 'menu',
          'weight' => '-8',
        ),
        'menu-menu-geography' => array(
          'module' => 'menu',
          'delta' => 'menu-geography',
          'region' => 'menu',
          'weight' => '-7',
        ),
        'menu-menu-sources' => array(
          'module' => 'menu',
          'delta' => 'menu-sources',
          'region' => 'menu',
          'weight' => '-6',
        ),
      ),
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('Sitewide context');
  t('global');
  $export['gcldata_global'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'search_results';
  $context->description = '';
  $context->tag = 'search';
  $context->conditions = array(
    'domain' => array(
      'values' => array(
        1 => 1,
      ),
    ),
    'path' => array(
      'values' => array(
        'search/*' => 'search/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'apachesolr_search-sort' => array(
          'module' => 'apachesolr_search',
          'delta' => 'sort',
          'region' => 'sidebar_first',
          'weight' => '-10',
        ),
        'facetapi-GiIy4zr9Gu0ZSa0bumw1Y9qIIpIDf1wu' => array(
          'module' => 'facetapi',
          'delta' => 'GiIy4zr9Gu0ZSa0bumw1Y9qIIpIDf1wu',
          'region' => 'sidebar_first',
          'weight' => '-8',
        ),
        'facetapi-6rL94KYR8rhFidmZzQ9CrPh5gdJMqMdr' => array(
          'module' => 'facetapi',
          'delta' => '6rL94KYR8rhFidmZzQ9CrPh5gdJMqMdr',
          'region' => 'sidebar_first',
          'weight' => '-7',
        ),
        'facetapi-n18E4x0qRRaIP7nV6I0SDYiCJsq77HCB' => array(
          'module' => 'facetapi',
          'delta' => 'n18E4x0qRRaIP7nV6I0SDYiCJsq77HCB',
          'region' => 'sidebar_first',
          'weight' => '-6',
        ),
        'facetapi-UOgmuLoCTvbzwcB0XZ82hUXQqCKMnUhH' => array(
          'module' => 'facetapi',
          'delta' => 'UOgmuLoCTvbzwcB0XZ82hUXQqCKMnUhH',
          'region' => 'sidebar_first',
          'weight' => '-5',
        ),
        'facetapi-R3lWVKZ0loVbYjsmiQUwPYESeoFz9xTm' => array(
          'module' => 'facetapi',
          'delta' => 'R3lWVKZ0loVbYjsmiQUwPYESeoFz9xTm',
          'region' => 'sidebar_first',
          'weight' => '-4',
        ),
        'facetapi-cyBmnhfrun41YOgQS0n3AmG7JiQM9lW1' => array(
          'module' => 'facetapi',
          'delta' => 'cyBmnhfrun41YOgQS0n3AmG7JiQM9lW1',
          'region' => 'sidebar_first',
          'weight' => '-3',
        ),
        'gcl_spotlight-gcl_spotlight_page_link' => array(
          'module' => 'gcl_spotlight',
          'delta' => 'gcl_spotlight_page_link',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
        'user_homepage-user_homepage_button' => array(
          'module' => 'user_homepage',
          'delta' => 'user_homepage_button',
          'region' => 'sidebar_second',
          'weight' => '-9',
        ),
        'user_homepage-user_homepage_reset_button' => array(
          'module' => 'user_homepage',
          'delta' => 'user_homepage_reset_button',
          'region' => 'sidebar_second',
          'weight' => '-8',
        ),
        'solr_search_saved-save-search' => array(
          'module' => 'solr_search_saved',
          'delta' => 'save-search',
          'region' => 'sidebar_second',
          'weight' => '-7',
        ),
        'solr_search_saved-saved-searches' => array(
          'module' => 'solr_search_saved',
          'delta' => 'saved-searches',
          'region' => 'sidebar_second',
          'weight' => '-6',
        ),
        'gcl_solr-gcl_opposite_domain' => array(
          'module' => 'gcl_solr',
          'delta' => 'gcl_opposite_domain',
          'region' => 'sidebar_second',
          'weight' => '-5',
        ),
      ),
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('search');
  $export['search_results'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'spotlight_index';
  $context->description = 'The main spotlight page / term index page';
  $context->tag = 'Spotlight';
  $context->conditions = array(
    'views' => array(
      'values' => array(
        'spotlight:page' => 'spotlight:page',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'gcl_spotlight-gcl_spotlight_search_link' => array(
          'module' => 'gcl_spotlight',
          'delta' => 'gcl_spotlight_search_link',
          'region' => 'content',
          'weight' => '-10',
        ),
        'user_homepage-user_homepage_button' => array(
          'module' => 'user_homepage',
          'delta' => 'user_homepage_button',
          'region' => 'content',
          'weight' => '-9',
        ),
        'views-videos-block_2' => array(
          'module' => 'views',
          'delta' => 'videos-block_2',
          'region' => 'content',
          'weight' => '59',
        ),
        'views-spotlight-block_8' => array(
          'module' => 'views',
          'delta' => 'spotlight-block_8',
          'region' => 'sidebar_first',
          'weight' => '-10',
        ),
        'views-spotlight-block_7' => array(
          'module' => 'views',
          'delta' => 'spotlight-block_7',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
      ),
    ),
    'delta' => array(
      'delta_template' => '3column',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Spotlight');
  t('The main spotlight page / term index page');
  $export['spotlight_index'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'spotlight_market';
  $context->description = 'Spotlight for Market Analysis Specifically';
  $context->tag = 'Spotlight';
  $context->conditions = array(
    'views' => array(
      'values' => array(
        'spotlight:page_1' => 'spotlight:page_1',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-spotlight-block_2' => array(
          'module' => 'views',
          'delta' => 'spotlight-block_2',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
        'views-spotlight-block_3' => array(
          'module' => 'views',
          'delta' => 'spotlight-block_3',
          'region' => 'sidebar_second',
          'weight' => '-9',
        ),
      ),
    ),
    'delta' => array(
      'delta_template' => 'article',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Spotlight');
  t('Spotlight for Market Analysis Specifically');
  $export['spotlight_market'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'spotlight_news';
  $context->description = 'Spotlight for Regulatory News Specifically';
  $context->tag = 'Spotlight';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'taxonomy/term/news/*' => 'taxonomy/term/news/*',
      ),
    ),
    'views' => array(
      'values' => array(
        'spotlight:page_2' => 'spotlight:page_2',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-spotlight-block_1' => array(
          'module' => 'views',
          'delta' => 'spotlight-block_1',
          'region' => 'sidebar_second',
          'weight' => '-49',
        ),
        'views-spotlight-block_2' => array(
          'module' => 'views',
          'delta' => 'spotlight-block_2',
          'region' => 'sidebar_second',
          'weight' => '-48',
        ),
      ),
    ),
    'delta' => array(
      'delta_template' => 'article',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Spotlight');
  t('Spotlight for Regulatory News Specifically');
  $export['spotlight_news'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'spotlight_reports';
  $context->description = 'Spotlight for Regulatory Reports Specifically';
  $context->tag = 'Spotlight';
  $context->conditions = array(
    'views' => array(
      'values' => array(
        'spotlight:page_3' => 'spotlight:page_3',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-spotlight-block_3' => array(
          'module' => 'views',
          'delta' => 'spotlight-block_3',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
        'views-spotlight-block_1' => array(
          'module' => 'views',
          'delta' => 'spotlight-block_1',
          'region' => 'sidebar_second',
          'weight' => '-9',
        ),
      ),
    ),
    'delta' => array(
      'delta_template' => 'article',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Spotlight');
  t('Spotlight for Regulatory Reports Specifically');
  $export['spotlight_reports'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'video';
  $context->description = 'The layout for the video section';
  $context->tag = 'section';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'video' => 'video',
      ),
      'options' => array(
        'node_form' => '1',
      ),
    ),
    'views' => array(
      'values' => array(
        'videos' => 'videos',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-topics-block' => array(
          'module' => 'views',
          'delta' => 'topics-block',
          'region' => 'sidebar_second',
          'weight' => '-45',
        ),
        'views-events-block_1' => array(
          'module' => 'views',
          'delta' => 'events-block_1',
          'region' => 'sidebar_second',
          'weight' => '-44',
        ),
        'views-videos-block_1' => array(
          'module' => 'views',
          'delta' => 'videos-block_1',
          'region' => 'postscript_first',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('The layout for the video section');
  t('section');
  $export['video'] = $context;

  return $export;
}
