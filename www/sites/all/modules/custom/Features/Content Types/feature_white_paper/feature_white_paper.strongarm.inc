<?php
/**
 * @file
 * feature_white_paper.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function feature_white_paper_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_anonymous_white_paper';
  $strongarm->value = 0;
  $export['comment_anonymous_white_paper'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_mode_white_paper';
  $strongarm->value = 0;
  $export['comment_default_mode_white_paper'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_per_page_white_paper';
  $strongarm->value = '50';
  $export['comment_default_per_page_white_paper'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_form_location_white_paper';
  $strongarm->value = 0;
  $export['comment_form_location_white_paper'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_preview_white_paper';
  $strongarm->value = '0';
  $export['comment_preview_white_paper'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_subject_field_white_paper';
  $strongarm->value = 0;
  $export['comment_subject_field_white_paper'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_white_paper';
  $strongarm->value = '1';
  $export['comment_white_paper'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'field_bundle_settings_node__white_paper';
  $strongarm->value = array(
    'view_modes' => array(
      'teaser' => array(
        'custom_settings' => TRUE,
      ),
      'subscription' => array(
        'custom_settings' => TRUE,
      ),
      'full' => array(
        'custom_settings' => FALSE,
      ),
      'rss' => array(
        'custom_settings' => FALSE,
      ),
      'search_index' => array(
        'custom_settings' => FALSE,
      ),
      'search_result' => array(
        'custom_settings' => TRUE,
      ),
      'token' => array(
        'custom_settings' => FALSE,
      ),
      'anonymous user' => array(
        'custom_settings' => FALSE,
      ),
      'authenticated user' => array(
        'custom_settings' => FALSE,
      ),
      'Lite Subscription' => array(
        'custom_settings' => FALSE,
      ),
      'Full Subscription' => array(
        'custom_settings' => FALSE,
      ),
      'Internal Author' => array(
        'custom_settings' => FALSE,
      ),
      'Content Editor' => array(
        'custom_settings' => FALSE,
      ),
      'Content Manager' => array(
        'custom_settings' => FALSE,
      ),
      'admin' => array(
        'custom_settings' => FALSE,
      ),
      'print' => array(
        'custom_settings' => FALSE,
      ),
      'anonymous' => array(
        'custom_settings' => TRUE,
      ),
      'Americas_Premium' => array(
        'custom_settings' => FALSE,
      ),
      'International_Premium' => array(
        'custom_settings' => FALSE,
      ),
      'Americas_Professional' => array(
        'custom_settings' => FALSE,
      ),
      'International_Professional' => array(
        'custom_settings' => FALSE,
      ),
      'Gambling_Data' => array(
        'custom_settings' => FALSE,
      ),
    ),
    'extra_fields' => array(
      'form' => array(),
      'display' => array(
        'subscriptions_ui' => array(
          'default' => array(
            'weight' => '11',
            'visible' => TRUE,
          ),
          'search_result' => array(
            'weight' => '100',
            'visible' => TRUE,
          ),
          'subscription' => array(
            'weight' => '17',
            'visible' => FALSE,
          ),
          'anonymous' => array(
            'weight' => '17',
            'visible' => FALSE,
          ),
        ),
        'domain' => array(
          'default' => array(
            'weight' => '1',
            'visible' => TRUE,
          ),
          'subscription' => array(
            'weight' => '12',
            'visible' => FALSE,
          ),
          'anonymous' => array(
            'weight' => '12',
            'visible' => FALSE,
          ),
        ),
        'freetrialform' => array(
          'default' => array(
            'weight' => '6',
            'visible' => FALSE,
          ),
          'subscription' => array(
            'weight' => '13',
            'visible' => FALSE,
          ),
          'anonymous' => array(
            'weight' => '1',
            'visible' => TRUE,
          ),
        ),
      ),
    ),
  );
  $export['field_bundle_settings_node__white_paper'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_options_white_paper';
  $strongarm->value = array(
    0 => 'main-menu',
  );
  $export['menu_options_white_paper'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_parent_white_paper';
  $strongarm->value = 'main-menu:0';
  $export['menu_parent_white_paper'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_white_paper';
  $strongarm->value = array(
    0 => 'moderation',
    1 => 'revision',
  );
  $export['node_options_white_paper'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_preview_white_paper';
  $strongarm->value = '1';
  $export['node_preview_white_paper'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_submitted_white_paper';
  $strongarm->value = 1;
  $export['node_submitted_white_paper'] = $strongarm;

  return $export;
}
