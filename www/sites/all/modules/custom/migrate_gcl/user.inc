<?php
/**
 * GCL User Migration Handler.
 */
class GCLUserMigration extends DrupalUser7Migration {

  public function __construct(array $arguments) {
    parent::__construct($arguments);

    $this->addFieldMapping('signature_format', 'signature_format', FALSE);
    $this->addFieldMapping('is_new', NULL, FALSE)
      ->defaultValue(TRUE);
    $this->addFieldMapping('uid', 'uid');
  }

  /**
   * Query for the basic user data. Same query is used for all currently-supported
   * versions of Drupal.
   *
   * @return QueryConditionInterface
   */
  protected function query() {
    // Migrate only GCL Staff users.
    $gcl_legacy_staff_accounts = $this->gclLegacyStaffUsernames();
    $query = Database::getConnection('default', $this->sourceConnection)
      ->select('users', 'u');
    $query->join('users_roles', 'ur', 'u.uid = ur.uid');
    $query->fields('u')
      ->condition('u.uid', 1, '>')
      ->condition('u.name', $gcl_legacy_staff_accounts, 'IN');

    return $query;
  }

  public function prepareRow($row) {
      // Set a property with the roles of the current user in the old site.
      $row->legacy_roles = $this->getLegacyUserRoles($row->uid);
  }

  public function prepare($entity, stdClass $row) {
    if (isset($row->legacy_roles)) {
      $entity->roles = $this->mapRoles($row->legacy_roles);
    }
  }

  /**
   * Given an array of roles from the legacy site, returns an array with the
   * equivalent roles for the new site. The array returned can be assigned "as
   * is" to the 'roles' property of a user object.
   *
   * @param array $legacy_roles Array of roles from the old site.
   * @return array $roles_mapped The equivalent roles in the new site..
   */
  public function mapRoles($legacy_roles = array()) {
    $roles_map = array(
      'administrator' => 'admin',
      'Content admin' => 'Webmaster',
      'contractor' => 'Author',
      'Contributors' => 'Author',
      'Editor' => 'Editor',
      'Journalist' => 'Editor',
      'Newsletter admin' => 'Webmaster',
      'System Administrator' => 'admin',
      'User admin' => 'admin',
      'Webmaster' => 'Webmaster',
    );
    $roles_mapped = array();

    // Assemble the array of roles assigned to the user in the new site.
    foreach ($legacy_roles as $leg_role) {
      // If there's no mapping for the current legacy_role, do nothing.
      if (!isset($roles_map[$leg_role])) {
        continue;
      }
      $new_role = user_role_load_by_name($roles_map[$leg_role]);
      if (!isset($roles_mapped[$new_role->rid])) {
        $roles_mapped[$new_role->rid] = $new_role->name;
      }
    }

    return $roles_mapped;
  }

  public function getLegacyUserRoles($uid) {
    // Get the User Roles for the given user.
    $connection = Database::getConnection('default', 'legacy');
    $query = "SELECT name FROM {role} r, {users_roles} ur
     WHERE r.rid = ur.rid AND ur.uid = :uid";
    $args = array(
      ':uid' => $uid,
    );
    $results = $connection->query($query, $args);
    $user_roles = array();

    foreach ($results as $result) {
      $user_roles[] = $result->name;
    }

    return $user_roles;
  }

  public function gclLegacyStaffRids() {
    // TODO: Leave uncommented only the roles considered as GCL STAFF!!!
    $gcl_staff_roles = array(
      'administrator',
//      'anonymous user',
//      'authenticated user',
      'Content admin',
      'contractor',
      'Contributors',
      'Editor',
      'Journalist',
//      'lapsed subs',
      'Newsletter admin',
//      'pdfprinter',
//      'SearchBots',
//      'subscribed user',
      'System Administrator',
//      'trial expired user',
//      'trial user',
      'User admin',
      'Webmaster',
    );

    // Get the Role IDs for the Staff roles.
    $connection = Database::getConnection('default', 'legacy');
    $query = "SELECT rid FROM {role} WHERE name IN (:name)";
    $args = array(
      ':name' => $gcl_staff_roles,
    );
    $results = $connection->query($query, $args);

    $staff_role_ids = array();
    foreach ($results as $res) {
      $staff_role_ids[] = $res->rid;
    }

    return $staff_role_ids;
  }

  public function gclLegacyStaffUsernames() {
    return array(
      'chris@gamblingcompliance.com',
      'olliedemo',
      'ravi@gamblingcompliance.com',
      'Karen@gamblingcompliance.com',
      'johanna@gamblingcompliance.com',
      'tomp@gamblingcompliance.com',
      'lena@gamblingcompliance.com',
      'pauline',
      'tonybatt',
      'daniels@gamblingcompliance.com',
      'scottl',
      'TomD',
      'laurie',
      'Richard@gamblingcompliance.com',
      'ashleyb@gamblingcompliance.com',
      'AndrewG',
      'vicki@gamblingcompliance.com',
      'Balas@gamblingcompliance.com',
      'hannahf@gamblingcompliance.com',
      'moira@gamblingcompliance.com',
      'linas@gamblingcompliance.com',
      'dmorgan',
      'JamesK',
      'Soniac@gamblingcompliance.com',
      'kevinc@gamblingcompliance.com',
      'Jennifer@gamblingcompliance.com',
      'HarryA',
      'marina@gamblingcompliance.com',
      'rita@gamblingcompliance.com',
      'louise@gamblingcompliance.com',
    );
  }

}