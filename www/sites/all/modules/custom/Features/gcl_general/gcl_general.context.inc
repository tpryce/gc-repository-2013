<?php
/**
 * @file
 * gcl_general.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function gcl_general_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'gcl_global';
  $context->description = 'Sitewide context';
  $context->tag = 'global';
  $context->conditions = array(
    'domain' => array(
      'values' => array(
        1 => 1,
      ),
    ),
    'sitewide' => array(
      'values' => array(
        1 => 1,
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'menu-menu-category' => array(
          'module' => 'menu',
          'delta' => 'menu-category',
          'region' => 'menu',
          'weight' => '-48',
        ),
        'menu-menu-sector-menu' => array(
          'module' => 'menu',
          'delta' => 'menu-sector-menu',
          'region' => 'menu',
          'weight' => '-47',
        ),
        'menu-menu-geography' => array(
          'module' => 'menu',
          'delta' => 'menu-geography',
          'region' => 'menu',
          'weight' => '-46',
        ),
        'menu-menu-topics' => array(
          'module' => 'menu',
          'delta' => 'menu-topics',
          'region' => 'menu',
          'weight' => '-45',
        ),
      ),
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('Sitewide context');
  t('global');
  $export['gcl_global'] = $context;

  return $export;
}
