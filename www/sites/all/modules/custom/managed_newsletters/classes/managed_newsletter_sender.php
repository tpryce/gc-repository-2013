<?php
// $Id$

abstract class managed_newsletter_sender {
  public static function set_default_sender($sender) {
    variable_set('managed_newsletters_default_sender', $sender);
  }

  public static function get_default_sender() {
    return variable_get('managed_newsletters_default_sender', 'managed_newsletter_sender_phpmailer');
  }

  public static function load_default_sender() {
    $default_sender = managed_newsletter_sender::get_default_sender();
    if (class_exists($default_sender)) {
      return new $default_sender();
    }
  }
  protected function add_log(&$newsletter, $uid) {
    $id = db_insert('managed_newsletters_sent')
	  ->fields(array(
		  'uid' => $uid,
		  'lid' => $newsletter->get_send_process_id(),
		  'mail'=>'',
		  'content'=>'',
		  'error'=>'',
		  'timestamp'=>REQUEST_TIME
		))
	  ->execute();
    $newsletter->set_send_email_id($id);
  }

  protected function log(&$newsletter, $uid, $mail, $content, $error = null) {
    db_update('managed_newsletters_sent')
	  ->fields(array(
		  'uid' => $uid,
		  'lid' => $newsletter->get_send_process_id(),
		  'mail' => $mail,
		  'content' => $content,
		  'error' => $error,
		  'timestamp' => REQUEST_TIME,
		))
	  ->condition('sid', $newsletter->get_send_email_id())
	  ->execute();
  }

  public abstract function send_newsletter($newsletter, $user, $qid);
  public abstract function send_test($newsletter, $account);
}
