<?php

function keno_alpha_preprocess_region(&$vars) {
  if ($vars['region'] == 'content') {
    if(arg(0) == 'taxonomy' && arg(2)){
      $vars['theme_hook_suggestions'][] = 'region__content__termindex';
    }
    if ((arg(0) == 'taxonomy') && ((arg(2) == ('news')) || (arg(2) == ('reports')) || (arg(2) == ('market')))) {
      $vars['theme_hook_suggestions'][] = 'region__content__term';
    }
  }
}