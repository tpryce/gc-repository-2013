<?php
/**
 * @file
 * gcl_nodelinks.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function gcl_nodelinks_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_flag_default_flags().
 */
function gcl_nodelinks_flag_default_flags() {
  $flags = array();
  // Exported flag: "Add to watchlist".
  $flags['add_to_watchlist'] = array(
    'content_type' => 'node',
    'title' => 'Add to watchlist',
    'global' => '0',
    'types' => array(
      0 => 'analyst_view',
      1 => 'chart',
      2 => 'data',
      3 => 'data_report',
      4 => 'market_analysis',
      5 => 'press_release',
      6 => 'regulatory_news',
      7 => 'regulatory_reports',
      8 => 'static_page',
      9 => 'white_paper',
      10 => 'product',
      11 => 'event',
      12 => 'video',
    ),
    'flag_short' => 'Add to watchlist',
    'flag_long' => '',
    'flag_message' => '',
    'unflag_short' => 'Remove from watchlist',
    'unflag_long' => '',
    'unflag_message' => '',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'roles' => array(
      'flag' => array(
        0 => '2',
      ),
      'unflag' => array(
        0 => '2',
      ),
    ),
    'show_on_page' => 1,
    'show_on_teaser' => 0,
    'show_on_form' => 0,
    'access_author' => '',
    'i18n' => 0,
    'api_version' => 2,
    'module' => 'gcl_nodelinks',
    'locked' => array(
      0 => 'name',
    ),
  );
  // Exported flag: "Bookmarks".
  $flags['bookmarks'] = array(
    'content_type' => 'node',
    'title' => 'Bookmarks',
    'global' => '0',
    'types' => array(
      0 => 'analyst_view',
      1 => 'data',
      2 => 'data_report',
      3 => 'market_analysis',
      4 => 'product',
      5 => 'press_release',
      6 => 'regulatory_news',
      7 => 'regulatory_reports',
      8 => 'white_paper',
    ),
    'flag_short' => 'Add to bookmarks',
    'flag_long' => 'Add this post to your bookmarks',
    'flag_message' => 'This post has been added to your bookmarks',
    'unflag_short' => 'Remove from bookmarks',
    'unflag_long' => 'Remove this post from your bookmarks',
    'unflag_message' => 'This post has been removed from your bookmarks',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'roles' => array(
      'flag' => array(
        0 => '2',
        1 => '3',
        2 => '4',
        3 => '6',
        4 => '7',
      ),
      'unflag' => array(
        0 => '2',
        1 => '3',
        2 => '4',
        3 => '6',
        4 => '7',
      ),
    ),
    'show_on_page' => 1,
    'show_on_teaser' => 0,
    'show_on_form' => 1,
    'access_author' => '',
    'i18n' => 0,
    'module' => 'gcl_nodelinks',
    'locked' => array(
      0 => 'name',
    ),
    'api_version' => 2,
  );
  return $flags;
}
