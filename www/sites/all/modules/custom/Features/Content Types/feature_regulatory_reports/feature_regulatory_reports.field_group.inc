<?php
/**
 * @file
 * feature_regulatory_reports.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function feature_regulatory_reports_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_trunk_categories|node|regulatory_reports|Americas_Professional';
  $field_group->group_name = 'group_trunk_categories';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'regulatory_reports';
  $field_group->mode = 'Americas_Professional';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => '',
    'weight' => '3',
    'children' => array(
      0 => 'field_content',
      1 => 'field_geography',
      2 => 'field_sectors',
      3 => 'field_topics',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => '',
      'instance_settings' => array(
        'classes' => '',
        'description' => '',
        'show_label' => '1',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_trunk_categories|node|regulatory_reports|Americas_Professional'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_trunk_categories|node|regulatory_reports|anonymous';
  $field_group->group_name = 'group_trunk_categories';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'regulatory_reports';
  $field_group->mode = 'anonymous';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => '',
    'weight' => '3',
    'children' => array(
      0 => 'field_content',
      1 => 'field_geography',
      2 => 'field_sectors',
      3 => 'field_topics',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => '',
      'instance_settings' => array(
        'classes' => '',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_trunk_categories|node|regulatory_reports|anonymous'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_trunk_categories|node|regulatory_reports|default';
  $field_group->group_name = 'group_trunk_categories';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'regulatory_reports';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => '',
    'weight' => '4',
    'children' => array(
      0 => 'field_content',
      1 => 'field_geography',
      2 => 'field_sectors',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => '',
      'instance_settings' => array(
        'classes' => '',
        'description' => '',
        'show_label' => '1',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_trunk_categories|node|regulatory_reports|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_trunk_categories|node|regulatory_reports|form';
  $field_group->group_name = 'group_trunk_categories';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'regulatory_reports';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Trunk Categories',
    'weight' => '7',
    'children' => array(
      0 => 'field_content',
      1 => 'field_geography',
      2 => 'field_sectors',
      3 => 'field_topics',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'formatter' => 'open',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_trunk_categories|node|regulatory_reports|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_trunk_categories|node|regulatory_reports|full';
  $field_group->group_name = 'group_trunk_categories';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'regulatory_reports';
  $field_group->mode = 'full';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => '',
    'weight' => '5',
    'children' => array(
      0 => 'field_content',
      1 => 'field_geography',
      2 => 'field_sectors',
      3 => 'field_spotlight',
      4 => 'field_subscription_role',
      5 => 'field_topics',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => '',
      'instance_settings' => array(
        'classes' => '',
        'description' => '',
        'show_label' => '1',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_trunk_categories|node|regulatory_reports|full'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_trunk_categories|node|regulatory_reports|subscription';
  $field_group->group_name = 'group_trunk_categories';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'regulatory_reports';
  $field_group->mode = 'subscription';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => '',
    'weight' => '3',
    'children' => array(
      0 => 'field_content',
      1 => 'field_geography',
      2 => 'field_sectors',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => '',
      'instance_settings' => array(
        'classes' => '',
        'description' => '',
        'show_label' => '1',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_trunk_categories|node|regulatory_reports|subscription'] = $field_group;

  return $export;
}
