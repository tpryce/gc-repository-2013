<?php
/**
 * @file
 * feature_regulatory_news.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function feature_regulatory_news_taxonomy_default_vocabularies() {
  return array(
    'roles' => array(
      'name' => 'Roles',
      'machine_name' => 'roles',
      'description' => 'Terms corresponding to user roles.',
      'hierarchy' => '0',
      'module' => 'role_vocabulary',
      'weight' => '0',
    ),
  );
}
