<?php
/**
 * @file
 * gcl_vma_config.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function gcl_vma_config_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'Americas Premium';
  $strongarm->value = array(
    'Americas Premiumgeograpghy' => '',
    'Americas Premiumsubscription_role' => '',
    'Americas Premiumcontent' => '',
  );
  $export['Americas Premium'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'Americas_Premium';
  $strongarm->value = array(
    'Americas_Premiumgeograpghy' => 'Americas',
    'Americas_Premiumsubscription_role' => '',
    'Americas_Premiumcontent' => '',
  );
  $export['Americas_Premium'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'Americas_Premiumcontent';
  $strongarm->value = '';
  $export['Americas_Premiumcontent'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'Americas_Premiumgeograpghy';
  $strongarm->value = 'Americas, United Kingdom';
  $export['Americas_Premiumgeograpghy'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'Americas_Premiumgeography';
  $strongarm->value = 'Americas,Caribbean';
  $export['Americas_Premiumgeography'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'Americas_Premiumsubscription_role';
  $strongarm->value = '';
  $export['Americas_Premiumsubscription_role'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'Americas_Professionalcontent';
  $strongarm->value = '';
  $export['Americas_Professionalcontent'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'Americas_Professionalgeography';
  $strongarm->value = 'Americas,Caribbean';
  $export['Americas_Professionalgeography'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'Americas_Professionalsubscription_role';
  $strongarm->value = '';
  $export['Americas_Professionalsubscription_role'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'Datacontent';
  $strongarm->value = '';
  $export['Datacontent'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'Datageography';
  $strongarm->value = '';
  $export['Datageography'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'Datasubscription_role';
  $strongarm->value = '';
  $export['Datasubscription_role'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'Gambling_Datacontent';
  $strongarm->value = '';
  $export['Gambling_Datacontent'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'Gambling_Datageography';
  $strongarm->value = 'Latin America,Europe,Middle East,Africa,Asia/Pacific,United Kingdom,United States';
  $export['Gambling_Datageography'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'Gambling_Datasubscription_role';
  $strongarm->value = '';
  $export['Gambling_Datasubscription_role'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'International Premium';
  $strongarm->value = array(
    'International Premiumgeograpghy' => '',
    'International Premiumsubscription_role' => '',
    'International Premiumcontent' => '',
  );
  $export['International Premium'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'International_Premium';
  $strongarm->value = array(
    'International_Premiumgeograpghy' => '',
    'International_Premiumsubscription_role' => '',
    'International_Premiumcontent' => '',
  );
  $export['International_Premium'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'International_Premiumcontent';
  $strongarm->value = '';
  $export['International_Premiumcontent'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'International_Premiumgeograpghy';
  $strongarm->value = 'Caribbean, Australia, Northern Ireland';
  $export['International_Premiumgeograpghy'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'International_Premiumgeography';
  $strongarm->value = 'Latin America,Europe,Middle East,Africa,Asia/Pacific';
  $export['International_Premiumgeography'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'International_Premiumsubscription_role';
  $strongarm->value = '';
  $export['International_Premiumsubscription_role'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'International_Professionalcontent';
  $strongarm->value = '';
  $export['International_Professionalcontent'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'International_Professionalgeography';
  $strongarm->value = 'Latin America,Europe,Middle East,Africa,Asia/Pacific';
  $export['International_Professionalgeography'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'International_Professionalsubscription_role';
  $strongarm->value = '';
  $export['International_Professionalsubscription_role'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_analyst_view_Americas_Premium_view';
  $strongarm->value = 'subscription';
  $export['vma_analyst_view_Americas_Premium_view'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_analyst_view_Americas_Premium_view_analyst_view';
  $strongarm->value = 'subscription';
  $export['vma_analyst_view_Americas_Premium_view_analyst_view'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_analyst_view_Americas_Professional_view';
  $strongarm->value = 'subscription';
  $export['vma_analyst_view_Americas_Professional_view'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_analyst_view_Americas_Professional_view_analyst_view';
  $strongarm->value = 'subscription';
  $export['vma_analyst_view_Americas_Professional_view_analyst_view'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_analyst_view_enabled';
  $strongarm->value = 1;
  $export['vma_analyst_view_enabled'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_analyst_view_enabled_analyst_view';
  $strongarm->value = 1;
  $export['vma_analyst_view_enabled_analyst_view'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_analyst_view_Gambling_Data_view';
  $strongarm->value = 'subscription';
  $export['vma_analyst_view_Gambling_Data_view'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_analyst_view_Gambling_Data_view_analyst_view';
  $strongarm->value = 'subscription';
  $export['vma_analyst_view_Gambling_Data_view_analyst_view'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_analyst_view_International_Premium_view';
  $strongarm->value = 'subscription';
  $export['vma_analyst_view_International_Premium_view'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_analyst_view_International_Premium_view_analyst_view';
  $strongarm->value = 'subscription';
  $export['vma_analyst_view_International_Premium_view_analyst_view'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_analyst_view_International_Professional_view';
  $strongarm->value = 'subscription';
  $export['vma_analyst_view_International_Professional_view'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_analyst_view_International_Professional_view_analyst_view';
  $strongarm->value = 'subscription';
  $export['vma_analyst_view_International_Professional_view_analyst_view'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_data_Americas_Premium_view';
  $strongarm->value = 'subscription';
  $export['vma_data_Americas_Premium_view'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_data_Americas_Premium_view_data';
  $strongarm->value = 'subscription';
  $export['vma_data_Americas_Premium_view_data'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_data_Americas_Professional_view';
  $strongarm->value = 'subscription';
  $export['vma_data_Americas_Professional_view'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_data_Americas_Professional_view_data';
  $strongarm->value = 'subscription';
  $export['vma_data_Americas_Professional_view_data'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_data_enabled';
  $strongarm->value = 1;
  $export['vma_data_enabled'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_data_enabled_data';
  $strongarm->value = 1;
  $export['vma_data_enabled_data'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_data_Gambling_Data_view';
  $strongarm->value = 'subscription';
  $export['vma_data_Gambling_Data_view'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_data_Gambling_Data_view_data';
  $strongarm->value = 'subscription';
  $export['vma_data_Gambling_Data_view_data'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_data_International_Premium_view';
  $strongarm->value = 'subscription';
  $export['vma_data_International_Premium_view'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_data_International_Premium_view_data';
  $strongarm->value = 'subscription';
  $export['vma_data_International_Premium_view_data'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_data_International_Professional_view';
  $strongarm->value = 'subscription';
  $export['vma_data_International_Professional_view'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_data_International_Professional_view_data';
  $strongarm->value = 'subscription';
  $export['vma_data_International_Professional_view_data'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_data_report_Americas_Premium_view';
  $strongarm->value = 'subscription';
  $export['vma_data_report_Americas_Premium_view'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_data_report_Americas_Premium_view_data_report';
  $strongarm->value = 'subscription';
  $export['vma_data_report_Americas_Premium_view_data_report'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_data_report_Americas_Professional_view';
  $strongarm->value = 'subscription';
  $export['vma_data_report_Americas_Professional_view'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_data_report_Americas_Professional_view_data_report';
  $strongarm->value = 'subscription';
  $export['vma_data_report_Americas_Professional_view_data_report'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_data_report_enabled';
  $strongarm->value = 1;
  $export['vma_data_report_enabled'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_data_report_enabled_data_report';
  $strongarm->value = 1;
  $export['vma_data_report_enabled_data_report'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_data_report_Gambling_Data_view';
  $strongarm->value = 'subscription';
  $export['vma_data_report_Gambling_Data_view'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_data_report_Gambling_Data_view_data_report';
  $strongarm->value = 'subscription';
  $export['vma_data_report_Gambling_Data_view_data_report'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_data_report_International_Premium_view';
  $strongarm->value = 'subscription';
  $export['vma_data_report_International_Premium_view'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_data_report_International_Premium_view_data_report';
  $strongarm->value = 'subscription';
  $export['vma_data_report_International_Premium_view_data_report'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_data_report_International_Professional_view';
  $strongarm->value = 'subscription';
  $export['vma_data_report_International_Professional_view'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_data_report_International_Professional_view_data_report';
  $strongarm->value = 'subscription';
  $export['vma_data_report_International_Professional_view_data_report'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_market_analysis_Americas_Premium_view';
  $strongarm->value = 'subscription';
  $export['vma_market_analysis_Americas_Premium_view'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_market_analysis_Americas_Premium_view_market_analysis';
  $strongarm->value = 'subscription';
  $export['vma_market_analysis_Americas_Premium_view_market_analysis'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_market_analysis_Americas_Professional_view';
  $strongarm->value = 'subscription';
  $export['vma_market_analysis_Americas_Professional_view'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_market_analysis_Americas_Professional_view_market_analysis';
  $strongarm->value = 'subscription';
  $export['vma_market_analysis_Americas_Professional_view_market_analysis'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_market_analysis_enabled';
  $strongarm->value = 1;
  $export['vma_market_analysis_enabled'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_market_analysis_enabled_market_analysis';
  $strongarm->value = 1;
  $export['vma_market_analysis_enabled_market_analysis'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_market_analysis_Gambling_Data_view';
  $strongarm->value = 'anonymous';
  $export['vma_market_analysis_Gambling_Data_view'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_market_analysis_Gambling_Data_view_market_analysis';
  $strongarm->value = 'anonymous';
  $export['vma_market_analysis_Gambling_Data_view_market_analysis'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_market_analysis_International_Premium_view';
  $strongarm->value = 'subscription';
  $export['vma_market_analysis_International_Premium_view'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_market_analysis_International_Premium_view_market_analysis';
  $strongarm->value = 'subscription';
  $export['vma_market_analysis_International_Premium_view_market_analysis'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_market_analysis_International_Professional_view';
  $strongarm->value = 'subscription';
  $export['vma_market_analysis_International_Professional_view'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_market_analysis_International_Professional_view_market_analysis';
  $strongarm->value = 'subscription';
  $export['vma_market_analysis_International_Professional_view_market_analysis'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_regulatory_news_admin_view';
  $strongarm->value = 'Americas_Professional';
  $export['vma_regulatory_news_admin_view'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_regulatory_news_admin_view_regulatory_news';
  $strongarm->value = 'Americas_Professional';
  $export['vma_regulatory_news_admin_view_regulatory_news'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_regulatory_news_Americas_Premium_view';
  $strongarm->value = 'subscription';
  $export['vma_regulatory_news_Americas_Premium_view'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_regulatory_news_Americas_Premium_view_regulatory_news';
  $strongarm->value = 'subscription';
  $export['vma_regulatory_news_Americas_Premium_view_regulatory_news'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_regulatory_news_Americas_Professional_view';
  $strongarm->value = 'subscription';
  $export['vma_regulatory_news_Americas_Professional_view'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_regulatory_news_Americas_Professional_view_regulatory_news';
  $strongarm->value = 'subscription';
  $export['vma_regulatory_news_Americas_Professional_view_regulatory_news'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_regulatory_news_Data_view';
  $strongarm->value = 'full';
  $export['vma_regulatory_news_Data_view'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_regulatory_news_Data_view_regulatory_news';
  $strongarm->value = 'full';
  $export['vma_regulatory_news_Data_view_regulatory_news'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_regulatory_news_enabled';
  $strongarm->value = 1;
  $export['vma_regulatory_news_enabled'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_regulatory_news_enabled_regulatory_news';
  $strongarm->value = 1;
  $export['vma_regulatory_news_enabled_regulatory_news'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_regulatory_news_Gambling_Data_view';
  $strongarm->value = 'anonymous';
  $export['vma_regulatory_news_Gambling_Data_view'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_regulatory_news_Gambling_Data_view_regulatory_news';
  $strongarm->value = 'anonymous';
  $export['vma_regulatory_news_Gambling_Data_view_regulatory_news'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_regulatory_news_International_Premium_view';
  $strongarm->value = 'subscription';
  $export['vma_regulatory_news_International_Premium_view'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_regulatory_news_International_Premium_view_regulatory_news';
  $strongarm->value = 'subscription';
  $export['vma_regulatory_news_International_Premium_view_regulatory_news'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_regulatory_news_International_Professional_view';
  $strongarm->value = 'subscription';
  $export['vma_regulatory_news_International_Professional_view'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_regulatory_news_International_Professional_view_regulatory_news';
  $strongarm->value = 'subscription';
  $export['vma_regulatory_news_International_Professional_view_regulatory_news'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_regulatory_reports_admin_view';
  $strongarm->value = 'Americas_Professional';
  $export['vma_regulatory_reports_admin_view'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_regulatory_reports_admin_view_regulatory_reports';
  $strongarm->value = 'Americas_Professional';
  $export['vma_regulatory_reports_admin_view_regulatory_reports'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_regulatory_reports_Americas_Premium_view';
  $strongarm->value = 'subscription';
  $export['vma_regulatory_reports_Americas_Premium_view'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_regulatory_reports_Americas_Premium_view_regulatory_reports';
  $strongarm->value = 'subscription';
  $export['vma_regulatory_reports_Americas_Premium_view_regulatory_reports'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_regulatory_reports_Americas_Professional_view';
  $strongarm->value = 'anonymous';
  $export['vma_regulatory_reports_Americas_Professional_view'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_regulatory_reports_Americas_Professional_view_regulatory_reports';
  $strongarm->value = 'anonymous';
  $export['vma_regulatory_reports_Americas_Professional_view_regulatory_reports'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_regulatory_reports_enabled';
  $strongarm->value = 1;
  $export['vma_regulatory_reports_enabled'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_regulatory_reports_enabled_regulatory_reports';
  $strongarm->value = 1;
  $export['vma_regulatory_reports_enabled_regulatory_reports'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_regulatory_reports_Gambling_Data_view';
  $strongarm->value = 'anonymous';
  $export['vma_regulatory_reports_Gambling_Data_view'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_regulatory_reports_Gambling_Data_view_regulatory_reports';
  $strongarm->value = 'anonymous';
  $export['vma_regulatory_reports_Gambling_Data_view_regulatory_reports'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_regulatory_reports_International_Premium_view';
  $strongarm->value = 'subscription';
  $export['vma_regulatory_reports_International_Premium_view'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_regulatory_reports_International_Premium_view_regulatory_reports';
  $strongarm->value = 'subscription';
  $export['vma_regulatory_reports_International_Premium_view_regulatory_reports'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_regulatory_reports_International_Professional_view';
  $strongarm->value = 'anonymous';
  $export['vma_regulatory_reports_International_Professional_view'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_regulatory_reports_International_Professional_view_regulatory_reports';
  $strongarm->value = 'anonymous';
  $export['vma_regulatory_reports_International_Professional_view_regulatory_reports'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_static_page_enabled';
  $strongarm->value = 0;
  $export['vma_static_page_enabled'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_static_page_enabled_static_page';
  $strongarm->value = 0;
  $export['vma_static_page_enabled_static_page'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_white_paper_Americas_Premium_view';
  $strongarm->value = 'subscription';
  $export['vma_white_paper_Americas_Premium_view'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_white_paper_Americas_Premium_view_white_paper';
  $strongarm->value = 'subscription';
  $export['vma_white_paper_Americas_Premium_view_white_paper'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_white_paper_Americas_Professional_view';
  $strongarm->value = 'subscription';
  $export['vma_white_paper_Americas_Professional_view'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_white_paper_Americas_Professional_view_white_paper';
  $strongarm->value = 'subscription';
  $export['vma_white_paper_Americas_Professional_view_white_paper'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_white_paper_enabled';
  $strongarm->value = 1;
  $export['vma_white_paper_enabled'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_white_paper_enabled_white_paper';
  $strongarm->value = 1;
  $export['vma_white_paper_enabled_white_paper'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_white_paper_Gambling_Data_view';
  $strongarm->value = 'subscription';
  $export['vma_white_paper_Gambling_Data_view'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_white_paper_Gambling_Data_view_white_paper';
  $strongarm->value = 'subscription';
  $export['vma_white_paper_Gambling_Data_view_white_paper'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_white_paper_International_Premium_view';
  $strongarm->value = 'subscription';
  $export['vma_white_paper_International_Premium_view'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_white_paper_International_Premium_view_white_paper';
  $strongarm->value = 'subscription';
  $export['vma_white_paper_International_Premium_view_white_paper'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_white_paper_International_Professional_view';
  $strongarm->value = 'subscription';
  $export['vma_white_paper_International_Professional_view'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vma_white_paper_International_Professional_view_white_paper';
  $strongarm->value = 'subscription';
  $export['vma_white_paper_International_Professional_view_white_paper'] = $strongarm;

  return $export;
}
