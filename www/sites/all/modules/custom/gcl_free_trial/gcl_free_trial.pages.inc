<?php

/**
 * @file
 * Callbacks for Free Trial page(s).
 */

/**
 * Callback for free-trial page.
 */
function gcl_free_trial_page() {
  return '[free trial form to go here]';
}