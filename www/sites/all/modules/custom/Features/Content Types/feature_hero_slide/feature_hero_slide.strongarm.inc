<?php
/**
 * @file
 * feature_hero_slide.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function feature_hero_slide_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_anonymous_hero_slide';
  $strongarm->value = 0;
  $export['comment_anonymous_hero_slide'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_mode_hero_slide';
  $strongarm->value = 1;
  $export['comment_default_mode_hero_slide'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_per_page_hero_slide';
  $strongarm->value = '50';
  $export['comment_default_per_page_hero_slide'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_form_location_hero_slide';
  $strongarm->value = 1;
  $export['comment_form_location_hero_slide'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_hero_slide';
  $strongarm->value = '0';
  $export['comment_hero_slide'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_preview_hero_slide';
  $strongarm->value = '1';
  $export['comment_preview_hero_slide'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_subject_field_hero_slide';
  $strongarm->value = 1;
  $export['comment_subject_field_hero_slide'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'field_bundle_settings_node__hero_slide';
  $strongarm->value = array(
    'view_modes' => array(
      'teaser' => array(
        'custom_settings' => TRUE,
      ),
      'full' => array(
        'custom_settings' => FALSE,
      ),
      'rss' => array(
        'custom_settings' => FALSE,
      ),
      'search_index' => array(
        'custom_settings' => FALSE,
      ),
      'search_result' => array(
        'custom_settings' => FALSE,
      ),
      'anonymous user' => array(
        'custom_settings' => FALSE,
      ),
      'authenticated user' => array(
        'custom_settings' => FALSE,
      ),
      'Lite Subscription' => array(
        'custom_settings' => FALSE,
      ),
      'Full Subscription' => array(
        'custom_settings' => FALSE,
      ),
      'Internal Author' => array(
        'custom_settings' => FALSE,
      ),
      'Content Editor' => array(
        'custom_settings' => FALSE,
      ),
      'Content Manager' => array(
        'custom_settings' => FALSE,
      ),
      'admin' => array(
        'custom_settings' => FALSE,
      ),
      'token' => array(
        'custom_settings' => FALSE,
      ),
      'subscription' => array(
        'custom_settings' => TRUE,
      ),
      'anonymous' => array(
        'custom_settings' => TRUE,
      ),
      'Americas_Premium' => array(
        'custom_settings' => FALSE,
      ),
      'International_Premium' => array(
        'custom_settings' => FALSE,
      ),
      'Americas_Professional' => array(
        'custom_settings' => FALSE,
      ),
      'International_Professional' => array(
        'custom_settings' => FALSE,
      ),
      'Gambling_Data' => array(
        'custom_settings' => FALSE,
      ),
      'print' => array(
        'custom_settings' => FALSE,
      ),
    ),
    'extra_fields' => array(
      'form' => array(
        'title' => array(
          'weight' => '0',
        ),
        'path' => array(
          'weight' => '4',
        ),
      ),
      'display' => array(
        'subscriptions_ui' => array(
          'default' => array(
            'weight' => '100',
            'visible' => FALSE,
          ),
        ),
        'domain' => array(
          'default' => array(
            'weight' => '1',
            'visible' => TRUE,
          ),
        ),
        'freetrialform' => array(
          'default' => array(
            'weight' => '5',
            'visible' => TRUE,
          ),
        ),
      ),
    ),
  );
  $export['field_bundle_settings_node__hero_slide'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_options_hero_slide';
  $strongarm->value = array();
  $export['menu_options_hero_slide'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_parent_hero_slide';
  $strongarm->value = 'main-menu:0';
  $export['menu_parent_hero_slide'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_hero_slide';
  $strongarm->value = array(
    0 => 'status',
  );
  $export['node_options_hero_slide'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_preview_hero_slide';
  $strongarm->value = '0';
  $export['node_preview_hero_slide'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_submitted_hero_slide';
  $strongarm->value = 0;
  $export['node_submitted_hero_slide'] = $strongarm;

  return $export;
}
